/*
 *  boot/main.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <mmc.h>
#include <hardware.h>
#include <firmware.h>
#include <led.h>
#include <avr/boot.h>
#include <avr/interrupt.h>

uint8_t buffer[512];

static void write_flash_page(uint32_t addr, uint8_t * data) {
    boot_page_erase(addr);
    boot_spm_busy_wait();

    for (uint8_t i = 0; i < 128; i++) {
        boot_page_fill(i<<1, *(uint16_t *)data);
        data += 2;
    }

    boot_page_write(addr);
    boot_spm_busy_wait();

    boot_rww_enable();
}

__attribute__ ((naked))
void main() {
    struct tab {
        uint8_t boot;
        uint8_t chsStart[3];
        uint8_t type;
        uint8_t chsEnd[3];
        uint32_t start;
        uint32_t size;
    } *parts;
    struct firmwareHeader *hdr;
    uint8_t n;
    uint8_t blocks;
    uint32_t start;
    uint32_t addr = 0;
    uint8_t tmp;

    cli();

    ledInit();
    ledOn(0);

    LCD_CS_DDR |= _BV(LCD_CS);
    LCD_CS_PORT |= _BV(LCD_CS);

    MMC_DDR |= _BV(MMC_CS);
    MMC_PORT |= _BV(MMC_CS);

    SPI_DDR |= _BV(SS);
    SPI_PORT |= _BV(SS);

    SPI_PORT |= _BV(MOSI);
    SPI_DDR |= _BV(MOSI);

    SPI_PORT &= ~_BV(SCK);
    SPI_DDR |= _BV(SCK);

    SPI_PORT |= _BV(MISO);

    SPSR = _BV(SPI2X);
    SPCR = _BV(SPR1) | _BV(MSTR) | _BV(SPE);

    tmp = SPSR;

    if (mmcInit()) {
        goto error;
    }

    mmcReadSector(0, buffer);

    if (*(uint16_t *)(buffer+0x1fe) != 0xaa55) goto error;

    parts = (struct tab *)(buffer + 0x1be);

    for (n = 0; n < 4; ++n) {
        if (parts[n].type == 0x80) {
            start = parts[n].start;
            break;
        }
    }

    if (n == 4)
        goto error;

    mmcReadSector(start, buffer);

    hdr = (struct firmwareHeader *)buffer;

    if (hdr->magic != FIRMWARE_MAGIC)
        goto error;
    blocks = hdr->blocks;

    for (n = 0; n < blocks; ++n) {
        mmcReadSector(start + 1 + n, buffer);

        write_flash_page(addr, buffer);
        addr += 256;
        write_flash_page(addr, buffer + 256);
        addr += 256;

        ledToggle();
    }

error:
    asm volatile ("jmp 0x0000");
}


