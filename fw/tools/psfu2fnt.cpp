/*
 *  fw/tools/psfu2fnt.cpp
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <stdint.h>
#include <cstdlib>
#include <cstring>
using namespace std;

#define PSF2_MAGIC	0x864ab572

/* bits used in flags */
#define PSF2_HAS_UNICODE_TABLE 0x01

/* max version recognized so far */
#define PSF2_MAXVERSION 0

/* UTF8 separators */
#define PSF2_SEPARATOR  0xFF
#define PSF2_STARTSEQ   0xFE

struct psf2_header {
        uint32_t magic;
        uint32_t version;
        uint32_t headersize;    /* offset of bitmaps in file */
        uint32_t flags;
        uint32_t length;        /* number of glyphs */
        uint32_t charsize;      /* number of bytes for each character */
        uint32_t height, width; /* max dimensions of glyphs */
        /* charsize = height * ((width + 7) / 8) */
};

#define IO (uint32_t)-1
#define TERM (uint32_t)-2
#define SS (uint32_t)-3
#define INVAL (uint32_t)-4

uint32_t readUtf8(ifstream &file) {
	uint8_t c;

	c = file.get();
	if (!file) return IO;

	if (c == 0xff) {
		return TERM;
	}

	if (c == 0xfe) {
		return SS;
	}

	if ((c&0x80) == 0) {
		return c;
	}

	if ((c&0xe0) == 0xc0) {
		uint16_t w=(c&0x1f)<<6;
		c = file.get();
		if (!file) return IO;
		if ((c&0xc0) != 0x80) {
			return INVAL;
		}
		w|=c&0x3f;
		return w;
	}

	if ((c&0xf0) == 0xe0) {
		uint16_t w=(c&0x0f)<<12;
		c = file.get();
		if (!file) return IO;
		if ((c&0xc0) != 0x80) {
			return INVAL;
		}
		w|=(c&0x3f)<<6;
		c = file.get();
		if (!file) return IO;
		if ((c&0xc0) != 0x80) {
			return INVAL;
		}
		w|=c&0x3f;
		return w;
	}

	if ((c&0xf8) == 0xf0) {
		uint32_t w=(c&0x07)<<18;
		c = file.get();
		if (!file) return IO;
		if ((c&0xc0) != 0x80) {
			return INVAL;
		}
		w|=(c&0x3f)<<12;
		c = file.get();
		if (!file) return IO;
		if ((c&0xc0) != 0x80) {
			return INVAL;
		}
		w|=(c&0x3f)<<6;
		c = file.get();
		if (!file) return IO;
		if ((c&0xc0) != 0x80) {
			return INVAL;
		}
		w|=c&0x3f;

		return w;
	}

	return INVAL;
}

void usage(const char *prog) {
	cerr << "Usage: " << prog << " output-file psfu-file:from[:to]..." << endl;
	exit(1);
}

struct group {
	char *psfu;
	uint16_t from,to;
};

void copyGroup(ofstream &fnt, struct group g) {
	struct psf2_header hdr;

	ifstream psf(g.psfu);

	if (!psf) {
		cerr << "can't open " << g.psfu << endl;
		exit(1);
	}

	psf.read((char *)&hdr, sizeof(hdr));

	if (hdr.magic != PSF2_MAGIC) {
		cerr << "no psf2 file." << endl;
		exit(2);
	}

	if (!(hdr.flags & PSF2_HAS_UNICODE_TABLE)) {
		cerr << "psf2 file doesn't include a unicode table." << endl;
		exit(3);
	}

	psf.seekg (hdr.headersize+hdr.length*hdr.charsize, ios::beg);

	vector<uint16_t> table[hdr.length];

	for(int pos=0; pos<hdr.length; ++pos) {
		uint32_t c;
		bool ignore=false;

		while ((c = readUtf8(psf)) != TERM) {
			if (c == IO) {
				cerr << "unicode table ended unexpected." << endl;
				exit(4);
			}

			if (c == SS) ignore = true;

			if (!ignore && c != INVAL && c < 0x10000) {
				table[pos].push_back(c);
			}
		}
	}

	for (uint16_t uc=g.from; uc <= g.to; ++uc) {
		bool found=false;

		for (int pos=0; pos<hdr.length; ++pos) {
			for (int i=0; i<table[pos].size(); ++i) {
				if (table[pos][i] == uc) {
					uint8_t bitmap[hdr.charsize];

					psf.seekg (hdr.headersize+pos*hdr.charsize, ios::beg);
					psf.read((char *)bitmap,hdr.charsize);

					fnt.write((char *)bitmap,hdr.charsize);

					found = true;
					break;
				}
			}
			if (found) break;
		}

		if (!found) {
			cerr << "U+" << hex << setw(4) << setfill('0') << uc << dec << " not found" << endl;
			exit(5);
		}
	}
}

int main(int argc, char **argv) {
	vector<struct group> groups;
	uint32_t c;

	if (argc < 3) usage(argv[0]);

	for (int n=2; n<argc; ++n) {
		struct group g;
		char *endptr;

		g.psfu = new char[strlen(argv[n])+1];
		strcpy(g.psfu,argv[n]);

		endptr = strchr(g.psfu,':');
		if (!endptr) usage(argv[0]);

		*endptr = 0;
		++endptr;

		if (*endptr == ':') usage(argv[0]);
		g.from = strtol(endptr,&endptr,0);

		if (!*endptr) {
			g.to = g.from;
		} else {
			if (*endptr != ':') usage(argv[0]);
			if (!*(endptr+1)) usage(argv[0]);
			g.to = strtol(endptr+1,&endptr,0);
			if (*endptr) usage(argv[0]);

			if (g.to < g.from) usage(argv[0]);
		}

		groups.push_back(g);
	}


	ofstream fnt(argv[1]);

	if (!fnt) {
		cerr << "can't open " << argv[1] << endl;
		return 1;
	}


	fnt.put(groups.size());

	for (int n=0; n<groups.size(); ++n) {
		fnt.write((char *)&(groups[n].from),2);
		fnt.write((char *)&(groups[n].to),2);
	}

	for (int n=0; n<groups.size(); ++n) {
		copyGroup(fnt,groups[n]);
	}

	return 0;
}

