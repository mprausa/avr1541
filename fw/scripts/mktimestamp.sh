#!/bin/bash

TIMESTAMP=`date "+%s"`

echo "#ifndef __TIMESTAMP"
echo "#define __TIMESTAMP $TIMESTAMP"
echo "#endif"
echo
echo "#ifndef __DATE"
date "+#define __DATE \"%Y-%m-%d, %H:%M:%S\"" -d @$TIMESTAMP
echo "#endif"

