#!/bin/bash

TEXT=`avr-size -A $1 | grep "^\.text" | sed "s|  *|\t|g" | cut -f2`
DATA=`avr-size -A $1 | grep "^\.data" | sed "s|  *|\t|g" | cut -f2`
BSS=`avr-size -A $1 | grep "^\.bss" | sed "s|  *|\t|g" | cut -f2`
EEPROM=`avr-size -A $1 | grep "^\.eeprom" | sed "s|  *|\t|g" | cut -f2`
BOOT=1024

echo $(( 131072-($TEXT+$DATA+$BOOT) )) bytes ROM free.
echo $(( 16384-($DATA+$BSS) )) bytes SRAM free. 
echo $(( 4096-$EEPROM)) bytes EEPROM free.
