#!/bin/bash

echo '#include <rom1541.h>'
echo '#include <progmem.h>'
echo
echo 'const uint8_t PROGMEM __attribute__((aligned(256))) rom1541[] = {'
xxd -g1 -c 8 -i $1 | sed 's/^  /\t/;$d;1d'

