#!/bin/bash

TYPE=80
ROOTDEV=`readlink -f /dev/disk/by-uuid/$2 | sed 's/[0-9]$//'`

DEV=`fdisk -l $ROOTDEV | sed "s/  */\t/g" | grep "^$ROOTDEV" | cut -f1,5 | grep "$TYPE\$" | cut -f1`

if [ "$DEV" = "" ]; then
	echo uuid $2 not found or no firmware partition on disk
	exit 1
fi

echo "copying $1 to $DEV..."
echo

dd if=$1 of=$DEV

exit 0

