#!/bin/bash

SIZE=`stat --printf="%s" $1`
BLOCKS=$(( ($SIZE+511)/512 ))
MAGIC="\x64\x0c"

printf $MAGIC > $2
printf `printf '\\\\x%x' $BLOCKS` >> $2

truncate -s 512 $2

cat $1 >> $2

truncate -s $(( ($BLOCKS+1)*512 )) $2

