#!/bin/bash

echo '#include <petscii-charset.h>'
echo '#include <progmem.h>'
echo
echo 'const uint8_t PROGMEM petsciiCharset[][8] = {'
xxd -g1 -c 8 -l 1024 -i $1 | sed 's/^  /\t{/;s/,$/},/;$d;/};/d;1d' | sed '$s/$/}};/'

