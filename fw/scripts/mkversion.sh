#!/bin/bash

version=$(git describe | sed 's/^version//')

echo "#ifndef __VERSION"
echo "#define __VERSION \"$version\""

echo -n "#define __VERSION_STATUS "

[ ${#version} -lt 14 ] && echo -n "\"VERSION" || echo -n "\"VER"

echo " $version\"" | tr [a-z] [A-Z]

echo "#endif"

