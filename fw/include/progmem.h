/*
 *  fw/include/progmem.h
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PROGMEM_H
#define __PROGMEM_H

#include <inttypes.h>
#include <avr/pgmspace.h>

#define PROGSTR __attribute__ ((section (".progmem.strings,\"aMS\",@progbits,1 ;")))

#define _PSTR(s) (__extension__({static const char __c[] PROGSTR = (s); &__c[0];}))

uint8_t X_read_byte(const uint8_t *ptr, uint8_t pgm);
size_t strlen_X(const char *ptr, uint8_t pgm);

#endif //__PROGMEM_H
