/*
 *  fw/include/hardware.h
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HARDWARE_H
#define HARDWARE_H

#include <avr/io.h>

#define LED_DDR DDRB
#define LED_PORT PORTB
#define LED1 PB2
#define LED2 PB3
#define LED_GREEN _BV(LED2)
#define LED_RED _BV(LED1)

#define SPI_PORT PORTB
#define SPI_DDR DDRB
#define MOSI   PB5
#define MISO   PB6
#define SCK    PB7
#define SS     PB4

#define MMC_DDR    DDRA
#define MMC_PORT   PORTA
#define MMC_CS     PA6

#define LCD_CS_PORT   PORTA
#define LCD_CS_DDR    DDRA
#define LCD_CS     PA7
#define LCD_RESET_PORT PORTB
#define LCD_RESET_DDR DDRB
#define LCD_RESET  PB1
#define LCD_RS_PORT PORTB
#define LCD_RS_DDR DDRB
#define LCD_RS     PB0

#define BACKLIGHT_DDR DDRD
#define BACKLIGHT_PORT PORTD
#define BACKLIGHT_PWM PD7

#define KEYS_PORT PORTD
#define KEYS_PIN PIND
#define KEYS_DDR DDRD
#define KEYS_SHIFT 3
#define KEY_UP _BV(0)
#define KEY_DOWN _BV(1)
#define KEY_LEFT _BV(2)
#define KEY_RIGHT _BV(3)
#define KEYS_MASK (KEY_DOWN|KEY_UP|KEY_LEFT|KEY_RIGHT)

#define IEC_PORT PORTC
#define IEC_DDR DDRC
#define IEC_PIN PINC
#define IEC_ATNACK PC2
#define IEC_DAT PC3
#define IEC_CLK PC4
#define IEC_ATN PC5

#define VGA_PORT PORTA
#define VGA_DDR DDRA

#define VGA_AVSV PA0
#define VGA_PC PA1
#define VGA_PIP PA2
#define VGA_MODE PA3
#define VGA_MENU PA4
#define VGA_PP PA5

#define VGA_MASK (_BV(VGA_AVSV)|_BV(VGA_PC)|_BV(VGA_PIP)|_BV(VGA_MODE)|_BV(VGA_MENU)|_BV(VGA_PP))

#endif //HARDWARE_H
