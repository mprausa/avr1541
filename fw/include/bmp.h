/*
 *  fw/include/bmp.h
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BMP_H
#define __BMP_H

#include <inttypes.h>
#include <ext2.h>

enum corner { topLeft, bottomLeft, topRight, bottomRight };

int8_t showBmpIno(uint32_t ino, uint8_t posX, uint8_t posY, enum corner crn, uint8_t maxX, uint8_t maxY);
int8_t showBmpPath(const char *path, uint8_t posX, uint8_t posY, enum corner crn, uint8_t maxX, uint8_t maxY);

#endif //__BMP_H
