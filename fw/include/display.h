/*
 *  fw/include/display.h
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DISPLAY_H
#define DISPLAY_H

#include <inttypes.h>

#define DISPLAY_HEIGHT 132
#define DISPLAY_WIDTH 176

void initDisplay();
void displayOff();
void setPixel(uint8_t x, uint8_t y, uint16_t color);
void drawHorizontal(uint8_t x1, uint8_t x2, uint8_t y, uint16_t color);
void drawVertical(uint8_t x, uint8_t y1, uint8_t y2, uint16_t color);
void drawBox(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint16_t color);
void drawFilledBox(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint16_t color);
void fillScreen(uint16_t color);
void blankScreen();
void drawPixmap(uint8_t x, uint8_t y, uint8_t sizeX, uint8_t sizeY, const uint16_t *pixels);
void drawPixmap24(uint8_t x, uint8_t y, uint8_t sizeX, uint8_t sizeY, const uint8_t *pixels);
void drawBitmap(uint8_t x, uint8_t y, uint8_t sizeX, uint8_t sizeY, uint16_t color, uint16_t bgcolor, const uint8_t *bitmap, uint8_t eep);

#endif //DISPLAY_H
