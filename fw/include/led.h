/*
 *  fw/include/led.h
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __LED_H
#define __LED_H

#include <hardware.h>

#define ledInit() LED_DDR|=_BV(LED1)|_BV(LED2)

#define ledOff() LED_PORT|=_BV(LED1)|_BV(LED2)
#define GREEN 0
#define RED 1
#define ledOn(red) {LED_PORT &= ~(_BV(LED1)|_BV(LED2)); LED_PORT|=red?LED_RED:LED_GREEN;}
#define ledToggle() LED_PORT^=_BV(LED1)|_BV(LED2)

#endif //__LED_H
