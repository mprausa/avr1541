/*
 *  fw/include/mbr.h
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MBR_H
#define MBR_H

#include <inttypes.h>

int8_t mbrFindPartition();
int8_t mbrReadPartition(uint32_t sector, uint16_t offset, uint16_t size, uint8_t *buf);
int8_t mbrWritePartition(uint32_t sector, uint16_t offset, uint16_t size, const uint8_t *buf, uint8_t pgm);
int8_t mbrSync();

#ifdef DEBUG
int8_t mbrDebugWrite(uint32_t addr, uint16_t size, const uint8_t *buf);
#endif

#endif //MBR_H
