/*
 *  fw/include/ext2.h
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EXT2_H
#define EXT2_H

#include <inttypes.h>

/* Amount of indirect blocks in an inode.  */
#define EXT2_INDIRECT_BLOCKS        12

#define EXT2_S_IRWXU    0x01C0  /* user access rights mask */
#define EXT2_S_IRUSR    0x0100  /* read */
#define EXT2_S_IWUSR    0x0080  /* write */
#define EXT2_S_IXUSR    0x0040  /* execute */
#define EXT2_S_IRWXG    0x0038  /* group access rights mask */
#define EXT2_S_IRGRP    0x0020  /* read */
#define EXT2_S_IWGRP    0x0010  /* write */
#define EXT2_S_IXGRP    0x0008  /* execute */
#define EXT2_S_IRWXO    0x0007  /* others access rights mask */
#define EXT2_S_IROTH    0x0004  /* read */
#define EXT2_S_IWOTH    0x0002  /* write */
#define EXT2_S_IXOTH    0x0001  /* execute */

struct ext2_stripped_inode {
    uint16_t mode;
    uint32_t size;
    uint16_t nlinks;
    uint32_t blockcnt;  /* Blocks of 512 bytes!! */
    union {
        struct _datablocks {
            uint32_t dir_blocks[EXT2_INDIRECT_BLOCKS];
            uint32_t indir_block;
            uint32_t double_indir_block;
            uint32_t triple_indir_block;
        } blocks;
        char symlink[60];
    };
};

/* File description.  */
struct ext2_file {
    struct ext2_stripped_inode inode;
    uint32_t ino;
    uint32_t offset;
    struct {
        uint32_t fblock;
        uint32_t block;
    } cache;
};

/* Filetype used in directory entry.  */
#define FILETYPE_UNKNOWN    0
#define FILETYPE_REG        1
#define FILETYPE_DIRECTORY  2
#define FILETYPE_SYMLINK    7

int8_t ext2Mount();
int8_t ext2ReadInode(struct ext2_file *file, uint32_t ino);
uint32_t ext2FindFile(const char *name, uint8_t pgm);
int8_t ext2Open(struct ext2_file *file, const char *name, uint8_t pgm);
uint32_t ext2Create(const char *dir, const char *name, uint8_t pgm);
uint32_t ext2MkDir(const char *dir, const char *name, uint8_t pgm);
int8_t ext2Remove(const char *dir, const char *name, uint8_t pgm);
int16_t ext2Read(struct ext2_file *file, uint8_t *buf, uint16_t len);
int16_t ext2Write(struct ext2_file *file, const uint8_t *buf, uint16_t len);
uint32_t ext2ReadDir(struct ext2_file *file, char *filename, uint8_t len, uint8_t *filetype, uint8_t cmp);  // cmp = 1 -> compare (ram), cmp = 2 -> compare (pgm)
int8_t ext2Truncate(struct ext2_file *file, uint32_t len);
uint32_t ext2WriteTime();

#endif //EXT2_H
