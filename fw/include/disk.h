/*
 *  fw/include/disk.h
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DISK_H
#define __DISK_H

#include <stdint.h>
#include <ext2.h>

extern unsigned char diskTitle[];
extern struct ext2_file diskFloppy;
extern uint8_t diskID[];
extern uint8_t diskWriteProtection;
extern uint8_t diskChanged;
extern uint8_t diskHead;

int8_t diskMountIno(uint32_t ino);
int8_t diskMount(const char *name);
void diskUMount();
uint8_t diskSectorsPerTrack(uint8_t track);
uint32_t diskTrackOffset(uint8_t track);
void diskSync();

#endif //__DISK_H

