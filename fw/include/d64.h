/*
 *  fw/include/d64.h
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __D64_H
#define __D64_H

#include <stdint.h>
#include <ext2.h>

#define D64_TITLE_SIZE 16
#define D64_ID_SIZE 5

struct d64DirEntry {
	uint8_t next_track;
	uint8_t next_sector;
	uint8_t type;
	uint8_t track;
	uint8_t sector;
	unsigned char filename[16];
	uint8_t rel_track;
	uint8_t rel_sector;
	uint8_t rel_reclen;
	uint8_t unused[6];
	uint16_t size;
};

extern struct d64DirEntry d64Directory[];

int8_t d64Title(struct ext2_file *file, unsigned char *title);
int8_t d64ID(struct ext2_file *file, unsigned char *id);
uint8_t d64ReadDir(struct ext2_file *file);

#endif //__D64_H

