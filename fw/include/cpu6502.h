/*
 *  fw/include/cpu6502.h
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CPU6502_H
#define __CPU6502_H

#include <stdint.h>

extern uint8_t ram1541[];
extern uint8_t rom1541[];

extern uint8_t resetCpu;

void cpuInit();
void cpuMainLoop() __attribute__((noreturn));

#endif //__CPU6502_H


