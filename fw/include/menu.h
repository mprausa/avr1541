/*
 *  fw/include/menu.h
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MENU_H
#define __MENU_H

#include <stdint.h>

struct menu;

struct menuEntry {
	const char *name;	//pgm

	void (*handler) ();
};

struct menu {
	const char *title;
	uint8_t pgm;

	int nEntries;
	struct menuEntry *entries;

	void (*exitHandler) ();
};

void makeMenu(struct menu *menu);
void menuDraw(uint8_t keys, uint8_t rising, uint8_t falling);
void stateMenu(uint8_t keys, uint8_t rising, uint8_t falling);
uint16_t getMenuPosition();
void setMenuPosition(uint16_t pos);

#endif //__MENU_H
