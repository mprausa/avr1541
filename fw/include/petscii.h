/*
 *  fw/include/petscii.h
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PETSCII_H
#define __PETSCII_H

#include <inttypes.h>

uint8_t strlen_petscii(const unsigned char *str, uint8_t pgm);
void renderPETSCII(uint8_t x, uint8_t y, uint16_t color, uint16_t bgcolor, const unsigned char *text, uint8_t pgm);
void renderPETSCIILine(uint8_t y, uint16_t color, uint16_t bgcolor, const unsigned char *text, uint8_t pgm);

#endif //__PETSCII_H
