/*
 *  fw/src/backlight.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <hardware.h>
#include <backlight.h>
#include <avr/io.h>

void initBacklight() {
	BACKLIGHT_PORT &= ~_BV(BACKLIGHT_PWM);
	BACKLIGHT_DDR |= _BV(BACKLIGHT_PWM);
}

void setBacklight(uint8_t v) {
	if (v) {
		TCCR2A = _BV(WGM21) | _BV(WGM20) | _BV(COM2A1);
		TCCR2B = _BV(CS20);
		TCNT2 = 0x00;
		OCR2A = ((uint16_t)v * 210) / 255;
	} else {
		TCCR2A = 0;
		TCCR2B = 0;
	}
}
