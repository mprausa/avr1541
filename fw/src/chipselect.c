/*
 *  fw/src/chipselect.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <chipselect.h>
#include <hardware.h>
#include <interrupt.h>
#include <avr/io.h>
#include <progmem.h>
#include <bluescreen.h>

static int8_t enabled = 0;
static enum chips enabledChip;

void initCS(void) {
    MMC_DDR |= _BV(MMC_CS);
    MMC_PORT |= _BV(MMC_CS);

    LCD_CS_DDR |= _BV(LCD_CS);
    LCD_CS_PORT |= _BV(LCD_CS);
}

void chipSelect(enum chips chip, int8_t enable) {
    if (enabled && enable) {
        cli();
        chipSelect(enabledChip, 0);

        bluescreen(_PSTR(" chipselect error "));
    }

    enabled = enable;
    enabledChip = chip;

    switch (chip) {
        case mmc:
            if (enable)
                MMC_PORT &= ~_BV(MMC_CS);
            else
                MMC_PORT |= _BV(MMC_CS);
            break;
        case lcd:
            if (enable)
                LCD_CS_PORT &= ~_BV(LCD_CS);
            else
                LCD_CS_PORT |= _BV(LCD_CS);
            break;
    }
}
