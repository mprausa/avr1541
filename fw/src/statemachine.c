/*
 *  fw/src/statemachine.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <statemachine.h>
#include <filebrowser.h>
#include <hardware.h>
#include <stdint.h>
#include <disk.h>
#include <keys.h>
#include <text.h>
#include <display.h>
#include <led.h>
#include <timer.h>
#include <petscii.h>

#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <progmem.h>

void (*stateHandler) (uint8_t keys, uint8_t rising, uint8_t falling) = (void (*)(uint8_t keys, uint8_t rising, uint8_t falling))NULL;

#define REPEAT_IVAL1 7813	// 400 msec
#define REPEAT_IVAL2 1953	// 100 msec
static uint32_t lastRising = (uint32_t)-1;

uint8_t ignoreKeys = 0;

void stateMachine() {
	uint8_t keys, rising, falling;
	static uint8_t prevRepIval = 0xff;
	uint32_t ticks = getTicks();

	diskSync();

	getKeys(&keys,&rising,&falling);

	if (rising) {
		lastRising = ticks;
	} else {
		if (lastRising != (uint32_t)-1 && ((ticks - lastRising) & TIMER_MASK) >= REPEAT_IVAL1) {
			uint8_t repIval = (((ticks - lastRising) & TIMER_MASK) - REPEAT_IVAL1) / REPEAT_IVAL2;
			if (repIval != prevRepIval) {
				prevRepIval = repIval;
				rising = keys;
			}
		}
	}

	if (ignoreKeys) {
		if (!keys) {
			ignoreKeys = 0;
		}
		keys = rising = falling = 0;
	}

	if (stateHandler)
		stateHandler(keys, rising, falling);
}
