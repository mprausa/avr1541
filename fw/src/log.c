/*
 *  fw/src/log.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <log.h>
#include <ext2.h>
#include <mbr.h>

#include <progmem.h>
#include <stdio.h>
#include <errno.h>
#include <sys/errno.h>
#include <version.h>
#include <ctype.h>

static struct ext2_file logFile = {.ino = 0 };

char logBuffer[255];
uint8_t logPos = 0;

void logInit() {
    if (ext2Open(&logFile, _PSTR("/.c64/logfile.log"), 1)) {
        if (errno == ENOENT) {
            uint32_t ino;
            errno = 0;

            ino = ext2Create(_PSTR("/.c64"), _PSTR("logfile.log"), 1);
            if (!ino || errno) {
                errno = 0;
                logFile.ino = 0;
                return;
            }
            if (ext2ReadInode(&logFile, ino)) {
                errno = 0;
                logFile.ino = 0;
                return;
            }
        } else {
            errno = 0;
            logFile.ino = 0;
            return;
        }
    }

    logFile.offset = logFile.inode.size;    //append

    logPrintf(_PSTR("\n-------- version: " __VERSION " compile time: " __DATE " --------\n"));
}

static int logPut(char c, FILE * stream) {
    if (isprint(c) || isspace(c)) {
        logBuffer[logPos++] = c;
        if (logPos == 255 || c == '\n') {
            if (logFlush())
                return 1;
        }
    } else {
        if (logPos > 255 - 5) {
            if (logFlush())
                return 1;
        }

        sprintf_P(logBuffer + logPos, _PSTR("<%02X>"), (uint16_t)((uint8_t)c));
        logPos += 4;
    }

    return 0;
}

int8_t logFlush() {
    if (!logPos)
        return 0;

    if (ext2Write(&logFile, logBuffer, logPos) != logPos) {
        return -1;
    }
    if (mbrSync()) {
        return -1;
    }
    logPos = 0;

    return 0;
}

static FILE logStream = FDEV_SETUP_STREAM(logPut, NULL, _FDEV_SETUP_WRITE);

void logPrintf(const char *format, ...) {
    va_list args;

    if (!logFile.ino)
        return;

    va_start(args, format);

    vfprintf_P(&logStream, format, args);

    va_end(args);
}
