/*
 *  fw/src/config_menu.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config_menu.h>
#include <menu.h>
#include <config.h>
#include <disk.h>
#include <hybrid.h>
#include <via1.h>
#include <cpu6502.h>
#include <statemachine.h>
#include <mainstate.h>

#include <progmem.h>

static void updateEntries();

static void exitHandler() {
	saveConfig();
	stateHandler = mainState;
}

static void toggleHybridMode() {
	hybridMode ^= 1;
	updateEntries();
	stateHandler = menuDraw;
}

static void toggleWriteProtection() {
	diskWriteProtection ^= 1;
	updateEntries();
	stateHandler = menuDraw;
}

static void incDeviceNumber() {
	via1deviceNo += (1<<5);
	via1deviceNo &= (3<<5);

	resetCpu = 1;

	updateEntries();
	stateHandler = menuDraw;
}

static const char PROGSTR str_hybrid_on[] = "hybrid mode: on";
static const char PROGSTR str_hybrid_off[] = "hybrid mode: off";
static const char PROGSTR str_write_prot_on[] = "write protection: on";
static const char PROGSTR str_write_prot_off[] = "write protection: off";

static const char PROGSTR str_device_number8[] = "device number: 8";
static const char PROGSTR str_device_number9[] = "device number: 9";
static const char PROGSTR str_device_number10[] = "device number: 10";
static const char PROGSTR str_device_number11[] = "device number: 11";

static const char *str_device_numbers[] = {str_device_number8,str_device_number9,str_device_number10,str_device_number11};

static struct menuEntry configMenuEntries[] = {
	{.handler = toggleHybridMode},
	{.handler = toggleWriteProtection},
	{.handler = incDeviceNumber}};

static void updateEntries() {
	configMenuEntries[0].name = hybridMode?str_hybrid_on:str_hybrid_off;
	configMenuEntries[1].name = diskWriteProtection?str_write_prot_on:str_write_prot_off;
	configMenuEntries[2].name = str_device_numbers[via1deviceNo>>5];
}


static const char PROGSTR str_config[] = "config";

static struct menu configMenu = {.title = str_config, .pgm = 1, .nEntries = 3, .entries = configMenuEntries, .exitHandler = exitHandler};

void makeConfigMenu() {
	updateEntries();
	makeMenu(&configMenu);
	stateHandler = stateMenu;
}

