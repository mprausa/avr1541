/*
 *  fw/src/disk.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <disk.h>
#include <d64.h>
#include <config.h>
#include <rotation.h>
#include <statusbar.h>
#include <autoswap.h>
#include <ext2.h>
#include <progmem.h>
#include <log.h>
#include <via2.h>
#include <gcr.h>

#include <string.h>
#include <errno.h>

#define NUM_TRACKS 35

unsigned char diskTitle[D64_TITLE_SIZE+1];

struct ext2_file diskFloppy = {.ino = 0 };

static uint8_t gcrBuffer[GCR_SECTOR_SIZE];
static uint8_t bufferTrack = 0;
static uint8_t bufferSector = 0xff;
static uint8_t bufferDirty = 0;
static uint8_t bufferRead = 0;

uint8_t diskHead=2;     // halftrack of head
uint8_t diskChanged=0;
uint8_t diskWriteProtection=1;

uint8_t diskID[2];

uint8_t diskSectorsPerTrack(uint8_t track) {
    if (track < 18) {
        return 21;
    } else if (track < 25) {
        return 19;
    } else if (track < 31) {
        return 18;
    } else {
        return 17;
    }
}

uint32_t diskTrackOffset(uint8_t track) {
    if (track < 18) {
        return ((uint32_t)(track - 1) * 21) << 8;
    } else if (track < 25) {
        return ((uint32_t)(track - 18) * 19 + 17 * 21) << 8;
    } else if (track < 31) {
        return ((uint32_t)(track - 25) * 18 + 17 * 21 + 7 * 19) << 8;
    } else {
        return ((uint32_t)(track - 31) * 17 + 17 * 21 + 7 * 19 + 6 * 18) << 8;
    }
}

static void setHeader(uint8_t track, uint8_t sect) {
    uint8_t *gcrPtr = gcrBuffer;
    uint8_t bin[4];

    bin[0] = 0x08;                  // Header ID
    bin[1] = sect ^ track ^ diskID[1] ^ diskID[0];  // Checksum
    bin[2] = sect;                  // Sector
    bin[3] = track;                 // Track
    bin2gcr(bin, gcrPtr);
    gcrPtr += 5;

    bin[0] = diskID[1];
    bin[1] = diskID[0];
    bin[2] = 0x0f;
    bin[3] = 0x0f;
    bin2gcr(bin, gcrPtr);
    gcrPtr += 5;

    memset(gcrPtr, 0x55, 9);
    gcrPtr += 9;

    *gcrPtr = 0x55;                 // bin: 0x07

    bufferTrack = track;
    bufferSector = sect;
    bufferDirty = 0;
    bufferRead = 0;
}

static int8_t readSector() {
    uint8_t *gcrPtr = gcrBuffer+19;
    uint8_t *binPtr = gcrBuffer+GCR_SECTOR_SIZE-256;
    uint8_t checksum;
    uint8_t bin[4];

    statusBar(_PSTR("read"),bufferTrack,bufferSector);

    diskFloppy.offset = diskTrackOffset(bufferTrack)+(((uint16_t)bufferSector)<<8);

    if (ext2Read(&diskFloppy, binPtr, 256) != 256)
        return -1;

    bin[0] = 0x07;
    checksum = bin[1] = *(binPtr++);
    checksum ^= bin[2] = *(binPtr++);
    checksum ^= bin[3] = *(binPtr++);
    bin2gcr(bin, gcrPtr);
    gcrPtr += 5;

    for (uint8_t n=0; n<63; ++n) {
        checksum ^= bin[0] = *(binPtr++);
        checksum ^= bin[1] = *(binPtr++);
        checksum ^= bin[2] = *(binPtr++);
        checksum ^= bin[3] = *(binPtr++);
        bin2gcr(bin, gcrPtr);
        gcrPtr += 5;
    }

    checksum ^= bin[0] = *(binPtr++);
    bin[1] = checksum;
    bin[2] = 0;
    bin[3] = 0;
    bin2gcr(bin, gcrPtr);
    gcrPtr += 5;
    memset(gcrPtr, 0x55, 8);

    bufferRead = 1;
    return 0;
}

static int8_t writeSector() {
    uint8_t bin[4];
    uint8_t *gcrPtr = gcrBuffer+19;
    uint8_t *binPtr = gcrBuffer;
    uint8_t checksum=0;

    statusBar(_PSTR("write"),bufferTrack,bufferSector);

    gcr2bin(gcrBuffer,bin);
    if (bin[0] != 0x08) {
        logPrintf(_PSTR("wrong header id. not writing %u,%u"),bufferTrack,bufferSector);
        return -1;
    }
    if ((bin[2] != bufferSector) || (bin[3] != bufferTrack)) {
        logPrintf(_PSTR("sector/track mismatch. %u,%u (header) <-> %u,%u (buffer).\n"),bin[2],bin[3],bufferTrack,bufferSector);
        return -1;
    }

    for (uint8_t n=0; n<65; ++n) {
        gcr2bin(gcrPtr,binPtr);
        gcrPtr += 5;
        for (uint8_t i=0; i<4; ++i) {
            checksum ^= *(binPtr++);
        }
    }

    if (gcrBuffer[0] != 0x07) {
        logPrintf(_PSTR("wrong magic. not writing %u,%u"),bufferTrack,bufferSector);
        return -1;
    }

    if (checksum != 0x07) {
        logPrintf(_PSTR("wrong checksum. not writing %u,%u"),bufferTrack,bufferSector);
        return -1;
    }

    diskFloppy.offset = diskTrackOffset(bufferTrack)+(((uint16_t)bufferSector)<<8);

    if (ext2Write(&diskFloppy, gcrBuffer+1, 256) != 256)
        return -1;

    bufferSector = 0xff;
    bufferTrack = 0;
    bufferDirty = 0;
}

int8_t diskMountIno(uint32_t ino) {
    if (ext2ReadInode(&diskFloppy, ino)) {
        errno = 0;
        return -1;
    }

    bufferSector = 0xff;
    bufferTrack = 0;
    bufferDirty = 0;

    rotSect = 0;
    rotPos = 0;

    diskFloppy.offset = 0x165a2;

    if (ext2Read(&diskFloppy, diskID, 2) != 2) {
        return -1;
    }

    if (d64Title(&diskFloppy,diskTitle)) {
        return -1;
    }

    diskChanged = 1;
    diskWriteProtection = (diskFloppy.inode.mode & EXT2_S_IWUSR)?0:1;

    return 0;
}

int8_t diskMount(const char *name) {
    uint32_t ino = ext2FindFile(name, 0);
    if (errno) {
        errno = 0;
        return -1;
    }

    return diskMountIno(ino);
}

void diskUMount() {
    saveLastDisk("");
    disableAutoSwap();

    diskFloppy.ino = 0;
    bufferSector = 0xff;
    bufferTrack = 0;
    bufferDirty = 0;

    diskWriteProtection = 1;
}

void diskSync() {
    if (bufferDirty) {
        writeSector();
    }
}

uint8_t diskRead() {
    uint8_t sect;
    uint16_t pos;

    if ((via2PerControlReg & 0xe0) == 0xc0) {
        return via2portA;
    }

    if (rotBytePos) {
        sect = rotSect;
        pos = rotBytePos-1;
    } else {
        if (rotSect) {
            sect = rotSect-1;
        } else {
            sect = diskSectorsPerTrack(diskHead>>1)-1;
        }
        pos = GCR_SECTOR_SIZE-1;
    }

    if (pos >= 0x158) {
        return 0x55;
    }

    if (bufferSector != sect || bufferTrack != (diskHead>>1)) {
        diskSync();
        setHeader(diskHead>>1,sect);
    }

    if (!bufferRead && (pos >= 0x14)) {
        readSector();
    }

//  logPrintf(_PSTR("read @%u,%u,0x%x: 0x%02x\n"),diskHead>>1,sect,pos,gcrBuffer[pos]);

    return gcrBuffer[pos];
}

void diskWrite() {
    uint8_t sect;
    uint16_t pos;

    if (via2portA == 0xff) { // write sync
        if (rotBytePos >= 17 && rotBytePos <= 21) {
            rotPos = ((uint32_t)SYNC2)<<8;
            rotBytePos = ((((uint16_t)SYNC2)-20)>>3);
        } else if (rotBytePos <= 2 || rotBytePos >= GCR_SECTOR_SIZE-2) {
            rotPos = ((uint32_t)SYNC1)<<8;
            rotBytePos = 0;
        } else {
            logPrintf(_PSTR("not at sync (pos: %u)\n"),rotBytePos);
        }

        return;
    }

    if (rotBytePos) {
        sect = rotSect;
        pos = rotBytePos-1;
    } else {
        if (rotSect) {
            sect = rotSect-1;
        } else {
            sect = diskSectorsPerTrack(diskHead>>1)-1;
        }
        pos = GCR_SECTOR_SIZE-1;
    }

    if (bufferSector != sect || bufferTrack != (diskHead>>1)) {
        diskSync();
        setHeader(diskHead>>1,sect);
    }

    if (!bufferRead) {
        readSector();
    }

//  logPrintf(_PSTR("write @%u,%u,0x%x: 0x%02x\n"),diskHead>>1,sect,pos,via2portA);

    gcrBuffer[pos] = via2portA;
    bufferDirty = 1;
}

