/*
 *  fw/src/hybrid.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <hybrid.h>
#include <cpu6502.h>
#include <disk.h>
#include <rotation.h>
#include <string.h>
#include <via2.h>
#include <gcr.h>
#include <log.h>
#include <statusbar.h>

uint8_t hybridMode = 1;
uint8_t hybridFlag = 0;

static uint8_t hybridRead(uint16_t cpuCycles, uint8_t track, uint8_t sector, uint8_t *buffer) {
    if (!diskFloppy.ino) {
        return 0x0f;
    }

    diskFloppy.offset = diskTrackOffset(track)+(((uint16_t)sector)<<8);

    if (ext2Read(&diskFloppy, buffer, 256) != 256) {
        return 0x04;
    }

    statusBar(_PSTR("read"),track,sector);

    ram1541[0x16] = diskID[0];
    ram1541[0x17] = diskID[1];
    ram1541[0x18] = track;
    ram1541[0x19] = sector;
    ram1541[0x1a] = sector ^ track ^ diskID[1] ^ diskID[0];

    diskHead = track<<1;
    rotSect = sector+1;
    rotPos = 0;

    return 0x01;
}

static uint8_t hybridWrite(uint16_t cpuCycles, uint8_t track, uint8_t sector, const uint8_t *buffer) {
    if (!diskFloppy.ino) {
        return 0x0f;
    }

    if (diskWriteProtection) {
        return 0x08;
    }

    diskFloppy.offset = diskTrackOffset(track)+(((uint16_t)sector)<<8);

    if (ext2Write(&diskFloppy, buffer, 256) != 256) {
        return 0x04;
    }

    statusBar(_PSTR("write"),track,sector);

    ram1541[0x16] = diskID[0];
    ram1541[0x17] = diskID[1];
    ram1541[0x18] = track;
    ram1541[0x19] = sector;
    ram1541[0x1a] = sector ^ track ^ diskID[1] ^ diskID[0];

    diskHead = track<<1;
    rotSect = sector+1;
    rotPos = 0;

    return 0x01;
}

static uint8_t verifyBuffer[256];

static uint8_t hybridVerify(uint16_t cpuCycles, uint8_t track, uint8_t sector, uint8_t *buffer) {
    if (!diskFloppy.ino) {
        return 0x0f;
    }

    diskFloppy.offset = diskTrackOffset(track)+(((uint16_t)sector)<<8);

    if (ext2Read(&diskFloppy, verifyBuffer, 256) != 256) {
        return 0x04;
    }

    statusBar(_PSTR("verify"),track,sector);

    ram1541[0x16] = diskID[0];
    ram1541[0x17] = diskID[1];
    ram1541[0x18] = track;
    ram1541[0x19] = sector;
    ram1541[0x1a] = sector ^ track ^ diskID[1] ^ diskID[0];

    diskHead = track<<1;
    rotSect = sector+1;
    rotPos = 0;

    if (memcmp(buffer,verifyBuffer,256)) {
        return 0x07;
    }

    return 0x01;
}

static uint8_t hybridSeek(uint16_t cpuCycles, uint8_t track, uint8_t sector) {
    if (!diskFloppy.ino) {
        return 0x0f;
    }

    statusBar(_PSTR("seek"),track,sector);

    diskHead = track<<1;
    rotSect = sector;
    rotPos = 0;

    ram1541[0x12] = diskID[0];
    ram1541[0x13] = diskID[1];

    ram1541[0x16] = diskID[0];
    ram1541[0x17] = diskID[1];
    ram1541[0x18] = track;
    ram1541[0x19] = sector;
    ram1541[0x1a] = sector ^ track ^ diskID[1] ^ diskID[0];

    diskChanged = 0;
    ram1541[0x1e] = diskWriteProtection?0:0x10;

    return 0x01;
}

static uint8_t hybridBump(uint16_t cpuCycles) {
    if (!diskFloppy.ino) {
        return 0x0f;
    }

    statusBar(_PSTR("bump"),0,0);

    diskHead = 2;
    rotSect = 0;
    rotPos = 0;

    return 0x01;
}

static uint8_t hybridExecute(uint16_t cpuCycles, uint8_t track, uint8_t sector) {
    uint8_t speed = (track < 18) ? 3 : ((track < 25) ? 2 : ((track < 31) ? 1 : 0));
    uint8_t bin[4];

    if (!diskFloppy.ino) {
        return 0x0f;
    }

    statusBar(_PSTR("execute"),track,sector);

    via2portB &= 0x9b;
    via2portB |= 0x04|(speed<<5);

    via2PerControlReg |= 0x0e;

    ram1541[0x20] = 0xa0;
    ram1541[0x48] = 0x3c;

    ram1541[0x43] = diskSectorsPerTrack(track);

    bin[0] = 0x08;                      // Header ID
    bin[1] = sector ^ track ^ diskID[1] ^ diskID[0];    // Checksum
    bin[2] = sector;                    // Sector
    bin[3] = track;                     // Track

    bin2gcr(bin, ram1541+0x24);

    diskHead = track<<1;
    rotSect = sector;
    rotPos = 90<<8;

    return 0;
}

uint16_t hybridHandler(uint16_t cpuCycles) {
    if (diskChanged) {
        ram1541[0x1c] = 1;
    }

    for (uint8_t n=0; n<6; ++n) {
        if (ram1541[n]&0x80) {
            uint8_t res;

            logPrintf(_PSTR("buffer %d, code: 0x%02x, track: %u, sector: %u\n"),n,ram1541[n],ram1541[0x06+(n<<1)],ram1541[0x07+(n<<1)]);

            if (ram1541[n]&1) {
                res = 0x0f;
            } else {
                ram1541[0x3f] = n;

                switch(ram1541[n]) {
                    case 0x80:
                        res = hybridRead(cpuCycles,ram1541[0x06+(n<<1)],ram1541[0x07+(n<<1)],ram1541+0x0300+(((uint16_t)n)<<8));
                        break;
                    case 0x90:
                        res = hybridWrite(cpuCycles,ram1541[0x06+(n<<1)],ram1541[0x07+(n<<1)],ram1541+0x0300+(((uint16_t)n)<<8));
                        break;
                    case 0xa0:
                        res = hybridVerify(cpuCycles,ram1541[0x06+(n<<1)],ram1541[0x07+(n<<1)],ram1541+0x0300+(((uint16_t)n)<<8));
                        break;
                    case 0xb0:
                        res = hybridSeek(cpuCycles,ram1541[0x06+(n<<1)],ram1541[0x07+(n<<1)]);
                        break;
                    case 0xc0:
                        res = hybridBump(cpuCycles);
                        break;
                    case 0xd0:
                        return 0x0300+(((uint16_t)n)<<8);
                    case 0xe0:
                        res = hybridExecute(cpuCycles,ram1541[0x06+(n<<1)],ram1541[0x07+(n<<1)]);
                        if (!res) {
                            return 0x0300+(((uint16_t)n)<<8);
                        }
                        break;
                }
            }

            logPrintf(_PSTR("res: 0x%02x\n"),res);

            ram1541[n] = res;
        }
    }

    return 0;
}


