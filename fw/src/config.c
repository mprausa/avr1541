/*
 *  fw/src/config.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <ext2.h>
#include <hybrid.h>
#include <disk.h>
#include <autoswap.h>
#include <via1.h>

#include <progmem.h>
#include <string.h>

void loadConfig() {
    struct ext2_file file;
    uint8_t byte;

    if (ext2Open(&file,_PSTR("/.c64/config"),1)) return;
    if (ext2Read(&file, &byte, 1) != 1) return;

    via1deviceNo = byte & (3<<5);
    hybridMode = byte & 1;
}

void saveConfig() {
    struct ext2_file file;
    uint8_t byte = via1deviceNo | hybridMode;

    if (ext2Open(&file,_PSTR("/.c64/config"),1)) return;
    ext2Write(&file, &byte, 1);
}

void mountLastDisk() {
    struct ext2_file file;
    char path[256];
    if (ext2Open(&file,_PSTR("/.c64/lastdisk"),1)) return;

    if (ext2Read(&file,path,256)) {
        uint8_t len = strlen(path);

        if (!strcasecmp_P(path+len-4,_PSTR(".d64"))) {
            diskMount(path);
        } else if(!strcasecmp_P(path+len-4,_PSTR(".swp"))) {
            loadAutoSwap(path);
        }
    }
}

void saveLastDisk(const char *path) {
    struct ext2_file file;

    if (ext2Open(&file,_PSTR("/.c64/lastdisk"),1)) return;
    if (ext2Truncate(&file,0)) return;
    if (path[0]) {
        ext2Write(&file,path,strlen(path)+1);
    }
}


