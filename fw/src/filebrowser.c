/*
 *  fw/src/filebrowser.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <filebrowser.h>
#include <statemachine.h>
#include <config.h>
#include <autoswap.h>
#include <mainstate.h>
#include <statusbar.h>
#include <ext2.h>
#include <d64.h>
#include <disk.h>
#include <text.h>
#include <petscii.h>
#include <display.h>
#include <hardware.h>
#include <timer.h>
#include <utf8.h>
#include <progmem.h>

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

static uint16_t fbPos = 0;
static uint16_t fbStart = 0;
static uint16_t fbEntries = 0;
static uint16_t fbIsDir;
static char fbPath[256] = "";
static char *fbDirEnd = fbPath;
static uint8_t fbFileScrollPos, fbDirScrollPos;
static enum { left, right } fbFileScrollDirection, fbDirScrollDirection;
static uint32_t fbScrollTicks;

static void fbWait(uint8_t keys, uint8_t rising, uint8_t falling);
static void fbDraw(uint8_t keys, uint8_t rising, uint8_t falling);

static void fbD64Wait(uint8_t keys, uint8_t rising, uint8_t falling) {
    if (falling & KEY_LEFT) {
        stateHandler = fbDraw;
    }
}

static void fbD64Draw(uint8_t keys, uint8_t rising, uint8_t falling) {
    struct ext2_file file;
    uint8_t str[D64_TITLE_SIZE+1];
    uint8_t entries=0;
    uint8_t posY;

    *fbDirEnd = '/';
    if (ext2Open(&file,fbPath,0)) {
        *fbDirEnd = 0;
        stateHandler = fbWait;
        return;
    }

    *fbDirEnd = 0;

    if (d64ReadDir(&file)) {
        stateHandler = fbWait;
        return;
    }

    for (uint8_t n=0; n<16; ++n) {
        if (d64Directory[n].type) ++entries;
    }

    if (entries > 13) entries = 13;

    posY = (108-(entries<<3))>>1;

    drawBox(3,posY-1,172,posY+(entries<<3)+8,0xffff);
    drawFilledBox(4,posY+8,171,posY+(entries<<3)+7,0x49d5);
    drawFilledBox(4,posY,171,posY+7,0xffff);

    if (d64Title(&file,str)) {
        stateHandler = fbWait;
        return;
    }

    renderPETSCII(4,posY,0x49d5,0xffff,_PSTR("\""),1);
    renderPETSCII(12,posY,0x49d5,0xffff,str,0);
    if (strlen_petscii(str,0) < 14) {
        renderPETSCII(116,posY,0x49d5,0xffff,_PSTR("\""),1);
    }

    if (d64ID(&file,str)) {
        stateHandler = fbWait;
        return;
    }

    for (uint8_t n=0; n<D64_ID_SIZE; ++n) {
        if (str[n] == 0xa0) str[n] = ' ';
    }

    renderPETSCII(124,posY,0x49d5,0xffff,_PSTR(" "),1);
    renderPETSCII(132,posY,0x49d5,0xffff,str,0);

    for (uint8_t n=0; n<16 && entries; ++n) {
        if (d64Directory[n].type) {
            const char *ptr;
            uint8_t posX;

            posY += 8;
            --entries;

            sprintf_P(str,_PSTR("%d"),d64Directory[n].size);

            renderPETSCII(4,posY,0xffff,0x49d5,str,0);
            renderPETSCII(28,posY,0xffff,0x49d5,_PSTR("\""),1);

            d64Directory[n].filename[16] = 0;

            renderPETSCII(36,posY,0xffff,0x49d5,d64Directory[n].filename,0);
            renderPETSCII(36+(strlen_petscii(d64Directory[n].filename,0)<<3),posY,0xffff,0x49d5,_PSTR("\""),1);
            switch(d64Directory[n].type & 0x0f) {
                case 0:
                    ptr = _PSTR("*DEL");
                    break;
                case 1:
                    ptr = _PSTR("*SEQ");
                    break;
                case 2:
                    ptr = _PSTR("*PRG");
                    break;
                case 3:
                    ptr = _PSTR("*USR");
                    break;
                case 4:
                    ptr = _PSTR("*REL");
                    break;
                default:
                    ptr = _PSTR("*INV");
                    break;
            }

            if (d64Directory[n].type & 0x80) ++ptr;

            posX = 164-((strlen_P(ptr)+((d64Directory[n].type>>6)&1))<<3);

            renderPETSCII(posX,posY,0xffff,0x49d5,_PSTR(" "),1);
            renderPETSCII(posX+8,posY,0xffff,0x49d5,ptr,1);
            if (d64Directory[n].type & 0x40) {
                renderPETSCII(164,posY,0xffff,0x49d5,_PSTR("<"),1);
            }
        }
    }

    stateHandler = fbD64Wait;
}

static void fbWait(uint8_t keys, uint8_t rising, uint8_t falling) {
    if (((getTicks() - fbScrollTicks) & TIMER_MASK) > 1953) {
        char *dirname = *fbPath ? fbPath : "/";

        fbScrollTicks = getTicks();

        if (strlen_utf8(dirname, 0) > 22) {
            renderTextLine(0, 0x0000, 0xf800, shift_utf8(dirname, fbDirScrollPos, 0), 0);
            if (fbDirScrollDirection == right) {
                if (fbDirScrollPos == strlen_utf8(dirname, 0) - 22) {
                    fbDirScrollDirection = left;
                } else {
                    fbDirScrollPos++;
                }
            } else {
                if (fbDirScrollPos == 0) {
                    fbDirScrollDirection = right;
                } else {
                    fbDirScrollPos--;
                }
            }
        }
        if (strlen_utf8(fbDirEnd + 1, 0) > 22) {
            renderTextLine(16 + (fbPos - fbStart) * 16, 0x0000, 0xffff, shift_utf8(fbDirEnd + 1, fbFileScrollPos, 0), 0);
            if (fbFileScrollDirection == right) {
                if (fbFileScrollPos == strlen_utf8(fbDirEnd + 1, 0) - 22) {
                    fbFileScrollDirection = left;
                } else {
                    fbFileScrollPos++;
                }
            } else {
                if (fbFileScrollPos == 0) {
                    fbFileScrollDirection = right;
                } else {
                    fbFileScrollPos--;
                }
            }
        }
    }
    if (fbEntries && (falling & KEY_UP)) {
        if (fbPos > 0) {
            --fbPos;
            if (fbPos < fbStart) {
                fbStart = fbPos;
            }
            stateHandler = fbDraw;
        }
    }
    if (fbEntries && (falling & KEY_DOWN)) {
        if (fbPos < fbEntries - 1) {
            ++fbPos;
            if (fbPos >= fbStart + 5) {
                fbStart = fbPos - 5;
            }
            stateHandler = fbDraw;
        }
    }
    if (falling & KEY_LEFT) {
        if (*fbPath) {
            fbDirEnd = strrchr(fbPath, '/');
            if (fbDirEnd)
                *fbDirEnd = 0;

            fbPos = fbStart = 0;

            stateHandler = fileBrowser;
        }
    }
    if (fbEntries && (falling & KEY_RIGHT)) {
        if (fbIsDir) {
            *fbDirEnd = '/';
            fbDirEnd = fbPath + strlen(fbPath);

            fbPos = fbStart = 0;

            stateHandler = fileBrowser;
        } else {
            if (!strcasecmp_P(fbDirEnd + 1 + strlen(fbDirEnd + 1) - 4, _PSTR(".d64"))) {
                if (diskFloppy.ino) {
                    diskUMount();
                }

                *fbDirEnd = '/';
                diskMount(fbPath);
                saveLastDisk(fbPath);
                *fbDirEnd = 0;

                stateHandler = mainState;
            } else if (!strcasecmp_P(fbDirEnd + 1 + strlen(fbDirEnd + 1) - 4, _PSTR(".swp"))) {
                if (diskFloppy.ino) {
                    diskUMount();
                }

                *fbDirEnd = '/';
                loadAutoSwap(fbPath);
                saveLastDisk(fbPath);
                *fbDirEnd = 0;

                stateHandler = mainState;
            }

        }
    }

    if ((keys & (KEY_LEFT | KEY_RIGHT)) == (KEY_LEFT | KEY_RIGHT)) {
        ignoreKeys = 1;
        stateHandler = mainState;
    }

    if ((keys & (KEY_LEFT | KEY_UP)) == (KEY_LEFT | KEY_UP)) {
        ignoreKeys = 1;
        if (!strcasecmp_P(fbDirEnd + 1 + strlen(fbDirEnd + 1) - 4, _PSTR(".d64"))) {
            stateHandler = fbD64Draw;
        }
    }
}

static void fbDraw(uint8_t keys, uint8_t rising, uint8_t falling) {
    struct ext2_file dir;
    char *dirname = *fbPath ? fbPath : "/";

    renderTextLine(0, 0x0000, 0xf800, dirname + fbDirScrollPos, 0);

    ext2Open(&dir, dirname, 0);
    if (!errno) {
        char filename[128];
        uint16_t pos = 0;
        uint32_t ino;
        uint8_t filetype;

        *(fbDirEnd + 1) = 0;

        while ((ino = ext2ReadDir(&dir, filename, 128, &filetype, 0)) != (uint32_t)-1) {
            if (!ino)
                continue;
            if (filename[0] == '.')
                continue;

            if (pos >= fbStart && pos < fbStart + 6) {
                uint16_t color = 0xffff;
                uint16_t bgcolor = 0x0000;

                if (pos == fbPos) {
                    color = 0x0000;
                    bgcolor = 0xffff;
                    strcpy(fbDirEnd + 1, filename);
                    fbIsDir = (filetype == FILETYPE_DIRECTORY);
                }
                renderTextLine(16 + (pos - fbStart) * 16, color, bgcolor, filename, 0);
            }

            ++pos;
        }

        fbEntries = pos;

        if (pos > fbStart+6) pos = fbStart+6;
        drawFilledBox(0,16 + (pos - fbStart) * 16, 175, 115, 0x0000);
    }

    fbScrollTicks = getTicks();
    fbFileScrollPos = 0;
    fbFileScrollDirection = right;

    stateHandler = fbWait;
}

void fileBrowser(uint8_t keys, uint8_t rising, uint8_t falling) {
    blankAllButStatus();

    fbDirScrollPos = 0;
    fbDirScrollDirection = right;

    stateHandler = fbDraw;
}
