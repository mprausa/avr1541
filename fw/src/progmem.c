/*
 *  fw/src/progmem.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <progmem.h>
#include <string.h>

uint8_t X_read_byte(const uint8_t *ptr, uint8_t pgm) {
    return pgm ? pgm_read_byte(ptr) : *ptr;
}

size_t strlen_X(const char *ptr, uint8_t pgm) {
    return pgm ? strlen_P(ptr) : strlen(ptr);
}
