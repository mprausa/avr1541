/*
 *  fw/src/mmc.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <mmc.h>
#include <chipselect.h>
#include <spi.h>
#include <interrupt.h>

#include <stdio.h>
#include <progmem.h>

/* commands available in SPI mode */

/* CMD0: response R1 */
#define CMD_GO_IDLE_STATE 0x00
/* CMD1: response R1 */
#define CMD_SEND_OP_COND 0x01
/* CMD8: response R7 */
#define CMD_SEND_IF_COND 0x08
/* CMD9: response R1 */
#define CMD_SEND_CSD 0x09
/* CMD10: response R1 */
#define CMD_SEND_CID 0x0a
/* CMD12: response R1 */
#define CMD_STOP_TRANSMISSION 0x0c
/* CMD13: response R2 */
#define CMD_SEND_STATUS 0x0d
/* CMD16: arg0[31:0]: block length, response R1 */
#define CMD_SET_BLOCKLEN 0x10
/* CMD17: arg0[31:0]: data address, response R1 */
#define CMD_READ_SINGLE_BLOCK 0x11
/* CMD18: arg0[31:0]: data address, response R1 */
#define CMD_READ_MULTIPLE_BLOCK 0x12
/* CMD24: arg0[31:0]: data address, response R1 */
#define CMD_WRITE_SINGLE_BLOCK 0x18
/* CMD25: arg0[31:0]: data address, response R1 */
#define CMD_WRITE_MULTIPLE_BLOCK 0x19
/* CMD27: response R1 */
#define CMD_PROGRAM_CSD 0x1b
/* CMD28: arg0[31:0]: data address, response R1b */
#define CMD_SET_WRITE_PROT 0x1c
/* CMD29: arg0[31:0]: data address, response R1b */
#define CMD_CLR_WRITE_PROT 0x1d
/* CMD30: arg0[31:0]: write protect data address, response R1 */
#define CMD_SEND_WRITE_PROT 0x1e
/* CMD32: arg0[31:0]: data address, response R1 */
#define CMD_TAG_SECTOR_START 0x20
/* CMD33: arg0[31:0]: data address, response R1 */
#define CMD_TAG_SECTOR_END 0x21
/* CMD34: arg0[31:0]: data address, response R1 */
#define CMD_UNTAG_SECTOR 0x22
/* CMD35: arg0[31:0]: data address, response R1 */
#define CMD_TAG_ERASE_GROUP_START 0x23
/* CMD36: arg0[31:0]: data address, response R1 */
#define CMD_TAG_ERASE_GROUP_END 0x24
/* CMD37: arg0[31:0]: data address, response R1 */
#define CMD_UNTAG_ERASE_GROUP 0x25
/* CMD38: arg0[31:0]: stuff bits, response R1b */
#define CMD_ERASE 0x26
/* ACMD41: arg0[31:0]: OCR contents, response R1 */
#define CMD_SD_SEND_OP_COND 0x29
/* CMD42: arg0[31:0]: stuff bits, response R1b */
#define CMD_LOCK_UNLOCK 0x2a
/* CMD55: arg0[31:0]: stuff bits, response R1 */
#define CMD_APP 0x37
/* CMD58: arg0[31:0]: stuff bits, response R3 */
#define CMD_READ_OCR 0x3a
/* CMD59: arg0[31:1]: stuff bits, arg0[0:0]: crc option, response R1 */
#define CMD_CRC_ON_OFF 0x3b

/* command responses */
/* R1: size 1 byte */
#define R1_IDLE_STATE 0
#define R1_ERASE_RESET 1
#define R1_ILL_COMMAND 2
#define R1_COM_CRC_ERR 3
#define R1_ERASE_SEQ_ERR 4
#define R1_ADDR_ERR 5
#define R1_PARAM_ERR 6
/* R1b: equals R1, additional busy bytes */
/* R2: size 2 bytes */
#define R2_CARD_LOCKED 0
#define R2_WP_ERASE_SKIP 1
#define R2_ERR 2
#define R2_CARD_ERR 3
#define R2_CARD_ECC_FAIL 4
#define R2_WP_VIOLATION 5
#define R2_INVAL_ERASE 6
#define R2_OUT_OF_RANGE 7
#define R2_CSD_OVERWRITE 7
#define R2_IDLE_STATE (R1_IDLE_STATE + 8)
#define R2_ERASE_RESET (R1_ERASE_RESET + 8)
#define R2_ILL_COMMAND (R1_ILL_COMMAND + 8)
#define R2_COM_CRC_ERR (R1_COM_CRC_ERR + 8)
#define R2_ERASE_SEQ_ERR (R1_ERASE_SEQ_ERR + 8)
#define R2_ADDR_ERR (R1_ADDR_ERR + 8)
#define R2_PARAM_ERR (R1_PARAM_ERR + 8)
/* R3: size 5 bytes */
#define R3_OCR_MASK (0xffffffffUL)
#define R3_IDLE_STATE (R1_IDLE_STATE + 32)
#define R3_ERASE_RESET (R1_ERASE_RESET + 32)
#define R3_ILL_COMMAND (R1_ILL_COMMAND + 32)
#define R3_COM_CRC_ERR (R1_COM_CRC_ERR + 32)
#define R3_ERASE_SEQ_ERR (R1_ERASE_SEQ_ERR + 32)
#define R3_ADDR_ERR (R1_ADDR_ERR + 32)
#define R3_PARAM_ERR (R1_PARAM_ERR + 32)
/* Data Response: size 1 byte */
#define DR_STATUS_MASK 0x0e
#define DR_STATUS_ACCEPTED 0x05
#define DR_STATUS_CRC_ERR 0x0a
#define DR_STATUS_WRITE_ERR 0x0c

/* status bits for card types */
#define SD_RAW_SPEC_1 0
#define SD_RAW_SPEC_2 1
#define SD_RAW_SPEC_SDHC 2

#define MAX_RETRIES 10

/* card type state */
uint8_t mmcCardType;

static uint8_t mmc_send_command(uint8_t command, uint32_t arg) {
	uint8_t response;

	/* wait some clock cycles */
	spiTransfer(0xff);

	/* send command via SPI */
	spiTransfer(0x40 | command);
	spiTransfer((arg >> 24) & 0xff);
	spiTransfer((arg >> 16) & 0xff);
	spiTransfer((arg >> 8) & 0xff);
	spiTransfer((arg >> 0) & 0xff);
	switch (command) {
		case CMD_GO_IDLE_STATE:
			spiTransfer(0x95);
			break;
		case CMD_SEND_IF_COND:
			spiTransfer(0x87);
			break;
		default:
			spiTransfer(0xff);
			break;
	}

	/* receive response */
	for (uint8_t i = 0; i < 100; ++i) {
		response = spiTransfer(0xff);
		if (response != 0xff)
			break;
	}

	return response;
}

int8_t mmcInit(void) {
	uint8_t state = getIntState();

	cli();

	spiInit(32);

	/* initialization procedure */

	/* card needs 74 cycles minimum to start up */
	for (uint8_t i = 0; i < 10; ++i) {
		/* wait 8 clock cycles */
		spiTransfer(0xff);
	}

	/* address card */
	chipSelect(mmc, 1);

	/* reset card */
	uint8_t response;
	for (uint16_t i = 0;; ++i) {
		response = mmc_send_command(CMD_GO_IDLE_STATE, 0);
		if (response == (1 << R1_IDLE_STATE))
			break;

		if (i == 0x1ff) {
			chipSelect(mmc, 0);
			restoreIntState(state);
			return -1;
		}
	}

	/* check for version of SD card specification */
	response = mmc_send_command(CMD_SEND_IF_COND, 0x100 /* 2.7V - 3.6V */  | 0xaa /* test pattern */ );
	if ((response & (1 << R1_ILL_COMMAND)) == 0) {
		spiTransfer(0xff);
		spiTransfer(0xff);
		if ((spiTransfer(0xff) & 0x01) == 0) {
			chipSelect(mmc, 0);
			restoreIntState(state);
			return -2;	/* card operation voltage range doesn't match */
		}
		if (spiTransfer(0xff) != 0xaa) {
			chipSelect(mmc, 0);
			restoreIntState(state);
			return -3;	/* wrong test pattern */
		}

		/* card conforms to SD 2 card specification */
		mmcCardType = (1 << SD_RAW_SPEC_2);
	} else {
		/* determine SD/MMC card type */
		mmc_send_command(CMD_APP, 0);
		response = mmc_send_command(CMD_SD_SEND_OP_COND, 0);
		if ((response & (1 << R1_ILL_COMMAND)) == 0) {
			/* card conforms to SD 1 card specification */
			mmcCardType = (1 << SD_RAW_SPEC_1);
		} else {
			/* MMC card */
			mmcCardType = 0;
		}
	}

	/* wait for card to get ready */
	for (uint16_t i = 0;; ++i) {
		if (mmcCardType & ((1 << SD_RAW_SPEC_1) | (1 << SD_RAW_SPEC_2))) {
			uint32_t arg = 0;
			if (mmcCardType & (1 << SD_RAW_SPEC_2))
				arg = 0x40000000;
			mmc_send_command(CMD_APP, 0);
			response = mmc_send_command(CMD_SD_SEND_OP_COND, arg);
		} else {
			response = mmc_send_command(CMD_SEND_OP_COND, 0);
		}

		if ((response & (1 << R1_IDLE_STATE)) == 0)
			break;

		if (i == 0x7fff) {
			chipSelect(mmc, 0);
			restoreIntState(state);
			mmcCardType = (uint8_t)-1;
			return -4;
		}
	}

	if (mmcCardType & (1 << SD_RAW_SPEC_2)) {
		if (mmc_send_command(CMD_READ_OCR, 0)) {
			chipSelect(mmc, 0);
			restoreIntState(state);
			mmcCardType = (uint8_t)-1;
			return -5;
		}

		if (spiTransfer(0xff) & 0x40)
			mmcCardType |= (1 << SD_RAW_SPEC_SDHC);

		spiTransfer(0xff);
		spiTransfer(0xff);
		spiTransfer(0xff);
	}

	/* set block size to 512 bytes */
	if (mmc_send_command(CMD_SET_BLOCKLEN, 512)) {
		chipSelect(mmc, 0);
		restoreIntState(state);
		mmcCardType = (uint8_t)-1;
		return -6;
	}

	/* deaddress card */
	chipSelect(mmc, 0);
	restoreIntState(state);

	spiInit(16);

	return 0;
}

int8_t mmcReadSector(uint32_t addr, uint8_t *buffer) {
	uint8_t state = getIntState();

	if (mmcCardType == (uint8_t)-1)
		return -1;

	cli();

	/* address card */
	chipSelect(mmc, 1);

	/* send single block request */
	if (mmc_send_command(CMD_READ_SINGLE_BLOCK, (mmcCardType & (1 << SD_RAW_SPEC_SDHC) ? addr : (addr << 9)))) {
		chipSelect(mmc, 0);
		restoreIntState(state);
		mmcCardType = (uint8_t)-1;
		return -1;
	}

	/* wait for data block (start byte 0xfe) */
	while (spiTransfer(0xff) != 0xfe) ;

	for (uint16_t i = 0; i < 512; ++i) {
		*buffer++ = spiTransfer(0xff);
	}

	/* read crc16 */
	spiTransfer(0xff);
	spiTransfer(0xff);

	/* deaddress card */
	chipSelect(mmc, 0);

	/* let card some time to finish */
	spiTransfer(0xff);
	restoreIntState(state);

	return 0;
}

int8_t mmcWriteSector(uint32_t addr, const uint8_t *buffer, uint8_t pgm) {
	uint8_t state = getIntState();
	uint8_t res;

	if (mmcCardType == (uint8_t)-1)
		return -1;

	cli();

	/* address card */
	chipSelect(mmc, 1);

	/* send single block request */
	if (mmc_send_command(CMD_WRITE_SINGLE_BLOCK, (mmcCardType & (1 << SD_RAW_SPEC_SDHC) ? addr : (addr << 9)))) {
		chipSelect(mmc, 0);
		restoreIntState(state);
		mmcCardType = (uint8_t)-1;
		return -1;
	}

	/* send start byte */
	spiTransfer(0xfe);

	/* write byte block */
	for (uint16_t i = 0; i < 512; ++i) {
		if (buffer) {
			spiTransfer(X_read_byte(&(buffer[i]), pgm));
		} else {
			spiTransfer(0);
		}
	}

	/* write dummy crc16 */
	spiTransfer(0xff);
	spiTransfer(0xff);

	res = spiTransfer(0xff);

	/* wait while card is busy */
	while (spiTransfer(0xff) != 0xff) ;

	spiTransfer(0xff);

	/* deaddress card */
	chipSelect(mmc, 0);
	restoreIntState(state);

	return (res & 0x1f) == 0x05 ? 0 : -2;
}
