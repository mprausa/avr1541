/*
 *  fw/src/l2f50_display.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <hardware.h>
#include <chipselect.h>
#include <display.h>
#include <spi.h>
#include <avr/io.h>
#include <progmem.h>
#include <util/delay.h>
#include <interrupt.h>
#include <avr/eeprom.h>

#define CHAR_H 14
#define CHAR_W 8
#define TEXT_COL 16
#define TEXT_ROW 12

#define DATCTL 0xBC     // Data Control (data handling in RAM)
#define DISCTL 0xCA     // Display Control
#define GCP64 0xCB      // pulse set for 64 gray scale
#define GCP16 0xCC      // pulse set for 16 gray scale
#define OSSEL 0xD0      // Oscillator select
#define GSSET 0xCD      // set for gray scales
#define ASCSET 0xAA     // aerea scroll setting
#define SCSTART 0xAB        // scroll start setting
#define DISON 0xAF      // Display ON (no parameter)
#define DISOFF 0xAE     // Display OFF (no parameter)
#define DISINV 0xA7     // Display Invert (no parameter)
#define DISNOR 0xA6     // Display Normal (no parameter)
#define SLPIN  0x95     // Display Sleep (no parameter)
#define SLPOUT 0x94     // Display out of sleep (no parameter)
#define RAMWR 0x5C      // Display Memory write
#define PTLIN 0xA8      // partial screen write

#define SD_CSET 0x15        // column address setting
#define SD_PSET 0x75        // page address setting

#define SORT(n1,n2) if(n2<n1) {uint8_t tmp = n2; n2 = n1; n1 = tmp;}

static void lcd_cmd(uint8_t dat) {
    uint8_t state = getIntState();
    cli();

    LCD_RS_PORT &= ~_BV(LCD_RS);
    spiTransfer(dat);
    spiTransfer(0x00);
    LCD_RS_PORT |= _BV(LCD_RS);

    restoreIntState(state);
}

static void lcd_dat0(uint8_t dat) {
    spiTransfer(dat);
    spiTransfer(0x00);
}

static void lcd_dat16(uint16_t dat) {
    spiTransfer(dat >> 8);
    spiTransfer(dat);
}

static void lcd_cspulse(void) {
    uint8_t state = getIntState();
    cli();

    chipSelect(lcd, 0);
    asm volatile ("nop; nop; nop; nop");
    chipSelect(lcd, 1);

    restoreIntState(state);
}

void initDisplay() {
    uint8_t i;
    static const uint8_t disctl[9] PROGMEM = { 0x4C, 0x01, 0x53, 0x00, 0x02, 0xB4, 0xB0, 0x02, 0x00 };
    static const uint8_t gcp64_0[29] PROGMEM = { 0x11, 0x27, 0x3C, 0x4C, 0x5D, 0x6C, 0x78, 0x84, 0x90, 0x99, 0xA2,
        0xAA, 0xB2, 0xBA,
        0xC0, 0xC7, 0xCC, 0xD2, 0xD7, 0xDC, 0xE0, 0xE4, 0xE8, 0xED,
        0xF0, 0xF4, 0xF7, 0xFB,
        0xFE
    };
    static const uint8_t gcp64_1[34] PROGMEM = { 0x01, 0x03, 0x06, 0x09, 0x0B, 0x0E, 0x10, 0x13, 0x15, 0x17, 0x19,
        0x1C, 0x1E, 0x20,
        0x22, 0x24, 0x26, 0x28, 0x2A, 0x2C, 0x2D, 0x2F, 0x31, 0x33,
        0x35, 0x37, 0x39, 0x3B,
        0x3D, 0x3F, 0x42, 0x44, 0x47, 0x5E
    };
    static const uint8_t gcp16[15] PROGMEM = { 0x13, 0x23, 0x2D, 0x33, 0x38, 0x3C, 0x40, 0x43, 0x46, 0x48, 0x4A,
        0x4C, 0x4E, 0x50, 0x64
    };

    LCD_RESET_PORT &= ~_BV(LCD_RESET);
    LCD_RESET_DDR |= _BV(LCD_RESET);

    LCD_RS_PORT |= _BV(LCD_RS); // not used from LPH display
    LCD_RS_DDR |= _BV(LCD_RS);

    // generate clean display reset
    LCD_RESET_PORT &= ~_BV(LCD_RESET);  // reset display
    LCD_RS_PORT |= _BV(LCD_RS); // RS is set to high
    _delay_ms(10);
    LCD_RESET_PORT |= _BV(LCD_RESET);   // release reset
    _delay_ms(35);

    uint8_t state = getIntState();

    cli();

    chipSelect(lcd, 1); // select display

    lcd_cmd(DATCTL);
    lcd_dat0(0x2A);     // 0x2A=565 mode, 0x0A=666mode, 0x3A=444mode

    lcd_cspulse();

    lcd_cmd(DISCTL);
    for (i = 0; i < 9; i++) {
        lcd_dat0(pgm_read_byte(&disctl[i]));
    }

    lcd_cmd(GCP64);
    for (i = 0; i < 29; i++) {
        lcd_dat0(pgm_read_byte(&gcp64_0[i]));
        lcd_dat0(0x00);
    }
    for (i = 0; i < 34; i++) {
        lcd_dat0(pgm_read_byte(&gcp64_1[i]));
        lcd_dat0(0x01);
    }

    lcd_cmd(GCP16);
    for (i = 0; i < 15; i++) {
        lcd_dat0(pgm_read_byte(&gcp16[i]));
    }

    lcd_cmd(GSSET);
    lcd_dat0(0x00);

    lcd_cmd(OSSEL);
    lcd_dat0(0x00);

    lcd_cmd(SLPOUT);

//  _delay_ms(7);

    lcd_cmd(SD_CSET);
    lcd_dat0(0x08);
    lcd_dat0(0x01);
    lcd_dat0(0x8B);
    lcd_dat0(0x01);

    lcd_cmd(SD_PSET);
    lcd_dat0(0x00);
    lcd_dat0(0x8F);

    lcd_cmd(ASCSET);
    lcd_dat0(0x00);
    lcd_dat0(0xAF);
    lcd_dat0(0xAF);
    lcd_dat0(0x03);

    lcd_cmd(SCSTART);
    lcd_dat0(0x00);

    LCD_RS_PORT &= ~_BV(LCD_RS);
    lcd_dat0(DISON);

    chipSelect(lcd, 0); // deselect display

    restoreIntState(state);
}

void displayOff() {
    LCD_RESET_PORT &= ~_BV(LCD_RESET);  // reset display
}

void setPixel(uint8_t x, uint8_t y, uint16_t color) {
    drawFilledBox(x, y, x, y, color);
}

void drawHorizontal(uint8_t x1, uint8_t x2, uint8_t y, uint16_t color) {
    drawFilledBox(x1, y, x2, y, color);
}

void drawVertical(uint8_t x, uint8_t y1, uint8_t y2, uint16_t color) {
    drawFilledBox(x, y1, x, y2, color);
}

void drawBox(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint16_t color) {
    drawHorizontal(x1, x2, y1, color);
    drawHorizontal(x1, x2, y2, color);
    drawVertical(x1, y1, y2, color);
    drawVertical(x2, y1, y2, color);
}

void drawFilledBox(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint16_t color) {
    uint16_t i, j;
    uint8_t state = getIntState();

    cli();

    SORT(x1, x2);
    SORT(y1, y2);

    chipSelect(lcd, 1); // select display

    lcd_cmd(SD_CSET);
    lcd_dat0(8 + y1);   // startpoint (+8)
    lcd_dat0(0x01);
    lcd_dat0(8 + y2);   // endpoint (+8)
    lcd_dat0(0x01);
    lcd_cmd(SD_PSET);
    lcd_dat0(DISPLAY_WIDTH - x2 - 1);
    lcd_dat0(DISPLAY_WIDTH - x1 - 1);

    lcd_cmd(RAMWR);

    // start data transmission
    for (i = 0; i < (y2 - y1 + 1); i++) {
        for (j = 0; j < (x2 - x1 + 1); j++) {
            lcd_dat16(color);
        }
    }

    chipSelect(lcd, 0); // deselect display

    restoreIntState(state);
}

void fillScreen(uint16_t color) {
    drawFilledBox(0, 0, DISPLAY_WIDTH - 1, DISPLAY_HEIGHT - 1, color);
}

void blankScreen() {
    fillScreen(0x0000);
}

void drawPixmap(uint8_t x, uint8_t y, uint8_t sizeX, uint8_t sizeY, const uint16_t *pixels) {
    uint8_t _x, _y;
    uint8_t state = getIntState();

    cli();

    chipSelect(lcd, 1); // select display

    lcd_cmd(SD_CSET);
    lcd_dat0(8 + y);    // startpoint (+8)
    lcd_dat0(0x01);
    lcd_dat0(8 + y + sizeY - 1);    // endpoint (+8)
    lcd_dat0(0x01);
    lcd_cmd(SD_PSET);
    lcd_dat0(DISPLAY_WIDTH - x - sizeX);
    lcd_dat0(DISPLAY_WIDTH - x - 1);

    lcd_cmd(RAMWR);

    // start data transmission
    for (_x = sizeX; _x > 0; _x--) {
        for (_y = 0; _y < sizeY; _y++) {
            lcd_dat16(pgm_read_word(&pixels[_y * sizeX + _x - 1]));
        }
    }

    chipSelect(lcd, 0); // deselect display

    restoreIntState(state);
}

void drawPixmap24(uint8_t x, uint8_t y, uint8_t sizeX, uint8_t sizeY, const uint8_t *pixels) {
    uint8_t _x, _y;
    uint8_t state = getIntState();

    cli();

    chipSelect(lcd, 1); // select display

    lcd_cmd(SD_CSET);
    lcd_dat0(8 + y);    // startpoint (+8)
    lcd_dat0(0x01);
    lcd_dat0(8 + y + sizeY - 1);    // endpoint (+8)
    lcd_dat0(0x01);
    lcd_cmd(SD_PSET);
    lcd_dat0(DISPLAY_WIDTH - x - sizeX);
    lcd_dat0(DISPLAY_WIDTH - x - 1);

    lcd_cmd(RAMWR);

    // start data transmission
    for (_x = sizeX; _x > 0; _x--) {
        for (_y = 0; _y < sizeY; _y++) {
            union {
                struct {
                    uint8_t b:5;
                    uint8_t g:6;
                    uint8_t r:5;
                } bitfield;
                uint16_t value;
            } color;

            color.bitfield.r = pixels[(_y * sizeX + _x - 1) * 3 + 2] >> 3;
            color.bitfield.g = pixels[(_y * sizeX + _x - 1) * 3 + 1] >> 2;
            color.bitfield.b = pixels[(_y * sizeX + _x - 1) * 3 + 0] >> 3;

            lcd_dat16(color.value);
        }
    }

    chipSelect(lcd, 0); // deselect display

    restoreIntState(state);
}

void drawBitmap(uint8_t x, uint8_t y, uint8_t sizeX, uint8_t sizeY, uint16_t color, uint16_t bgcolor, const uint8_t *bitmap, uint8_t eep) {
    uint8_t _x, _y;
    uint8_t state = getIntState();

    cli();

    chipSelect(lcd, 1); // select display

    lcd_cmd(SD_CSET);
    lcd_dat0(8 + y);    // startpoint (+8)
    lcd_dat0(0x01);
    lcd_dat0(8 + y + sizeY - 1);    // endpoint (+8)
    lcd_dat0(0x01);
    lcd_cmd(SD_PSET);
    lcd_dat0(DISPLAY_WIDTH - x - sizeX);
    lcd_dat0(DISPLAY_WIDTH - x - 1);

    lcd_cmd(RAMWR);

    // start data transmission
    for (_x = sizeX; _x > 0; _x--) {
        for (_y = 0; _y < sizeY; _y++) {
            uint16_t pos = (uint16_t)_y * sizeX + _x - 1;
            uint8_t byte = eep ? eeprom_read_byte(&bitmap[pos >> 3]) : pgm_read_byte(&bitmap[pos >> 3]);
            if (byte & (1 << (7 - (pos & 7)))) {
                lcd_dat16(color);
            } else {
                lcd_dat16(bgcolor);
            }
        }
    }

    chipSelect(lcd, 0); // deselect display

    restoreIntState(state);
}
