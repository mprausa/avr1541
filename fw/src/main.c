/*
 *  fw/src/main.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <init.h>
#include <cpu6502.h>
#include <disk.h>
#include <statusbar.h>
#include <led.h>
#include <log.h>
#include <text.h>
#include <keys.h>
#include <progmem.h>

int main() {
    init();

    drawVersion();

    cpuMainLoop();

    return 0;
}

