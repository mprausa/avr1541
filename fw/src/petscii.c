/*
 *  fw/src/petscii.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <petscii.h>
#include <petscii-charset.h>
#include <display.h>
#include <progmem.h>

uint8_t strlen_petscii(const unsigned char *str, uint8_t pgm) {
    uint8_t n = 0;
    for (unsigned char c = X_read_byte(str, pgm); c && c != 0xa0; c = X_read_byte(&str[++n], pgm)) ;

    return n;
}

void renderPETSCII(uint8_t x, uint8_t y, uint16_t color, uint16_t bgcolor, const unsigned char *text, uint8_t pgm) {
    for (unsigned char c = X_read_byte(text, pgm); c && c != 0xa0; c = X_read_byte(++text, pgm), x += 8) {
        switch (c >> 5) {
            case 1:
                break;
            case 2:
            case 5:
                c -= 0x40;
                break;
            case 3:
                c -= 0x20;
                break;
            case 6:
            case 7:
                c -= 0x80;
                break;
            default:
                c = 0x20;   // space
        }

        drawBitmap(x, y, 8, 8, color, bgcolor, petsciiCharset[c], 0);
    }
}

void renderPETSCIILine(uint8_t y, uint16_t color, uint16_t bgcolor, const unsigned char *text, uint8_t pgm) {
    uint16_t len = strlen_petscii(text, pgm);

    if (len < 22) {
        renderPETSCII(0, y, color, bgcolor, text, pgm);
        drawFilledBox(len * 8, y, DISPLAY_WIDTH - 1, y + 7, bgcolor);
    } else {
        renderPETSCII(0, y, color, bgcolor, text, pgm);
    }
}
