/*
 *  fw/src/autoswap.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <autoswap.h>
#include <ext2.h>
#include <disk.h>
#include <statemachine.h>
#include <mainstate.h>
#include <string.h>
#include <errno.h>
#include <keys.h>
#include <hardware.h>
#include <text.h>
#include <display.h>
#include <led.h>

#include <progmem.h>
#include <avr/interrupt.h>

#define MAX_ENTRIES 10

static uint32_t swapList[MAX_ENTRIES];
uint8_t swapListSize=0;
static uint8_t swapListPos=0;

int8_t loadAutoSwap(const char *filename) {
    char buf[256];
    uint8_t len, pos = 0;
    struct ext2_file file;
    uint8_t eof = 0, skip = 0;

    if (ext2Open(&file, filename, 0))
        return -1;

    for (;;) {
        if (!eof && !(len = ext2Read(&file, buf + pos, 255 - pos))) {
            eof = 1;
        }

        buf[pos + len] = 0;

        char *nl = strchr(buf, '\n');
        if (!nl) {
            if (eof)
                break;
            skip = 1;
            pos = 0;
            continue;
        }

        *nl = 0;

        if (!skip) {
            uint32_t ino = ext2FindFile(buf,0);
            if (!errno) {
                swapList[swapListSize++] = ino;
                if (swapListSize == MAX_ENTRIES) break;
            } else {
                errno = 0;
            }
        }
        skip = 0;

        pos = 254 - (uint8_t)(nl - buf);
        memmove(buf, nl + 1, pos);
    }

    if (!swapListSize) return 0;

    swapListPos = 0;
    diskMountIno(swapList[0]);

    PCMSK3 |= _BV(PCINT29);
}

void disableAutoSwap() {
    swapListSize = 0;

    PCMSK3 &= ~_BV(PCINT29);
}

void renderSwapList() {
    for (uint8_t n=0; n<(swapListSize>5?5:swapListSize); ++n) {
        textPrintf(1, 18+n*16, 0xffff, (n==swapListPos)?0x9103:0x1221, _PSTR("% 2d"), n+1);
        drawBox(0,18+n*16,17,18+(n+1)*16,0x210c);
    }

    for (uint8_t n=5; n<swapListSize; ++n) {
        textPrintf(18, 18+(n-5)*16, 0xffff, (n==swapListPos)?0x9103:0x1221, _PSTR("% 2d"), n+1);
        drawBox(17,18+(n-5)*16,34,18+(n-4)*16,0x210c);
    }
}

ISR(PCINT3_vect) {
    uint8_t keys, rising, falling;

    if (stateHandler != mainStateWait) return;

    getKeys(&keys,&rising,&falling);

    if (keys) return;

    ledOn(GREEN);

    if (!swapListSize) {
        PCMSK3 &= ~_BV(PCINT29);
    }

    ++swapListPos;
    if (swapListPos == swapListSize) {
        swapListPos = 0;
    }

    diskMountIno(swapList[swapListPos]);

    stateHandler = mainState;
    stateMachine();

    PCIFR = _BV(PCIE3);
}

