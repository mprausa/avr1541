/*
 *  fw/src/utf8.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <utf8.h>
#include <progmem.h>

uint8_t strlen_utf8(const char *str, uint8_t pgm) {
    uint8_t len = 0;
    unsigned char c;

    for (uint8_t n = 0; c = X_read_byte(&str[n], pgm); len++) {
        if ((c & 0xe0) == 0xc0) {
            c = X_read_byte(&str[++n], pgm);
            if (!c)
                break;
            if ((c & 0xc0) != 0x80) {
                continue;
            }
        } else if ((c & 0xf0) == 0xe0) {
            c = X_read_byte(&str[++n], pgm);
            if (!c)
                break;
            if ((c & 0xc0) != 0x80) {
                continue;
            }
            c = X_read_byte(&str[++n], pgm);
            if (!c)
                break;
            if ((c & 0xc0) != 0x80) {
                --n;
                continue;
            }
        }
        ++n;
    }

    return len;
}

const char *shift_utf8(const char *str, uint8_t shift, uint8_t pgm) {
    while (shift--) {
        unsigned char c = X_read_byte(str, pgm);
        ++str;

        if ((c & 0xe0) == 0xc0) {
            c = X_read_byte(str, pgm);
            if (!c)
                break;
            if ((c & 0xc0) != 0x80) {
                continue;
            }
            ++str;
        } else if ((c & 0xf0) == 0xe0) {
            c = X_read_byte(str, pgm);
            if (!c)
                break;
            if ((c & 0xc0) != 0x80) {
                continue;
            }
            ++str;
            c = X_read_byte(str, pgm);
            if (!c)
                break;
            if ((c & 0xc0) != 0x80) {
                --str;
                continue;
            }
            ++str;
        }
    }
    return str;
}
