/*
 *  fw/src/bluescreen.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <bluescreen.h>
#include <display.h>
#include <text.h>
#include <led.h>
#include <string.h>
#include <progmem.h>
#include <util/delay.h>
#include <stdarg.h>

void bluescreen(const char *format, ...) {
    va_list args;
    va_start(args, format);

    fillScreen(0x001f);

    textPrintfV((DISPLAY_WIDTH - strlen_P(format) * 8) / 2, (DISPLAY_HEIGHT - 14) / 2, 0x001f, 0xffff, format, args);

    ledOn(0);
    for (;;) {
        ledToggle();
        _delay_ms(100);
    }

    va_end(args);
}
