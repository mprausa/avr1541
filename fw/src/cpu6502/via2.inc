/*
 *  fw/src/cpu6502/via2.inc
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __VIA2_INC
#define __VIA2_INC

#include "registers.inc"
#include "head.inc"
#include "led.inc"

.macro via2portBread reg
    call diskRotate

    lds \reg,via2portB
    andi \reg,0x6f

    lds tmp2,diskWriteProtection
    com tmp2
    lds tmp3,diskChanged
    tst tmp3
    breq 6f
    clr tmp3
    sts diskChanged,tmp3
    com tmp2
6:  bst tmp2,1
    bld \reg,4

    lds tmp2,rotSync
    or \reg,tmp2
.endm

.macro via2portBwrite reg
    bst \reg,3      ; Drive LED
    brts 6f
    ledOff
    rjmp 7f
6:  ledRed
7:

    lds tmp2,via2portB

    bst \reg,2              ; Drive Motor
    brts 6f
    bst tmp2,2              ; Motor Off
    brtc 8f

    push tmp2
    push \reg
    call diskRotate
    pop \reg
    pop tmp2
    rjmp 8f
6:  bst tmp2,2              ; Motor On
    brts 7f

    movw diskRotationStartL,cpuCyclesL
7:
    andi tmp2,3             ; Step Motor
    mov tmp,\reg
    inc tmp
    andi tmp,3

    cp tmp2,tmp
    brne 6f

    headMoveOut

    rjmp 8f
6:  mov tmp,\reg
    dec tmp
    andi tmp,3
    cp tmp2,tmp
    brne 8f

    headMoveIn
8:
    sts via2portB,\reg
.endm

.macro via2read reg
    cpi tmpPtrL,0x00
    brne 4f

    via2portBread \reg          ; Port B

    rjmp 5f
4:  cpi tmpPtrL,0x01
    brne 4f

    lds tmp2,via2InterruptFlagReg       ; Port A
    andi tmp2,~0x03
    sts via2InterruptFlagReg,tmp2

    call diskRotate
    call diskReadHelper
    mov \reg,tmp

    rjmp 5f
4:  cpi tmpPtrL,0x02
    brne 4f

    ldi \reg,0x6f               ; DDR B

    rjmp 5f
4:  cpi tmpPtrL,0x03
    brne 4f

    lds \reg,via2ddrA           ; DDR A

    rjmp 5f
4:  cpi tmpPtrL,0x04
    brne 4f

    lds tmp2,via2InterruptFlagReg       ; Timer 1 Low Byte
    andi tmp2,~0x40
    sts via2InterruptFlagReg,tmp2

    lds tmp2,TCNT3L
    lds \reg,OCR3BL
    sub \reg,tmp2

    rjmp 5f
4:  cpi tmpPtrL,0x05
    brne 4f

    cli                 ; Timer 1 High Byte
    lds tmp2,TCNT3L
    lds tmp3,TCNT3H
    lds tmp,OCR3BL
    sub tmp,tmp2
    lds \reg,OCR3BH
    sei
    sbc \reg,tmp3

    rjmp 5f
4:  cpi tmpPtrL,0x06
    brne 4f

    lds \reg,via2Timer1Latch        ; Timer 1-Latch Low Byte

    rjmp 5f
4:  cpi tmpPtrL,0x07
    brne 4f

    lds \reg,via2Timer1Latch+1      ; Timer 1-Latch High Byte

    rjmp 5f
4:  cpi tmpPtrL,0x0b
    brne 4f

    lds \reg,via2AuxControlReg      ; Auxiliary Control Register

    rjmp 5f
4:  cpi tmpPtrL,0x0c
    brne 4f

    lds \reg,via2PerControlReg      ; Peripheral Control Register

    rjmp 5f
4:  cpi tmpPtrL,0x0d
    brne 4f

    lds \reg,via2InterruptFlagReg       ; Interrupt Flag Register
    mov tmp2,\reg
    and tmp2,via2InterruptEnableReg
    breq 5f
    ori \reg,0x80

    rjmp 5f
4:  cpi tmpPtrL,0x0e
    brne 4f

    mov \reg,via2InterruptEnableReg     ; Interrupt Enable Register

    rjmp 5f
4:  ser \reg                ; unused registers
5:
.endm

.macro via2write reg
    cpi tmpPtrL,0x00
    brne 4f

    via2portBwrite \reg     ; Port B

    rjmp 5f
4:  cpi tmpPtrL,0x01
    brne 4f

    lds tmp2,via2InterruptFlagReg   ; Port A
    andi tmp2,~0x03
    sts via2InterruptFlagReg,tmp2

    sts via2portA,\reg

    rjmp 5f
4:  cpi tmpPtrL,0x03
    brne 4f

    sts via2ddrA,\reg       ; DDR A

    rjmp 5f
4:  cpi tmpPtrL,0x04
    brne 4f

    sts via2Timer1Latch,\reg    ; Timer 1 Low Byte

    rjmp 5f
4:  cpi tmpPtrL,0x05
    brne 4f

    sts via2Timer1Latch+1,\reg  ; Timer 1 High Byte
    lds tmp2,via2InterruptFlagReg
    andi tmp2,~0x40
    sts via2InterruptFlagReg,tmp2

    lds tmp3,via2Timer1Latch
    cli
    lds tmp2,TCNT3L
    add tmp2,tmp3
    lds tmp3,TCNT3H
    adc tmp3,\reg

    sts OCR3BH,tmp2
    sts OCR3BL,tmp3
    sei

    rjmp 5f
4:  cpi tmpPtrL,0x06
    brne 4f

    sts via2Timer1Latch,\reg    ; Timer 1-Latch Low Byte

    rjmp 5f
4:  cpi tmpPtrL,0x07
    brne 4f

    sts via2Timer1Latch+1,\reg  ; Timer 1-Latch High Byte
    lds tmp2,via2InterruptFlagReg
    andi tmp2,~0x40
    sts via2InterruptFlagReg,tmp2

    rjmp 5f
4:  cpi tmpPtrL,0x0b
    brne 4f

    sts via2AuxControlReg,\reg  ; Auxiliary Control Register

    rjmp 5f
4:  cpi tmpPtrL,0x0c
    brne 4f

    call diskRotate
    sts via2PerControlReg,\reg  ; Peripheral Control Register

    rjmp 5f
4:  cpi tmpPtrL,0x0d
    brne 4f

    lds tmp2,via2InterruptFlagReg   ; Interrupt Flag Register
    mov tmp3,\reg
    com tmp3
    and tmp2,tmp3
    sts via2InterruptFlagReg,tmp2

    rjmp 5f
4:  cpi tmpPtrL,0x0e
    brne 5f

    mov tmp2,\reg           ; Interrupt Enable Register
    bst tmp2,7
    brtc 4f
    andi tmp2,0x7f
    or via2InterruptEnableReg,tmp2
    rjmp 5f
4:  com tmp2
    and via2InterruptEnableReg,tmp2
5:
.endm

#endif //__VIA2_INC
