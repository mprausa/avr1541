/*
 *  fw/src/cpu6502/irq.inc
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __IRQ_INC
#define __IRQ_INC

#include "cpu.inc"
#include "helpers.inc"

.macro handleIRQ
    lds tmp,via1InterruptFlagReg
    and tmp,via1InterruptEnableReg  
    brne 6f
    lds tmp,via2InterruptFlagReg
    and tmp,via2InterruptEnableReg
    breq 5f
    
6:  getPC
    getSR

    st -regSP,regPCH
    st -regSP,regPCL
    st -regSP,flags6502

    interruptsDisabledState
    ori flags6502,0x04

    ldi regPCH,hi8(rom1541+0xfe67-0xc000)
    ldi regPCL,lo8(rom1541+0xfe67-0xc000)
    clr regPCramFlag
    
    adiw cpuCycles,7
5:
.endm

#endif //__IRQ_INC
