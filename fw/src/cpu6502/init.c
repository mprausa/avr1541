/*
 *  fw/src/cpu6502/init.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <hardware.h>

void cpuInit() {
    IEC_DDR = 0;
    IEC_PORT = 0;

    PCICR |= _BV(PCIE2);
    PCIFR |= _BV(PCIE2);
    PCMSK2 |= _BV(PCINT21); // PCINT21 -> ATN

    DDRD &= ~_BV(PD0);
    PORTD &= ~_BV(PD0);
    DDRB |= _BV(PB4);

    /* timer 3 */
    TCCR3A = 0;             // normal mode
    TCCR3B = _BV(CS30)|_BV(CS31)|_BV(CS32); // external clock on rising edge
    TCNT3 = 0;
    TIMSK3 = _BV(OCIE3B);
    TIFR3 = _BV(OCF3B);

    /* timer 0 */
    OCR0A = F_CPU/(2*1000000L)-1;       // 1 MHz
    OCR0B = 0;              // toggle on counter value 0
    TCCR0A = _BV(COM0B0)|_BV(WGM01);    // CTC, toggle on compare match
    TCCR0B = _BV(CS00);         // prescaler = 1
}

