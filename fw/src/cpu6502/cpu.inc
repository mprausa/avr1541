/*
 *  fw/src/cpu6502/cpu.inc
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CPU_INC
#define __CPU_INC

#include <avr/io.h>
#include "registers.inc"
#include "via1.inc"
#include "via2.inc"

.macro readByteRAM reg          ; 3 clocks
    subi tmpPtrH,hi8(-(ram1541))
    ld \reg,tmpPtr  
.endm

.macro readPtrRAM           ; 6 clocks
    subi tmpPtrH,hi8(-(ram1541))
    ld tmp,tmpPtr+
    ld tmp2,tmpPtr
    movw tmpPtrL,tmp
.endm
    
.macro readByteROM reg          ; 4 clocks
    subi tmpPtrH,hi8(-(rom1541-0xc000))
    lpm \reg,tmpPtr
.endm

.macro readByte reg         ; 9 - 16 clocks
    cpi tmpPtrH,0x08
    brcc 1f             
    readByteRAM \reg        ; RAM
    rjmp 2f
1:  cpi tmpPtrH,0x18
    breq 3f
    jmp 1f
3:  via1read \reg           ; VIA1
    rjmp 2f
1:  cpi tmpPtrH,0x1c
    breq 3f
    jmp 1f
3:  via2read \reg           ; VIA2
    rjmp 2f         
1:  readByteROM \reg        ; ROM
2:
.endm 

.macro writeByteRAM reg         ; 3 clocks
    subi tmpPtrH,hi8(-(ram1541))
    st tmpPtr,\reg
.endm

.macro writeByte reg            ; 9 clocks
    cpi tmpPtrH,0x08
    brcc 1f             
    writeByteRAM \reg       ; RAM
    rjmp 3f
1:  cpi tmpPtrH,0x18
    breq 2f
    jmp 1f
2:  via1write \reg          ; VIA1
    rjmp 3f
1:  cpi tmpPtrH,0x1c
    breq 2f
    jmp 3f
2:  via2write \reg          ; VIA2
3:
.endm 

.macro getInstructionByte reg       ; 6 - 8 clocks
    tst regPCramFlag
    breq 3f
    ld \reg,regPC+
    rjmp 4f
3:
    movw tmpPtrL,regPCL
    lpm \reg,tmpPtr+
    movw regPCL,tmpPtrL
4:
.endm

.macro getInstructionWord       ; 8 - 13 clocks 
    tst regPCramFlag
    breq 3f
    ld tmpPtrL,regPC+
    ld tmpPtrH,regPC+
    rjmp 4f
3:
    movw tmpPtrL,regPCL
    lpm tmp,tmpPtr+
    lpm tmp2,tmpPtr+
    movw regPCL,tmpPtrL
    mov tmpPtrL,tmp
    mov tmpPtrH,tmp2
4:
.endm

.macro getInstructionWord1Inc       ; 8 - 13 clocks 
    tst regPCramFlag
    breq 3f
    ld tmpPtrL,regPC+
    ld tmpPtrH,regPC
    rjmp 4f
3:
    movw tmpPtrL,regPCL
    lpm tmp,tmpPtr+
    lpm tmp2,tmpPtr
    movw regPCL,tmpPtrL
    mov tmpPtrL,tmp
    mov tmpPtrH,tmp2
4:
.endm

.macro getZeroPageAddress       ; 7 - 10 clocks
    tst regPCramFlag
    breq 1f
    clr tmpPtrH
    ld tmpPtrL,regPC+
    rjmp 2f
1:
    movw tmpPtrL,regPCL
    lpm tmp,tmpPtr+
    movw regPCL,tmpPtrL
    mov tmpPtrL,tmp
    clr tmpPtrH
2:
.endm

.macro addWithExtraCycle  reg       ; 3 - 5 clocks
    add tmpPtrL,\reg
    brcc 1f 
    inc tmpPtrH
    adiw cpuCycles,1
1:
.endm

.macro getPC                    ; 4 - 5 clocks
    tst regPCramFlag
    breq 1f
    subi regPCH,hi8(ram1541)        ; RAM
    rjmp 2f
1:  subi regPCH,hi8(rom1541-0xc000)     ; ROM
2:
.endm

.macro getSP reg                ; 2 clocks
    mov \reg,regSPL
    dec \reg
.endm

.macro setSP reg                ; 4 clocks
    mov regSPL,\reg
    clr regSPH
    subi regSPL,lo8(-(ram1541+0x100+1))
    sbci regSPH,hi8(-(ram1541+0x100+1))
.endm

.macro interruptsDisabledState
    sts TCCR1B,zero_reg ; disable timer 1
    sts TIMSK3,zero_reg ; disable timer 3 interrupts

    ldi tmp,~_BV(PCIE2) ; disable ATN interrupt
    lds tmp2,PCICR
    and tmp2,tmp
    sts PCICR,tmp2
.endm

.macro interruptsEnabledState
    ldi tmp,_BV(CS10)|_BV(CS12) ; enable timer 1
    sts TCCR1B,tmp

    ldi tmp,_BV(OCIE3B)     ; enable timer 3 output compare interrupt
    sts TIMSK3,tmp

    ldi tmp,_BV(PCIE2)      ; enable ATN interrupt
    lds tmp2,PCICR
    or tmp2,tmp
    sts PCICR,tmp2
.endm

.macro getSR
    bst flagsAvr,0      ; C
    bld flags6502,0
    bst flagsAvr,1      ; Z
    bld flags6502,1
    bst flagsAvr,2      ; N
    bld flags6502,7
    set
    bld flags6502,5     ; bit 5 is unused and must always be set
.endm

.macro setSR
    bst flags6502,0     ; C
    bld flagsAvr,0
    bst flags6502,1     ; Z
    bld flagsAvr,1
    bst flags6502,7     ; N
    bld flagsAvr,2

    bst flags6502,2
    brts 1f

    interruptsEnabledState
    rjmp 2f

1:  interruptsDisabledState
2:
.endm

#endif //__CPU_INC

