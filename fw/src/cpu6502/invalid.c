/*
 *  fw/src/cpu6502/invalid.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <log.h>
#include <bluescreen.h>
#include <progmem.h>

void invalid(uint16_t regPC, uint8_t regA, uint8_t regX, uint8_t regY, uint8_t regSR, uint8_t regSP, uint8_t cmd) {
    logPrintf(_PSTR("[%04x] invalid instruction 0x%02x\n"),regPC,cmd);
    logPrintf(_PSTR("reg dump: A:%02x X:%02x Y:%02x SR:%02x SP:%02x\n"),regA,regX,regY,regSR,regSP);
    bluescreen(_PSTR("inv inst @ 0x%04x"),regPC);
}

