/*
 *  fw/src/cpu6502/sync.inc
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SYNC_INC
#define __SYNC_INC

#include "cpu.inc"
#include "helpers.inc"

#include <hardware.h>

#ifndef DEBUG_SYNC
.macro syncCycles   ; 8 clocks
    sub syncLastCycles,cpuCyclesL
    sub syncLastTimer,syncLastCycles    ; syncLastTimer = syncLastTimer+(cpuCyclesL-syncLastCycles)

5:  lds tmp2,TCNT3L
    cp tmp2,syncLastTimer
    brmi 5b

    mov syncLastTimer,tmp2
    mov syncLastCycles,cpuCyclesL
.endm
#else
.macro syncCycles
    sub syncLastCycles,cpuCyclesL
    sub syncLastTimer,syncLastCycles    ; syncLastTimer = syncLastTimer+(cpuCyclesL-syncLastCycles)
    lds tmp2,TCNT3L

    cp tmp2,syncLastTimer

    brpl 5f
    cbi _SFR_IO_ADDR(LED_PORT),LED1     ; green: tmp2 < syncLastTimer
    sbi _SFR_IO_ADDR(LED_PORT),LED2
    rjmp 6f
5:

    cbi _SFR_IO_ADDR(LED_PORT),LED2     ; red: tmp2 >= syncLastTimer
    sbi _SFR_IO_ADDR(LED_PORT),LED1

6:  push tmp

    mov tmp,tmp2
    call testFunctionHelper

    mov tmp,syncLastTimer
    call testFunctionHelper

    pop tmp

    lds tmp2,TCNT3L


    mov syncLastTimer,tmp2
    mov syncLastCycles,cpuCyclesL
.endm
#endif

#endif //__SYNC_INC

