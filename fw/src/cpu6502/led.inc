/*
 *  fw/src/cpu6502/led.inc
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __LED_INC
#define __LED_INC

#include <hardware.h>

.macro ledOff
    sbi _SFR_IO_ADDR(LED_PORT),LED1
    sbi _SFR_IO_ADDR(LED_PORT),LED2
.endm

.macro ledGreen
    cbi _SFR_IO_ADDR(LED_PORT),LED1
    sbi _SFR_IO_ADDR(LED_PORT),LED2
.endm

.macro ledRed
    sbi _SFR_IO_ADDR(LED_PORT),LED1
    cbi _SFR_IO_ADDR(LED_PORT),LED2
.endm

#endif //__LED_INC

