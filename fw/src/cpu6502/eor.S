/*
 *  fw/src/cpu6502/eor.S
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cpu.inc"

.section .text

.macro eorMacro				; 6 clocks
	out _SFR_IO_ADDR(SREG),flagsAvr
	eor regA,tmp
	in flagsAvr,_SFR_IO_ADDR(SREG)
.endm
	
.global _EORv
_EORv:					; 16-19 clocks
	adiw cpuCycles,2

	getInstructionByte tmp
	eorMacro

	jmp cpuLoop

.global _EORz
_EORz:					; 21 - 25 clocks 
	adiw cpuCycles,2

	getZeroPageAddress
	readByteRAM tmp
	eorMacro

	jmp cpuLoop

.global _EORzx
_EORzx:					; 23 - 27 clocks
	adiw cpuCycles,3

	getZeroPageAddress

	add tmpPtrL,regX
	adc tmpPtrH,zero_reg

	readByteRAM tmp
	eorMacro

	jmp cpuLoop

.global _EORa
_EORa:					; 28 - 39 clocks
	adiw cpuCycles,4

	getInstructionWord

	readByte tmp
	eorMacro

	jmp cpuLoop

.global _EORax
_EORax:					; 33 - 44 clocks
	adiw cpuCycles,4

	getInstructionWord
	addWithExtraCycle regX
	readByte tmp
	eorMacro

	jmp cpuLoop

.global _EORay
_EORay:					; 33 - 44 clocks
	adiw cpuCycles,4	

	getInstructionWord
	addWithExtraCycle regY
	readByte tmp
	eorMacro

	jmp cpuLoop

.global _EORix
_EORix:					; 35 - 45 clocks
	adiw cpuCycles,6

	getZeroPageAddress

	add tmpPtrL,regX
	adc tmpPtrH,zero_reg

	readPtrRAM
	readByte tmp
	eorMacro

	jmp cpuLoop

.global _EORiy
_EORiy:					; 38 - 48 clocks
	adiw cpuCycles,5

	getZeroPageAddress
	readPtrRAM
	addWithExtraCycle regY	
	readByte tmp
	eorMacro

	jmp cpuLoop

