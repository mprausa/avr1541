/*
 *  fw/src/cpu6502/helpers.inc
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __HELPERS_INC
#define __HELPERS_INC

#include "cpu.inc"

.macro callInvalid
    getSR
    getPC

    getSP tmp
    mov r14,tmp         ; diskRotationStartL
    clr r15             ; diskRotationStartH

    movw r24,regPCL         ; cpuCycles
    clr r23             ; regPCramFlag
    mov r22,regA            ; flagsAvr
    mov r16,flags6502       ; regA
    clr r21             ; tmp2
    mov r20,regX            ; tmp
    clr r19             ; flags6502
;   mov r18,regY            ; regY
    clr r17             ; regX

    mov r12,cmd
    clr r13

    jmp invalid
.endm

.macro callCpuDump
    lds r10,TCNT3L
    lds tmp2,lastTimerCycles
    sub r10,tmp2

    push regPCH
    push regPCL
    push regPCramFlag
    push regSPH
    push regSPL
    push regA
    push regX
    push regY
    push flags6502
    push diskRotationStartL
    push diskRotationStartH
    push cpuCyclesH
    push cpuCyclesL

    mov r12, cpuCyclesL
    lds tmp,lastCpuCycles
    sub r12,tmp
    sts lastCpuCycles,cpuCyclesL
    clr r13

    getSR   
    getPC

    getSP tmp
    mov r14,tmp         ; diskRotationStartL
    clr r15             ; diskRotationStartH

    movw r24,regPCL         ; cpuCycles
    clr r23             ; regPCramFlag
    mov r22,regA            ; flagsAvr
    mov r16,flags6502       ; regA
    clr r21             ; tmp2
    mov r20,regX            ; tmp
    clr r19             ; flags6502
;   mov r18,regY            ; regY
    clr r17             ; regX

    clr r11             ; tmp3

    call cpuDump

    pop cpuCyclesL
    pop cpuCyclesH
    pop diskRotationStartH
    pop diskRotationStartL
    pop flags6502
    pop regY
    pop regX
    pop regA
    pop regSPL
    pop regSPH
    pop regPCramFlag
    pop regPCL
    pop regPCH

    lds tmp,TCNT3L
    sts lastTimerCycles,tmp
.endm

.macro callTestFunction reg
    push r18
    push r19
    push r20
    push r21
    push r22
    push r23
    push r24
    push r25
    push r26
    push r27
    push r28
    push r29
    push r30
    push r31

    mov r22,\reg

    getPC

    movw r24,regPCL

    call testFunction

    pop r31
    pop r30
    pop r29
    pop r28
    pop r27
    pop r26
    pop r25
    pop r24
    pop r23
    pop r22
    pop r21
    pop r20
    pop r19
    pop r18
.endm

#endif //__HELPERS_INC

