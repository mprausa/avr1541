/*
 *  fw/src/cpu6502/cpudebug.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <log.h>
#include <text.h>
#include <stdint.h>
#include <via2.h>
#include <led.h>

uint8_t lastCpuCycles, lastTimerCycles;

extern uint8_t ram1541[];

void cpuDump(uint16_t regPC, uint8_t regA, uint8_t regX, uint8_t regY, uint8_t regSR, uint8_t regSP, uint8_t cycles, uint8_t time) {
/*	static uint8_t enabled=0;

	if (regPC == 0xebe7) {
		enabled = 1;
		ledOn(GREEN);
	}

	if (!enabled) return;  */

	logPrintf(_PSTR("[%04x] A:%02x X:%02x Y:%02x SR:%02x SP:%02x c:%d t:%dus\n"),regPC,regA,regX,regY,regSR,regSP,cycles,time);
}

void testFunction(uint16_t regPC, uint8_t reg) {
	logPrintf(_PSTR("[%04x] reg: 0x%02x - via2PerControlReg = 0x%02x\n"),regPC,reg,via2PerControlReg);
}

