/*
 *  fw/src/cpu6502/head.inc
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __HEAD_INC
#define __HEAD_INC 

#include "registers.inc"

#define NUM_TRACKS 35

.macro headMoveIn
	lds tmp2,diskHead
	cpi tmp2,NUM_TRACKS*2
	breq 20f
	
	inc tmp2
	sts diskHead,tmp2
20:
.endm

.macro headMoveOut
	lds tmp2,diskHead
	cpi tmp2,2
	breq 20f
	
	dec tmp2
	sts diskHead,tmp2
20:
.endm

#endif //__HEAD_INC
