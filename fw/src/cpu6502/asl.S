/*
 *  fw/src/cpu6502/asl.S
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cpu.inc"

.section .text

.macro aslMacro reg         ; 8 clocks
    lsl \reg
    in flagsAvr,_SFR_IO_ADDR(SREG)
.endm

.global _ASL
_ASL:                   ; 13 clocks
    adiw cpuCycles, 2

    aslMacro regA

    jmp cpuLoop

.global _ASLz
_ASLz:                  ; 25 - 29 clocks
    adiw cpuCycles,5
    getZeroPageAddress
    readByteRAM tmp
    aslMacro tmp
    st tmpPtr,tmp   

    jmp cpuLoop

.global _ASLzx
_ASLzx:                 ; 27 - 31 clocks
    adiw cpuCycles,6
    getZeroPageAddress

    add tmpPtrL,regX
    adc tmpPtrH,zero_reg

    readByteRAM tmp
    aslMacro tmp
    st tmpPtr,tmp   

    jmp cpuLoop

.global _ASLa
_ASLa:                  ; 27 - 32 clocks 
    adiw cpuCycles,6

    getInstructionWord

    readByteRAM tmp
    aslMacro tmp
    st tmpPtr,tmp   

    jmp cpuLoop

.global _ASLax
_ASLax:                 ; 29 - 34 clocks
    adiw cpuCycles,7

    getInstructionWord

    add tmpPtrL,regX
    adc tmpPtrH,zero_reg

    readByteRAM tmp
    aslMacro tmp
    st tmpPtr,tmp   

    jmp cpuLoop

