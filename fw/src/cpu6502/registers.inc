/*
 *  fw/src/cpu6502/registers.inc
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __REGISTERS_INC
#define __REGISTERS_INC

; r2    via2InterruptEnableReg
; r3    via1InterruptEnableReg
; r4    syncLastCycles
; r5    syncLastTimer
; r6    via1Timer1OcrL
; r7    via1Timer1OcrH
; r8    tmp4
; r9    cmd
; r10   via1Timer1C
; r11   tmp3
; r12   free
; r13   free
; r14   diskRotationStartL
; r15   diskRotationStartH
; r16   regA
; r17   regX
; r18   regY
; r19   flags6502
; r20   tmp
; r21   tmp2
; r22   flagsAvr
; r23   regPCramFlag
; r24   cpuCyclesL
; r25   cpuCyclesH
; r26   regSPL
; r27   regSPH
; r28   regPCL
; r29   regPCH
; r30   tmpPtrL
; r31   tmpPtrH

#define regPC Y
#define regPCH YH
#define regPCL YL

#define regPCramFlag r23

#define regSP X             /* stack pointer + 1 */
#define regSPH XH
#define regSPL XL

#define flagsAvr r22            /* Z,C,N */
#define flags6502 r19           /* V,D,I */

#define regA r16
#define regX r17
#define regY r18
#define cpuCycles r24
#define cpuCyclesL r24
#define cpuCyclesH r25

#define syncLastCycles r4
#define syncLastTimer r5

#define diskRotationStartL r14
#define diskRotationStartH r15

#define via1InterruptEnableReg r3
#define via2InterruptEnableReg r2
#define via1Timer1OcrL r6
#define via1Timer1OcrH r7
#define via1Timer1C r10

#define cmd r9

#define tmp r20
#define tmp2 r21
#define tmp3 r11
#define tmp4 r8

#define tmpPtr Z
#define tmpPtrL r30
#define tmpPtrH r31

#define zero_reg r1

#endif //__REGISTERS_INC
