/*
 *  fw/src/cpu6502/via1.inc
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __VIA1_INC
#define __VIA1_INC

#include "registers.inc"
#include "sync.inc"
#include "helpers.inc"

#include <hardware.h>

.macro via1portBread reg
    syncCycles
    in tmp,_SFR_IO_ADDR(IEC_PIN)
    in tmp2,_SFR_IO_ADDR(IEC_DDR)
    com tmp
    or tmp2,tmp
    lds \reg,via1deviceNo;
    bst tmp2,IEC_DAT
    bld \reg,0
    bst tmp2,IEC_CLK
    bld \reg,2
    bst tmp2,IEC_ATN
    bld \reg,7

    lds tmp3,via1ddrB
    lds tmp2,via1portB
    and tmp2,tmp3
    com tmp3
    and \reg,tmp3
    or \reg,tmp2
.endm

.macro via1portBwrite reg
    mov tmp2,\reg

    sts via1portB,tmp2

    lds tmp,via1ddrB
    com tmp

    or tmp2,tmp

    clr tmp

    bst tmp2,1
    bld tmp,IEC_DAT
    bst tmp2,3
    bld tmp,IEC_CLK

    com tmp2
    bst tmp2,4
    bld tmp,IEC_ATNACK

    syncCycles
    out _SFR_IO_ADDR(IEC_DDR),tmp
.endm

.macro via1handleTimer reg
    bst via1Timer1C,2
    cp cpuCyclesL,via1Timer1OcrL
    cpc cpuCyclesH,via1Timer1OcrH
    in via1Timer1C,_SFR_IO_ADDR(SREG)

    brmi 6f
    brtc 6f

    ori \reg,0x40
    sts via1InterruptFlagReg,\reg

    lds tmp2,via1AuxControlReg
    bst tmp2,6
    brtc 6f

    lds tmp2,via1Timer1Latch
    lds tmp3,via1Timer1Latch+1
    movw via1Timer1OcrL,cpuCyclesL
    add via1Timer1OcrL,tmp2
    adc via1Timer1OcrH,tmp3
6:
.endm

.macro via1read reg
    cpi tmpPtrL,0x00
    brne 4f

    via1portBread \reg          ; Port B

    rjmp 5f
4:  cpi tmpPtrL,0x01
    brne 4f

    lds tmp2,via1InterruptFlagReg       ; Port A
    andi tmp2,~0x03
    sts via1InterruptFlagReg,tmp2

    ser \reg

    rjmp 5f
4:  cpi tmpPtrL,0x02
    brne 4f

    lds \reg,via1ddrB           ; DDR B

    rjmp 5f
4:  cpi tmpPtrL,0x03
    brne 4f

    ldi \reg,0x00               ; DDR A

    rjmp 5f
4:  cpi tmpPtrL,0x04
    brne 4f

    lds tmp2,via1InterruptFlagReg       ; Timer 1 Low Byte
    andi tmp2,~0x40
    sts via1InterruptFlagReg,tmp2

    mov \reg,via1Timer1OcrL
    sub \reg,cpuCyclesL

    rjmp 5f
4:  cpi tmpPtrL,0x05
    brne 4f

    mov tmp,via1Timer1OcrL          ; Timer 1 High Byte
    sub tmp,cpuCyclesL
    mov \reg,via1Timer1OcrH
    sbc \reg,cpuCyclesH

    rjmp 5f
4:  cpi tmpPtrL,0x06
    brne 4f

    lds \reg,via1Timer1Latch        ; Timer 1-Latch Low Byte

    rjmp 5f
4:  cpi tmpPtrL,0x07
    brne 4f

    lds \reg,via1Timer1Latch+1      ; Timer 1-Latch High Byte

    rjmp 5f
4:  cpi tmpPtrL,0x0b
    brne 4f

    lds \reg,via1AuxControlReg      ; Auxiliary Control Register

    rjmp 5f
4:  cpi tmpPtrL,0x0c
    brne 4f

    lds \reg,via1PerControlReg      ; Peripheral Control Register

    rjmp 5f
4:  cpi tmpPtrL,0x0d
    brne 4f

    lds \reg,via1InterruptFlagReg       ; Interrupt Flag Register

    via1handleTimer \reg

    mov tmp2,\reg
    and tmp2,via1InterruptEnableReg
    breq 5f
    ori \reg,0x80

    rjmp 5f
4:  cpi tmpPtrL,0x0e
    brne 4f

    mov \reg,via1InterruptEnableReg     ; Interrupt Enable Register

    rjmp 5f
4:  ser \reg                ; unused registers
5:
.endm

.macro via1write reg
    cpi tmpPtrL,0x00
    brne 4f

    via1portBwrite \reg     ; Port B

    rjmp 5f
4:  cpi tmpPtrL,0x01
    brne 4f

    lds tmp2,via1InterruptFlagReg   ; Port A
    andi tmp2,~0x03
    sts via1InterruptFlagReg,tmp2

    rjmp 5f
4:  cpi tmpPtrL,0x02
    brne 4f

    sts via1ddrB,\reg       ; DDR B

    rjmp 5f
4:  cpi tmpPtrL,0x04
    brne 4f

    sts via1Timer1Latch,\reg    ; Timer 1 Low Byte

    rjmp 5f
4:  cpi tmpPtrL,0x05
    brne 4f

    sts via1Timer1Latch+1,\reg  ; Timer 1 High Byte
    lds tmp2,via1InterruptFlagReg
    andi tmp2,~0x40
    sts via1InterruptFlagReg,tmp2

    lds tmp3,via1Timer1Latch
    movw via1Timer1OcrL,cpuCyclesL
    add via1Timer1OcrL,tmp3
    adc via1Timer1OcrH,\reg

    cp cpuCyclesL,via1Timer1OcrL
    cpc cpuCyclesH,via1Timer1OcrH
    in via1Timer1C,_SFR_IO_ADDR(SREG)

    rjmp 5f
4:  cpi tmpPtrL,0x06
    brne 4f

    sts via1Timer1Latch,\reg    ; Timer 1-Latch Low Byte

    rjmp 5f
4:  cpi tmpPtrL,0x07
    brne 4f

    sts via1Timer1Latch+1,\reg  ; Timer 1-Latch High Byte
    lds tmp2,via1InterruptFlagReg
    andi tmp2,~0x40
    sts via1InterruptFlagReg,tmp2

    rjmp 5f
4:  cpi tmpPtrL,0x0b
    brne 4f

    sts via1AuxControlReg,\reg  ; Auxiliary Control Register

    rjmp 5f
4:  cpi tmpPtrL,0x0c
    brne 4f

    sts via1PerControlReg,\reg  ; Peripheral Control Register

    rjmp 5f
4:  cpi tmpPtrL,0x0d
    brne 4f


    lds tmp2,via1InterruptFlagReg   ; Interrupt Flag Register
    mov tmp3,\reg
    com tmp3
    and tmp2,tmp3
    sts via1InterruptFlagReg,tmp2

    rjmp 5f
4:  cpi tmpPtrL,0x0e
    brne 5f

    mov tmp2,\reg           ; Interrupt Enable Register
    bst tmp2,7
    brtc 4f
    andi tmp2,0x7f
    or via1InterruptEnableReg,tmp2
    rjmp 5f
4:  com tmp2
    and via1InterruptEnableReg,tmp2
5:
.endm

#endif //__VIA1_INC

