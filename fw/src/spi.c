/*
 *  fw/src/spi.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <hardware.h>
#include <spi.h>
#include <avr/io.h>

void spiInit(uint8_t divider) {
    uint8_t tmp;

    SPI_DDR |= _BV(SS);
    SPI_PORT |= _BV(SS);

    SPI_PORT |= _BV(MOSI);
    SPI_DDR |= _BV(MOSI);

    SPI_PORT &= ~_BV(SCK);
    SPI_DDR |= _BV(SCK);

    SPI_PORT |= _BV(MISO);

    // setup SPI Interface
    switch (divider) {
        case 2:
            SPSR = _BV(SPI2X);
        case 4:
            SPCR = 0;
            break;
        case 8:
            SPSR = _BV(SPI2X);
        case 16:
            SPCR = _BV(SPR0);
            break;
        case 32:
            SPSR = _BV(SPI2X);
        case 64:
            SPCR = _BV(SPR1);
            break;
        case 128:
            SPCR = _BV(SPR0) | _BV(SPR1);
            break;
    }

    SPCR |= _BV(MSTR) | _BV(SPE);

    tmp = SPSR;
}

uint8_t spiTransfer(uint8_t c) {
    uint8_t res = 0;

    SPDR = c;
    while (!(SPSR & _BV(SPIF)))
        asm volatile ("nop");
    res = SPDR;

    return res;
}
