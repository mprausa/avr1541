/*
 *  fw/src/mbr.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <mbr.h>
#include <mmc.h>
#include <string.h>
#include <stdio.h>
#include <progmem.h>
#include <errno.h>
#include <sys/errno.h>

static uint32_t ext2Start;
static uint32_t ext2Size;

#ifdef DEBUG
static uint32_t debugPartStart = (uint32_t)-1;
static uint32_t debugPartSize;
#endif

static uint8_t buffer[512];
static uint32_t bufferSector = (uint32_t)-1;
static uint8_t bufferDirty = 0;

int8_t mbrFindPartition() {
    int8_t res;
    struct tab {
        uint8_t boot;
        uint8_t chsStart[3];
        uint8_t type;
        uint8_t chsEnd[3];
        uint32_t start;
        uint32_t size;
    } *parts;

    if (mmcReadSector(0, buffer))
        return -1;

    if (*(uint16_t *)(buffer + 0x1fe) != 0xaa55)
        return -2;

    parts = (struct tab *)(buffer + 0x1be);

    ext2Start = (uint32_t)-1;

    for (uint8_t n = 0; n < 4; ++n) {
        if (parts[n].type == 0x83 && ext2Start == (uint32_t)-1) {
            ext2Start = parts[n].start;
            ext2Size = parts[n].size;
        }
        if (parts[n].type == 0x80) {
            mmcWriteSector(parts[n].start, NULL, 0);    // overwrite firmware header
        }
#ifdef DEBUG
        if (parts[n].type == 0x82 && debugPartStart == (uint32_t)-1) {
            debugPartStart = parts[n].start;
            debugPartSize = parts[n].size;
        }
#endif
    }

    if (ext2Start == (uint32_t)-1)
        return -3;

    return 0;
}

int8_t mbrReadPartition(uint32_t sector, uint16_t offset, uint16_t size, uint8_t *buf) {
    sector += offset / 512;
    offset %= 512;

    while (size) {
        uint16_t read;
        int8_t res;

        if (offset + size > 512) {
            read = 512 - offset;
        } else {
            read = size;
        }

        if (bufferSector != sector + ext2Start) {
            if (bufferDirty) {
                if (res = mmcWriteSector(bufferSector, buffer, 0)) {
                    errno = EIO;
                    return res;
                }

                bufferDirty = 0;
            }

            if (res = mmcReadSector(sector + ext2Start, buffer)) {
                return res;
            }
            bufferSector = sector + ext2Start;
        }

        memcpy(buf, buffer + offset, read);

        buf += read;
        size -= read;
        ++sector;
        offset = 0;
    }

    return 0;
}

int8_t mbrWritePartition(uint32_t sector, uint16_t offset, uint16_t size, const uint8_t *buf, uint8_t pgm) {
    sector += offset / 512;
    offset %= 512;

    while (size) {
        uint16_t write;
        int8_t res;

        if (sector >= ext2Size) {
            errno = EIO;
            return -2;
        }

        if (offset + size > 512) {
            write = 512 - offset;
        } else {
            write = size;
        }

        if (write == 512) {
            if (res = mmcWriteSector(sector + ext2Start, buf, pgm)) {
                return res;
            }
        } else {
            if (bufferSector != sector + ext2Start) {
                if (bufferDirty) {
                    if (res = mmcWriteSector(bufferSector, buffer, 0)) {
                        errno = EIO;
                        return res;
                    }

                    bufferDirty = 0;
                }

                if (res = mmcReadSector(sector + ext2Start, buffer)) {
                    errno = EIO;
                    return res;
                }
                bufferSector = sector + ext2Start;
            }

            if (buf) {
                if (pgm)
                    memcpy_P(buffer + offset, buf, write);
                else
                    memcpy(buffer + offset, buf, write);
            } else {
                memset(buffer + offset, 0, write);
            }

            bufferDirty = 1;
        }

        if (buf)
            buf += write;
        size -= write;
        ++sector;
        offset = 0;
    }

    return 0;
}

int8_t mbrSync() {
    int8_t res = 0;

    if (bufferDirty) {
        res = mmcWriteSector(bufferSector, buffer, 0);
        if (!res)
            bufferDirty = 0;
    }

    return res;
}

#ifdef DEBUG
int8_t mbrDebugWrite(uint32_t addr, uint16_t size, const uint8_t *buf) {
    uint32_t sector = addr >> 9;
    uint16_t offset = addr & 0x1ff;
    const uint8_t *ptr;

    if (debugPartStart == (uint32_t)-1)
        return -1;

    while (size) {
        uint16_t write;
        int8_t res;

        if (sector >= debugPartSize)
            return -2;

        if (offset + size > 512) {
            write = 512 - offset;
        } else {
            write = size;
        }

        if (write == 512) {
            if (res = mmcWriteSector(sector + debugPartStart, buf, 0)) {
                return res;
            }
        } else {
            if (bufferSector != sector + debugPartStart) {
                if (bufferDirty) {
                    if (res = mmcWriteSector(bufferSector, buffer, 0)) {
                        return res;
                    }

                    bufferDirty = 0;
                }

                if (res = mmcReadSector(sector + debugPartStart, buffer)) {
                    return res;
                }
                bufferSector = sector + debugPartStart;
            }

            if (buf) {
                memcpy(buffer + offset, buf, write);
            } else {
                memset(buffer + offset, 0, write);
            }

            bufferDirty = 1;
        }

        if (buf)
            buf += write;
        size -= write;
        ++sector;
        offset = 0;
    }

    mbrSync();

    return 0;
}
#endif
