/*
 *  fw/src/statusbar.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <statusbar.h>
#include <text.h>
#include <petscii.h>
#include <display.h>
#include <version.h>

#include <progmem.h>

void statusBar(const char *str, uint8_t track, uint8_t sector) {
	uint8_t len=strlen_P(str)+6;
	uint8_t space = (176-(len<<3))>>1;
	textPrintf(space,117,0xffff,0x49d5,_PSTR("%S %02u,%02u"),str,track,sector);
	drawFilledBox(0,116,space-1,131,0x49d5);
	drawFilledBox(space+(len<<3),116,175,131,0x49d5);
	drawHorizontal(space,space+(len<<3),131,0x49d5);
	drawHorizontal(space,space+(len<<3),116,0x49d5);
}

void drawVersion() {
	uint8_t len, space;

	len = sizeof(__VERSION_STATUS)-1;
	space = (176-(len<<3))>>1;

	renderPETSCII(space,116,0xffff,0x49d5,_PSTR(__VERSION_STATUS),1);
	drawFilledBox(0,116,space-1,123,0x49d5);
	drawFilledBox(space+(len<<3),116,175,123,0x49d5);

	len = sizeof(__DATE)-1;
	space = (176-(len<<3))>>1;

	renderPETSCII(space,124,0xffff,0x49d5,_PSTR(__DATE),1);
	drawFilledBox(0,124,space-1,131,0x49d5);
	drawFilledBox(space+(len<<3),124,175,131,0x49d5);
}

void blankAllButStatus() {
	drawFilledBox(0, 0, 175, 115, 0x0000);
}


