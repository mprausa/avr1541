/*
 *  fw/src/menu.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <menu.h>
#include <statemachine.h>
#include <string.h>
#include <text.h>
#include <timer.h>
#include <display.h>
#include <keys.h>
#include <hardware.h>
#include <utf8.h>
#include <progmem.h>

static struct menu *currentMenu;

static uint8_t start, position;
static uint32_t scrollTicks;

static uint8_t titleScrollPos, entryScrollPos;
static enum { left, right } titleScrollDirection, entryScrollDirection;

void makeMenu(struct menu *menu) {
    currentMenu = menu;
    titleScrollPos = 0;
    titleScrollDirection = right;
    start = position = 0;
}

static void menuWait(uint8_t keys, uint8_t rising, uint8_t falling) {
    if (getTicks() - scrollTicks > 10) {
        scrollTicks = getTicks();

        if (strlen_utf8(currentMenu->title, currentMenu->pgm) > 22) {
            renderText(0, 0, 0x0000, 0x07e0, shift_utf8(currentMenu->title, titleScrollPos, currentMenu->pgm), currentMenu->pgm);
            if (titleScrollDirection == right) {
                if (titleScrollPos == strlen_utf8(currentMenu->title, currentMenu->pgm) - 22) {
                    titleScrollDirection = left;
                } else {
                    titleScrollPos++;
                }
            } else {
                if (titleScrollPos == 0) {
                    titleScrollDirection = right;
                } else {
                    titleScrollPos--;
                }
            }
        }
        if (strlen_utf8(currentMenu->entries[position].name, 1) > 22) {
            renderText(0, 16 + (position - start) * 16, 0x0000, 0xffff, shift_utf8(currentMenu->entries[position].name, entryScrollPos, 1), 1);
            if (entryScrollDirection == right) {
                if (entryScrollPos == strlen_utf8(currentMenu->entries[position].name, 1) - 22) {
                    entryScrollDirection = left;
                } else {
                    entryScrollPos++;
                }
            } else {
                if (entryScrollPos == 0) {
                    entryScrollDirection = right;
                } else {
                    entryScrollPos--;
                }
            }
        }
    }
    if (falling & KEY_UP) {
        if (position > 0) {
            --position;
            if (position < start) {
                start = position;
            }
            entryScrollPos = 0;
            entryScrollDirection = right;
            stateHandler = menuDraw;
        }
    }
    if (falling & KEY_DOWN) {
        if (position < currentMenu->nEntries - 1) {
            ++position;
            if (position >= start + 5) {
                start = position - 5;
            }
            entryScrollPos = 0;
            entryScrollDirection = right;
            stateHandler = menuDraw;
        }
    }
    if (falling & KEY_LEFT) {
        currentMenu->exitHandler();
    }
    if (falling & KEY_RIGHT) {
        currentMenu->entries[position].handler();
    }
}

void menuDraw(uint8_t keys, uint8_t rising, uint8_t falling) {
    renderTextLine(0, 0x0000, 0x07e0, shift_utf8(currentMenu->title, titleScrollPos, currentMenu->pgm), currentMenu->pgm);

    for (uint8_t n = start; n < currentMenu->nEntries && n < start + 6; ++n) {
        uint16_t color = 0xffff;
        uint16_t bgcolor = 0x0000;
        uint8_t scrollPos = 0;

        if (n == position) {
            color = 0x0000;
            bgcolor = 0xffff;
            scrollPos = entryScrollPos;
        }

        renderTextLine(16 + (n - start) * 16, color, bgcolor, currentMenu->entries[n].name + scrollPos, 1);
    }
    scrollTicks = getTicks();
    stateHandler = menuWait;
}

void stateMenu(uint8_t keys, uint8_t rising, uint8_t falling) {
    blankScreen();
    stateHandler = menuDraw;
}

uint16_t getMenuPosition() {
    return ((uint16_t)position << 8) | start;
}

void setMenuPosition(uint16_t pos) {
    position = (uint8_t)(pos >> 8);
    start = (uint8_t)pos;
}
