/*
 *  fw/src/timer.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <timer.h>
#include <hardware.h>

uint8_t volatile timerOverflows=0;

void initTimer() {
    TCCR1A = 0;             // normal mode
    TCCR1B = _BV(CS10)|_BV(CS12);       // prescaler = 1024 -> f = 19.5kHz, T = 51.2us
    TCNT1 = 0;
    TIMSK1 = _BV(TOIE1);
    TIFR1 = _BV(ICF1);
}

uint32_t getTicks() {
    return (((uint32_t)timerOverflows)<<16)|TCNT1;
}



