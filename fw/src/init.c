/*
 *  fw/src/init.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <init.h>
#include <hardware.h>
#include <chipselect.h>
#include <display.h>
#include <text.h>
#include <bluescreen.h>
#include <mmc.h>
#include <mbr.h>
#include <ext2.h>
#include <timer.h>
#include <spi.h>
#include <backlight.h>
#include <led.h>
#include <keys.h>
#include <statemachine.h>
#include <mainstate.h>
#include <interrupt.h>
#include <config.h>
#include <log.h>
#include <cpu6502.h>
#include <util/delay.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <progmem.h>

void init() {
    int8_t res;
    uint8_t keys, rising, falling;

    errno = 0;

    ledInit();
    ledOff();

    VGA_DDR &= ~VGA_MASK;
    VGA_PORT &= ~VGA_MASK;

    initKeys();

    PCICR |= _BV(PCIE3);
    PCIFR = _BV(PCIE3);
    PCMSK3 = 0;

    initCS();

    _delay_ms(300);

    spiInit(16);
    initTimer();

    initDisplay();
    blankScreen();

    initBacklight();
    setBacklight(130);

    sei();

    if (res = mmcInit()) {
        bluescreen(_PSTR(" mmc - res = %d "), res);
        for (;;) ;
    }

    if (res = mbrFindPartition()) {
        bluescreen(_PSTR(" mbr - res = %d "), res);
        for (;;) ;
    }

    if (res = ext2Mount()) {
        bluescreen(_PSTR(" ext2 - res = %d "), res);
        for (;;) ;
    }

    logInit();

    openFont();

    _delay_ms(500);

    getKeys(&keys,&rising,&falling);

    if (!isFontLoaded() || keys == (KEY_LEFT | KEY_UP)) {
        loadFont();
    }

    loadConfig();
    mountLastDisk();

    cpuInit();

    stateHandler = mainState;
}
