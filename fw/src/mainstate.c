/*
 *  fw/src/mainstate.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <mainstate.h>
#include <statemachine.h>
#include <statusbar.h>
#include <filebrowser.h>
#include <vgabox.h>
#include <config_menu.h>
#include <display.h>
#include <text.h>
#include <petscii.h>
#include <disk.h>
#include <hardware.h>
#include <bmp.h>
#include <autoswap.h>
#include <string.h>
#include <progmem.h>

void mainStateWait(uint8_t keys, uint8_t rising, uint8_t falling) {
    if (falling & KEY_RIGHT) {
        stateHandler = fileBrowser;
    }

    if (falling & KEY_UP) {
        stateHandler = vgaBox;
    }

    if (falling & KEY_DOWN) {
        makeConfigMenu();
    }

    if ((keys & (KEY_RIGHT | KEY_LEFT)) == (KEY_RIGHT | KEY_LEFT)) {
        if (diskFloppy.ino) {
            diskUMount();
            stateHandler = mainState;
        }

        ignoreKeys = 1;
    }
}

static void renderTitle() {
    uint8_t len = strlen_petscii(diskTitle, 0);

    if (len > 11) {
        uint8_t n;
        unsigned char str2[12];
        for (n = 0; n < len - 11; ++n) {
            str2[n] = diskTitle[n];
        }

        for (n = len - 11; n < 11 && diskTitle[n] != ' '; ++n) {
            str2[n] = diskTitle[n];
        }
        str2[n] = 0;

        renderPETSCII(38 + 4 + (11 - n) * 4, 8 + 11, 0x0000, 0xffff, str2, 0);
        if (n < 11)
            ++n;
        renderPETSCII(38 + 4 + (11 - (len - n)) * 4, 8 + 19, 0x0000, 0xffff, diskTitle + n, 0);
    } else {
        renderPETSCII(38 + 4 + (11 - len) * 4, 8 + 15, 0x0000, 0xffff, diskTitle, 0);
    }
}

void mainState(uint8_t keys, uint8_t rising, uint8_t falling) {
    blankAllButStatus();

    if (diskFloppy.ino) {
        showBmpPath(_PSTR("/.c64/floppy.bmp"), 38, 8, topLeft, 0, 0);

        renderTitle();
    } else {
        showBmpPath(_PSTR("/.c64/commodore.bmp"), 34, 8, topLeft, 0, 0);
    }

    renderSwapList();

    stateHandler = mainStateWait;
}
