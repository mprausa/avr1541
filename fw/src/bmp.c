/*
 *  fw/src/bmp.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <bmp.h>
#include <ext2.h>
#include <display.h>
#include <string.h>
#include <errno.h>

struct bmphdr {
    uint16_t magic;
    uint32_t filesize;
    uint32_t __reserved;
    uint32_t offset;

    uint32_t size;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bitcount;
    uint32_t compression;
} __attribute__ ((packed));

int8_t showBmpIno(uint32_t ino, uint8_t posX, uint8_t posY, enum corner crn, uint8_t maxX, uint8_t maxY) {
    struct ext2_file bmp;
    int32_t w, h;
    uint16_t len;
    uint8_t pad;
    uint8_t x, y;
    int16_t pos = 0;

    uint8_t buf[256];
    struct bmphdr *hdr;

    if (ext2ReadInode(&bmp, ino))
        return -1;

    if (ext2Read(&bmp, buf, sizeof(struct bmphdr)) != sizeof(struct bmphdr))
        return -2;

    hdr = (struct bmphdr *)buf;

    if (hdr->magic != 0x4d42)
        return -3;

    if (hdr->bitcount != 24)
        return -4;
    if (hdr->compression != 0)
        return -5;

    w = hdr->width;
    h = hdr->height;

    if (!maxX)
        maxX = DISPLAY_WIDTH;
    if (!maxY)
        maxY = DISPLAY_HEIGHT;

    if (w > maxX)
        return -3;
    if ((h < 0 ? -h : h) > maxY)
        return -5;

    bmp.offset = hdr->offset;

    switch (crn) {
        case topRight:
            posX -= w - 1;
            break;
        case bottomRight:
            posX -= w - 1;
        case bottomLeft:
            posY -= h - 1;
            break;
    }

    x = y = 0;

    pad = (4 - ((w * 3) & 3)) & 3;

    while (len = ext2Read(&bmp, buf + pos, 256 - pos)) {
        int16_t startPos = -1;
        uint8_t startX;

        len += pos;
        pos = 0;

        while (pos + 2 < len) {
            if (!buf[pos] && !buf[pos + 1] && !buf[pos + 2]) {  // black
                if (startPos >= 0) {
                    drawPixmap24(posX + startX, posY + (h < 0 ? y : h - 1 - y), x - startX, 1, buf + startPos);
                    startPos = -1;
                }
            } else if (startPos < 0) {
                startPos = pos;
                startX = x;
            }

            ++x;
            pos += 3;

            if (x == w) {
                if (startPos >= 0) {
                    drawPixmap24(posX + startX, posY + (h < 0 ? y : h - 1 - y), x - startX, 1, buf + startPos);
                    startPos = -1;
                }

                ++y;
                x = 0;
                pos += pad;
            }
        }

        if (startPos >= 0) {
            drawPixmap24(posX + startX, posY + (h < 0 ? y : h - 1 - y), x - startX, 1, buf + startPos);
        }

        if (len >= 4 && len > pos) {
            memcpy(buf, buf + pos, len - pos);
            pos = len - pos;
        } else {
            bmp.offset += pos - len;
            pos = 0;
        }
    }

    return 0;
}

int8_t showBmpPath(const char *path, uint8_t posX, uint8_t posY, enum corner crn, uint8_t maxX, uint8_t maxY) {
    uint32_t ino = ext2FindFile(path, 1);

    if (errno) {
        errno = 0;
        return -1;
    }

    return showBmpIno(ino, posX, posY, crn, maxX, maxY);
}
