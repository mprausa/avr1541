/*
 *  fw/src/vgabox.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vgabox.h>
#include <hardware.h>
#include <text.h>
#include <display.h>
#include <statemachine.h>
#include <mainstate.h>
#include <timer.h>
#include <statusbar.h>
#include <progmem.h>
#include <util/delay.h>

struct vgaKey {
    uint8_t y;
    uint8_t key;
    const char *str;
};

static const char PROGSTR __key_AVSV[] = "AV/SV";
static const char PROGSTR __key_PC[] = "PC";
static const char PROGSTR __key_PIP[] = "PIP";
static const char PROGSTR __key_MODE[] = "MODE";
static const char PROGSTR __key_PP[] = "PP";

static struct vgaKey vgaKeys[] = {
    {24, VGA_AVSV, __key_AVSV},
    {40, VGA_PC, __key_PC},
    {56, VGA_PIP, __key_PIP},
    {72, VGA_MODE, __key_MODE},
    {96, VGA_PP, __key_PP}
};

#define KEYS 5

static uint8_t key = 0;

static const uint8_t PROGMEM button[] = {
    0x00, 0x00,     // ........ ........
    0x07, 0xe0,     // .....xxx xxx.....
    0x08, 0x10,     // ....x... ...x....
    0x10, 0x08,     // ...x.... ....x...
    0x20, 0x04,     // ..x..... .....x..
    0x40, 0x02,     // .x...... ......x.
    0x40, 0x02,     // .x...... ......x.
    0x40, 0x02,     // .x...... ......x.
    0x40, 0x02,     // .x...... ......x.
    0x40, 0x02,     // .x...... ......x.
    0x40, 0x02,     // .x...... ......x.
    0x20, 0x04,     // ..x..... .....x..
    0x10, 0x08,     // ...x.... ....x...
    0x08, 0x10,     // ....x... ...x....
    0x07, 0xe0,     // .....xxx xxx.....
    0x00, 0x00      // ........ ........
};

static const uint8_t PROGMEM buttonPressed[] = {
    0x00, 0x00,     // ........ ........
    0x07, 0xe0,     // .....xxx xxx.....
    0x08, 0x10,     // ....x... ...x....
    0x10, 0x08,     // ...x.... ....x...
    0x23, 0xc4,     // ..x...xx xx...x..
    0x47, 0xe2,     // .x...xxx xxx...x.
    0x4f, 0xf2,     // .x..xxxx xxxx..x.
    0x4f, 0xf2,     // .x..xxxx xxxx..x.
    0x4f, 0xf2,     // .x..xxxx xxxx..x.
    0x4f, 0xf2,     // .x..xxxx xxxx..x.
    0x47, 0xe2,     // .x...xxx xxx...x.
    0x23, 0xc4,     // ..x...xx xx...x..
    0x10, 0x08,     // ...x.... ....x...
    0x08, 0x10,     // ....x... ...x....
    0x07, 0xe0,     // .....xxx xxx.....
    0x00, 0x00      // ........ ........
};

static const uint8_t PROGMEM buttonArrowUp[] = {
    0x00, 0x00,     // ........ ........
    0x07, 0xe0,     // .....xxx xxx.....
    0x08, 0x10,     // ....x... ...x....
    0x11, 0x88,     // ...x...x x...x...
    0x23, 0xc4,     // ..x...xx xx...x..
    0x47, 0xe2,     // .x...xxx xxx...x.
    0x4f, 0xf2,     // .x..xxxx xxxx..x.
    0x41, 0x82,     // .x.....x x.....x.
    0x41, 0x82,     // .x.....x x.....x.
    0x41, 0x82,     // .x.....x x.....x.
    0x41, 0x82,     // .x.....x x.....x.
    0x21, 0x84,     // ..x....x x....x..
    0x11, 0x88,     // ...x...x x...x...
    0x08, 0x10,     // ....x... ...x....
    0x07, 0xe0,     // .....xxx xxx.....
    0x00, 0x00      // ........ ........
};

static const uint8_t PROGMEM buttonArrowDown[] = {
    0x00, 0x00,     // ........ ........
    0x07, 0xe0,     // .....xxx xxx.....
    0x08, 0x10,     // ....x... ...x....
    0x11, 0x88,     // ...x...x x...x...
    0x21, 0x84,     // ..x....x x....x..
    0x41, 0x82,     // .x.....x x.....x.
    0x41, 0x82,     // .x.....x x.....x.
    0x41, 0x82,     // .x.....x x.....x.
    0x41, 0x82,     // .x.....x x.....x.
    0x4f, 0xf2,     // .x..xxxx xxxx..x.
    0x47, 0xe2,     // .x...xxx xxx...x.
    0x23, 0xc4,     // ..x...xx xx...x..
    0x11, 0x88,     // ...x...x x...x...
    0x08, 0x10,     // ....x... ...x....
    0x07, 0xe0,     // .....xxx xxx.....
    0x00, 0x00      // ........ ........
};

static const uint8_t PROGMEM buttonArrowRight[] = {
    0x00, 0x00,     // ........ ........
    0x07, 0xe0,     // .....xxx xxx.....
    0x08, 0x10,     // ....x... ...x....
    0x10, 0x88,     // ...x.... x...x...
    0x20, 0xc4,     // ..x..... xx...x..
    0x40, 0xe2,     // .x...... xxx...x.
    0x40, 0xf2,     // .x...... xxxx..x.
    0x5f, 0xfa,     // .x.xxxxx xxxxx.x.
    0x5f, 0x8a,     // .x.xxxxx xxxxx.x.
    0x40, 0xf2,     // .x...... xxxx..x.
    0x40, 0xe2,     // .x...... xx....x.
    0x20, 0xc4,     // ..x..... xx...x..
    0x10, 0x88,     // ...x.... x...x...
    0x08, 0x10,     // ....x... ...x....
    0x07, 0xe0,     // .....xxx xxx.....
    0x00, 0x00      // ........ ........
};

static const uint8_t PROGMEM buttonArrowLeft[] = {
    0x00, 0x00,     // ........ ........
    0x07, 0xe0,     // .....xxx xxx.....
    0x08, 0x10,     // ....x... ...x....
    0x11, 0x08,     // ...x...x ....x...
    0x23, 0x04,     // ..x...xx .....x..
    0x47, 0x02,     // .x...xxx ......x.
    0x4f, 0x02,     // .x..xxxx ......x.
    0x5f, 0xfa,     // .x.xxxxx xxxxx.x.
    0x5f, 0xfa,     // .x.xxxxx xxxxx.x.
    0x4f, 0x02,     // .x..xxxx ......x.
    0x47, 0x02,     // .x...xxx ......x.
    0x23, 0x04,     // ..x...xx .....x..
    0x11, 0x08,     // ...x...x ....x...
    0x08, 0x10,     // ....x... ...x....
    0x07, 0xe0,     // .....xxx xxx.....
    0x00, 0x00      // ........ ........
};

static const uint8_t PROGMEM vgaSymbol[] = {
    0x1f, 0xff, 0xff, 0xff, 0xf8,   // ...xxxxx xxxxxxxx xxxxxxxx xxxxxxxx xxxxx...
    0x20, 0x00, 0x00, 0x00, 0x04,   // ..x..... ........ ........ ........ .....x..
    0x40, 0x00, 0x00, 0x00, 0x02,   // .x...... ........ ........ ........ ......x.
    0x80, 0x00, 0x00, 0x00, 0x01,   // x....... ........ ........ ........ .......x
    0x80, 0x84, 0x21, 0x08, 0x01,   // x....... x....x.. ..x....x ....x... .......x
    0x81, 0xce, 0x73, 0x9c, 0x01,   // x......x xx..xxx. .xxx..xx x..xxx.. .......x
    0x80, 0x84, 0x21, 0x08, 0x01,   // x....... x....x.. ..x....x ....x... .......x
    0x80, 0x00, 0x00, 0x00, 0x01,   // x....... ........ ........ ........ .......x
    0x80, 0x10, 0x84, 0x21, 0x01,   // x....... ...x.... x....x.. ..x....x .......x
    0x40, 0x39, 0xce, 0x73, 0x82,   // .x...... ..xxx..x xx..xxx. .xxx..xx x.....x.
    0x40, 0x10, 0x84, 0x21, 0x02,   // .x...... ...x.... x....x.. ..x....x ......x.
    0x40, 0x00, 0x00, 0x00, 0x02,   // .x...... ........ ........ ........ ......x.
    0x40, 0x84, 0x21, 0x08, 0x02,   // .x...... x....x.. ..x....x ....x... ......x.
    0x41, 0xce, 0x73, 0x9c, 0x02,   // .x.....x xx..xxx. .xxx..xx x..xxx.. ......x.
    0x40, 0x84, 0x21, 0x08, 0x02,   // .x...... x....x.. ..x....x ....x... ......x.
    0x20, 0x00, 0x00, 0x00, 0x04,   // ..x..... ........ ........ ........ .....x..
    0x20, 0x00, 0x00, 0x00, 0x04,   // ..x..... ........ ........ ........ .....x..
    0x10, 0x00, 0x00, 0x00, 0x08,   // ...x.... ........ ........ ........ ....x...
    0x0c, 0x00, 0x00, 0x00, 0x30,   // ....xx.. ........ ........ ........ ..xx....
    0x03, 0xff, 0xff, 0xff, 0xc0    // ......xx xxxxxxxx xxxxxxxx xxxxxxxx xx......
};

static void nextKey() {
    if (key >= KEYS - 1)
        return;

    renderText(96, vgaKeys[key].y, 0xffff, 0x0000, vgaKeys[key].str, 1);
    drawBitmap(152, vgaKeys[key].y, 16, 16, 0xffff, 0x0000, button, 0);

    ++key;

    renderText(96, vgaKeys[key].y, 0xf800, 0x0000, vgaKeys[key].str, 1);
    drawBitmap(152, vgaKeys[key].y, 16, 16, 0xf800, 0x0000, button, 0);
}

static void prevKey() {
    if (!key)
        return;

    renderText(96, vgaKeys[key].y, 0xffff, 0x0000, vgaKeys[key].str, 1);
    drawBitmap(152, vgaKeys[key].y, 16, 16, 0xffff, 0x0000, button, 0);

    --key;

    renderText(96, vgaKeys[key].y, 0xf800, 0x0000, vgaKeys[key].str, 1);
    drawBitmap(152, vgaKeys[key].y, 16, 16, 0xf800, 0x0000, button, 0);
}

static uint8_t risen = 0;
static uint32_t menuTicks;

static void vgaMenuState(uint8_t keys, uint8_t rising, uint8_t falling);

static void drawButtons() {
    drawBitmap(136, 64, 16, 16, 0xffff, 0x0000, button, 0); // up
    drawBitmap(136, 96, 16, 16, 0xffff, 0x0000, button, 0); // down

    drawBitmap(120, 80, 16, 16, 0xffff, 0x0000, button, 0); // left
    drawBitmap(152, 80, 16, 16, 0xffff, 0x0000, button, 0); // right
}

static void vgaMenuStateWait(uint8_t keys, uint8_t rising, uint8_t falling) {
    uint8_t mask = 0;

    if (((getTicks() - menuTicks) & TIMER_MASK) > 78125) {
        stateHandler = vgaBox;
        return;
    }

    if (rising) {
        risen = 1;
    }

    if (!risen)
        return;

    if ((keys & (KEY_UP | KEY_DOWN)) == (KEY_UP | KEY_DOWN)) {
        VGA_DDR = (VGA_DDR & (~VGA_MASK)) | _BV(VGA_MENU);
        _delay_ms(100);
        VGA_DDR &= ~VGA_MASK;

        drawButtons();
        ignoreKeys = 1;

        return;
    }

    if ((keys & (KEY_LEFT | KEY_RIGHT)) == (KEY_LEFT | KEY_RIGHT)) {
        stateHandler = vgaBox;
        ignoreKeys = 1;
        return;
    }

    if (keys) {
        menuTicks = getTicks();
    }

    if (rising & KEY_LEFT) {
        drawBitmap(120, 80, 16, 16, 0xf800, 0x0000, buttonArrowLeft, 0);
    }

    if (falling & KEY_LEFT) {
        drawBitmap(120, 80, 16, 16, 0xffff, 0x0000, button, 0);
    }

    if (rising & KEY_RIGHT) {
        drawBitmap(152, 80, 16, 16, 0xf800, 0x0000, buttonArrowRight, 0);
    }

    if (falling & KEY_RIGHT) {
        drawBitmap(152, 80, 16, 16, 0xffff, 0x0000, button, 0);
    }

    if (rising & KEY_UP) {
        drawBitmap(136, 64, 16, 16, 0xf800, 0x0000, buttonArrowUp, 0);
    }

    if (falling & KEY_UP) {
        drawBitmap(136, 64, 16, 16, 0xffff, 0x0000, button, 0);
    }

    if (rising & KEY_DOWN) {
        drawBitmap(136, 96, 16, 16, 0xf800, 0x0000, buttonArrowDown, 0);
    }

    if (falling & KEY_DOWN) {
        drawBitmap(136, 96, 16, 16, 0xffff, 0x0000, button, 0);
    }

    if (keys & KEY_UP) {
        mask |= _BV(VGA_AVSV);
    }

    if (keys & KEY_DOWN) {
        mask |= _BV(VGA_PC);
    }

    if (keys & KEY_LEFT) {
        mask |= _BV(VGA_MODE);
    }

    if (keys & KEY_RIGHT) {
        mask |= _BV(VGA_PIP);
    }

    VGA_DDR = (VGA_DDR & (~VGA_MASK)) | mask;
}

static void vgaMenuState(uint8_t keys, uint8_t rising, uint8_t falling) {
    blankAllButStatus();

    drawBitmap(8, 8, 40, 20, 0x001f, 0x0000, vgaSymbol, 0);

    drawButtons();

    risen = 0;

    VGA_DDR = (VGA_DDR & (~VGA_MASK)) | _BV(VGA_MENU);
    _delay_ms(100);
    VGA_DDR &= ~VGA_MASK;

    menuTicks = getTicks();

    stateHandler = vgaMenuStateWait;
}

static void vgaWait(uint8_t keys, uint8_t rising, uint8_t falling) {
    uint8_t mask = 0;

    if ((keys & (KEY_LEFT | KEY_RIGHT)) == (KEY_LEFT | KEY_RIGHT)) {
        stateHandler = mainState;
        ignoreKeys = 1;
        return;
    }

    if (falling & KEY_DOWN) {
        nextKey();
    }

    if (falling & KEY_UP) {
        prevKey();
    }

    if (falling & KEY_LEFT) {
        stateHandler = vgaMenuState;
        return;
    }

    if (rising & KEY_RIGHT) {
        drawBitmap(152, vgaKeys[key].y, 16, 16, 0xf800, 0x0000, buttonPressed, 0);
    }

    if (falling & KEY_RIGHT) {
        drawBitmap(152, vgaKeys[key].y, 16, 16, 0xf800, 0x0000, button, 0);
    }

    if (keys & KEY_RIGHT) {
        mask = _BV(vgaKeys[key].key);
    }

    VGA_DDR = (VGA_DDR & (~VGA_MASK)) | mask;
}

void vgaBox(uint8_t keys, uint8_t rising, uint8_t falling) {
    blankAllButStatus();

    key = 0;

    drawBitmap(8, 8, 40, 20, 0x001f, 0x0000, vgaSymbol, 0);

    for (uint8_t n = 0; n < KEYS; ++n) {
        renderText(96, vgaKeys[n].y, n ? 0xffff : 0xf800, 0x0000, vgaKeys[n].str, 1);
        drawBitmap(152, vgaKeys[n].y, 16, 16, n ? 0xffff : 0xf800, 0x0000, button, 0);
    }

    stateHandler = vgaWait;
}
