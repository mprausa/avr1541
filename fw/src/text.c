/*
 *  fw/src/text.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <text.h>
#include <display.h>
#include <utf8.h>
#include <string.h>
#include <progmem.h>
#include <ext2.h>
#include <stdio.h>
#include <avr/eeprom.h>
#include <errno.h>

static uint32_t fontIno;

#define CACHE_SIZE 255

uint8_t fontCacheSize EEMEM;
uint8_t fontCachePos EEMEM;

struct {
	uint16_t uc;
	uint8_t bitmap[14];
} fontCache[CACHE_SIZE] EEMEM;

static const uint8_t unknown[14] PROGMEM = { 0x81, 0x42, 0x42, 0x24, 0x24, 0x18, 0x18, 0x18, 0x24, 0x24, 0x42, 0x42,
	0x81, 0x81
};

static int8_t loadCharacter(struct ext2_file *file, uint16_t uc) {
	uint8_t groups;
	uint16_t pos = 0;
	uint8_t cPos, cSize;
	uint8_t n;

	file->offset = 0;

	ext2Read(file, &groups, 1);

	for (n = 0; n < groups; ++n) {
		struct {
			uint16_t from;
			uint16_t to;
		} __attribute__ ((packed)) g;

		ext2Read(file, (uint8_t *)&g, 4);

		if (g.from <= uc && uc <= g.to) {
			pos += uc - g.from;
			break;
		}

		pos += g.to - g.from + 1;
	}

	if (n < groups) {
		file->offset = 1 + groups * 4 + pos * 14;
	}

	cSize = eeprom_read_byte(&fontCacheSize);
	cPos = eeprom_read_byte(&fontCachePos);

	if (cPos == CACHE_SIZE) {
		cPos = 0x7f - 0x20;
	}

	for (uint8_t i = 0; i < 14; ++i) {
		uint8_t b;
		if (n < groups) {
			ext2Read(file, &b, 1);
		} else {
			b = pgm_read_byte(&(unknown[i]));
		}
		eeprom_write_byte(&(fontCache[cPos].bitmap[i]), b);
	}

	eeprom_write_word(&(fontCache[cPos].uc), uc);

	if (cSize < CACHE_SIZE)
		eeprom_write_byte(&fontCacheSize, cSize + 1);
	eeprom_write_byte(&fontCachePos, cPos + 1);

	return cPos;
}

int8_t openFont() {
	fontIno = ext2FindFile(_PSTR("/.c64/font.fnt"), 1);
	if (errno) {
		errno = 0;
		return -1;
	}

	return 0;
}

int8_t isFontLoaded() {
	uint8_t cSize = eeprom_read_byte(&fontCacheSize);
	if (cSize > 0 && cSize < CACHE_SIZE)
		return 1;
	return 0;
}

int8_t loadFont() {
	struct ext2_file file;
	uint8_t xPos;

	ext2ReadInode(&file, fontIno);
	if (errno) {
		errno = 0;
		return -2;
	}

	eeprom_write_byte(&fontCachePos, 0);
	eeprom_write_byte(&fontCacheSize, 0);

	drawBox(17,117,159,125,0x001f);

	xPos=18;
	for (uint8_t uc = 0x20; uc <= 0x7e; ++uc) {
		if (loadCharacter(&file, uc) < 0) {
			return -3;
		}
		if (uc&1) {
			drawFilledBox(xPos,118,xPos+2,124,0x001f);
			xPos += 3;
		}
	}

	blankScreen();

	return 0;
}

static int8_t findCharacter(uint16_t uc) {
	uint8_t size = eeprom_read_byte(&fontCacheSize);
	struct ext2_file file;

	if (uc >= 0x20 && uc <= 0x7e && size >= 0x7e - 0x20 + 1) {
		return uc - 0x20;
	}

	for (uint8_t n = 0; n < size; ++n) {
		if (eeprom_read_word(&(fontCache[n].uc)) == uc) {
			return n;
		}
	}

	ext2ReadInode(&file, fontIno);
	if (errno)
		return -1;

	return loadCharacter(&file, uc);
}

static struct streamData {
	uint8_t x;
	uint8_t y;
	uint16_t color;
	uint16_t bgcolor;
	uint8_t ucState;
	uint16_t uc;
} data;

static void renderUnicode() {
	int8_t cPos;

	if (data.x > DISPLAY_WIDTH - 8)
		return;

	if ((cPos = findCharacter(data.uc)) >= 0) {
		drawHorizontal(data.x, data.x+7, data.y, data.bgcolor);
		drawBitmap(data.x, data.y+1, 8, 14, data.color, data.bgcolor, fontCache[cPos].bitmap, 1);
		drawHorizontal(data.x, data.x+7, data.y+15, data.bgcolor);
		data.x += 8;
	}
}

static int putCharacter(char c, FILE * stream) {
	switch (data.ucState) {
		case 0:
			if ((c & 0x80) == 0) {
				data.uc = (uint16_t)c & 0xff;
			} else if ((c & 0xe0) == 0xc0) {
				data.ucState = 1;
				data.uc = ((uint16_t)c & 0x1f) << 6;
				return 0;
			} else if ((c & 0xf0) == 0xe0) {
				data.ucState = 2;
				data.uc = ((uint16_t)c & 0xf) << 12;
				return 0;
			}
			break;
		case 1:
			if ((c & 0xc0) == 0x80) {
				data.uc |= (uint16_t)c & 0x3f;
			} else {
				//try ascii
				data.uc >>= 6;
				data.uc |= 0xc0;
				renderUnicode();
				data.uc = (uint16_t)c & 0xff;
			}
			break;
		case 2:
			if ((c & 0xc0) == 0x80) {
				data.uc |= ((uint16_t)c & 0x3f) << 6;
				data.ucState = 3;
				return 0;
			} else {
				//try ascii
				data.uc >>= 12;
				data.uc |= 0xe0;
				renderUnicode();
				data.uc = (uint16_t)c & 0xff;
			}
			break;
		case 3:
			if ((c & 0xc0) == 0x80) {
				data.uc |= (uint16_t)c & 0x3f;
			} else {
				//try ascii
				uint16_t uc = data.uc;

				data.uc = (uc >> 12) | 0xe0;
				renderUnicode();
				data.uc = ((uc >> 6) & 0x3f) | 0x80;
				renderUnicode();
				data.uc = (uint16_t)c & 0xff;
			}
			break;
	}

	data.ucState = 0;
	renderUnicode();
	return 0;
}

static FILE renderStream = FDEV_SETUP_STREAM(putCharacter, NULL, _FDEV_SETUP_WRITE);

void textPrintfV(uint8_t x, uint8_t y, uint16_t color, uint16_t bgcolor, const char *format, va_list args) {
	data.x = x;
	data.y = y;
	data.color = color;
	data.bgcolor = bgcolor;
	data.ucState = 0;

	vfprintf_P(&renderStream, format, args);
}

void textPrintf(uint8_t x, uint8_t y, uint16_t color, uint16_t bgcolor, const char *format, ...) {
	va_list args;
	va_start(args, format);

	textPrintfV(x, y, color, bgcolor, format, args);

	va_end(args);
}

void renderText(uint8_t x, uint8_t y, uint16_t color, uint16_t bgcolor, const char *text, uint8_t pgm) {
	textPrintf(x, y, color, bgcolor, pgm ? _PSTR("%S") : _PSTR("%s"), text);
}

void renderTextLine(uint8_t y, uint16_t color, uint16_t bgcolor, const char *text, uint8_t pgm) {
	uint16_t len = strlen_utf8(text, pgm);

	if (len < 22) {
		renderText(0, y, color, bgcolor, text, pgm);
		drawFilledBox(len * 8, y, DISPLAY_WIDTH - 1, y + 15, bgcolor);
	} else {
		renderText(0, y, color, bgcolor, text, pgm);
	}
}
