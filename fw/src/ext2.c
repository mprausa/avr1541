/*
 *  fw/src/ext2.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ext2.h>
#include <mbr.h>
#include <errno.h>
#include <sys/errno.h>
#include <stdlib.h>
#include <string.h>
#include <progmem.h>
#include <version.h>

#define DISK_SECTOR_BITS 9

/* Magic value used to identify an ext2 filesystem.  */
#define EXT2_MAGIC      0xEF53
/* Maximum length of a pathname.  */
#define EXT2_PATH_MAX       4096
/* Maximum nesting of symlinks, used to prevent a loop.  */
#define EXT2_MAX_SYMLINKCNT 8

/* The good old revision and the default inode size.  */
#define EXT2_GOOD_OLD_REVISION      0
#define EXT2_GOOD_OLD_INODE_SIZE    128

/* Filetype information as used in inodes.  */
#define FILETYPE_INO_MASK   0170000
#define FILETYPE_INO_REG    0100000
#define FILETYPE_INO_DIRECTORY  0040000
#define FILETYPE_INO_SYMLINK    0120000

/* Log2 size of ext2 block in 512 blocks.  */
#define LOG2_EXT2_BLOCK_SIZE (sblock.log2_block_size + 1)

/* Log2 size of ext2 block in bytes.  */
#define LOG2_BLOCK_SIZE (sblock.log2_block_size + 10)

/* The size of an ext2 block in bytes.  */
#define EXT2_BLOCK_SIZE     (1 << LOG2_BLOCK_SIZE)

/* The revision level.  */
#define EXT2_REVISION   sblock.revision_level

/* The inode size.  */
#define EXT2_INODE_SIZE \
        (EXT2_REVISION  == EXT2_GOOD_OLD_REVISION \
         ? EXT2_GOOD_OLD_INODE_SIZE \
         : sblock.inode_size)

/* Superblock filesystem feature flags (RW compatible)
 * A filesystem with any of these enabled can be read and written by a driver
 * that does not understand them without causing metadata/data corruption.  */
#define EXT2_FEATURE_COMPAT_DIR_PREALLOC    0x0001
#define EXT2_FEATURE_COMPAT_IMAGIC_INODES   0x0002
#define EXT3_FEATURE_COMPAT_HAS_JOURNAL     0x0004
#define EXT2_FEATURE_COMPAT_EXT_ATTR        0x0008
#define EXT2_FEATURE_COMPAT_RESIZE_INODE    0x0010
#define EXT2_FEATURE_COMPAT_DIR_INDEX       0x0020
/* Superblock filesystem feature flags (RO compatible)
 * A filesystem with any of these enabled can be safely read by a driver that
 * does not understand them, but should not be written to, usually because
 * additional metadata is required.  */
#define EXT2_FEATURE_RO_COMPAT_SPARSE_SUPER 0x0001
#define EXT2_FEATURE_RO_COMPAT_LARGE_FILE   0x0002
#define EXT2_FEATURE_RO_COMPAT_BTREE_DIR    0x0004
#define EXT4_FEATURE_RO_COMPAT_GDT_CSUM     0x0010
#define EXT4_FEATURE_RO_COMPAT_DIR_NLINK    0x0020
#define EXT4_FEATURE_RO_COMPAT_EXTRA_ISIZE  0x0040
/* Superblock filesystem feature flags (back-incompatible)
 * A filesystem with any of these enabled should not be attempted to be read
 * by a driver that does not understand them, since they usually indicate
 * metadata format changes that might confuse the reader.  */
#define EXT2_FEATURE_INCOMPAT_COMPRESSION   0x0001
#define EXT2_FEATURE_INCOMPAT_FILETYPE      0x0002
#define EXT3_FEATURE_INCOMPAT_RECOVER       0x0004  /* Needs recovery */
#define EXT3_FEATURE_INCOMPAT_JOURNAL_DEV   0x0008  /* Volume is journal device */
#define EXT2_FEATURE_INCOMPAT_META_BG       0x0010
#define EXT4_FEATURE_INCOMPAT_EXTENTS       0x0040  /* Extents used */
#define EXT4_FEATURE_INCOMPAT_64BIT     0x0080
#define EXT4_FEATURE_INCOMPAT_FLEX_BG       0x0200

/* The set of back-incompatible features this driver DOES support. Add (OR)
 * flags here as the related features are implemented into the driver.  */
#define EXT2_DRIVER_SUPPORTED_INCOMPAT ( EXT2_FEATURE_INCOMPAT_FILETYPE \
                                       | EXT4_FEATURE_INCOMPAT_EXTENTS  \
                                       | EXT4_FEATURE_INCOMPAT_FLEX_BG )

#define EXT2_
/* List of rationales for the ignored "incompatible" features:
 * needs_recovery: Not really back-incompatible - was added as such to forbid
 *                 ext2 drivers from mounting an ext3 volume with a dirty
 *                 journal because they will ignore the journal, but the next
 *                 ext3 driver to mount the volume will find the journal and
 *                 replay it, potentially corrupting the metadata written by
 *                 the ext2 drivers. Safe to ignore for this RO driver.  */
#define EXT2_DRIVER_IGNORED_INCOMPAT ( EXT3_FEATURE_INCOMPAT_RECOVER )

#define EXT3_JOURNAL_MAGIC_NUMBER   0xc03b3998U

#define EXT3_JOURNAL_DESCRIPTOR_BLOCK   1
#define EXT3_JOURNAL_COMMIT_BLOCK   2
#define EXT3_JOURNAL_SUPERBLOCK_V1  3
#define EXT3_JOURNAL_SUPERBLOCK_V2  4
#define EXT3_JOURNAL_REVOKE_BLOCK   5

#define EXT3_JOURNAL_FLAG_ESCAPE    1
#define EXT3_JOURNAL_FLAG_SAME_UUID 2
#define EXT3_JOURNAL_FLAG_DELETED   4
#define EXT3_JOURNAL_FLAG_LAST_TAG  8

#define EXT4_EXTENTS_FLAG       0x80000

/* The ext2 superblock.  */
struct ext2_sblock {
    uint32_t total_inodes;
    uint32_t total_blocks;
    uint32_t reserved_blocks;
    uint32_t free_blocks;
    uint32_t free_inodes;
    uint32_t first_data_block;
    uint32_t log2_block_size;
    uint32_t log2_fragment_size;
    uint32_t blocks_per_group;
    uint32_t fragments_per_group;
    uint32_t inodes_per_group;
    uint32_t mtime;
    uint32_t utime;
    uint16_t mnt_count;
    uint16_t max_mnt_count;
    uint16_t magic;
    uint16_t fs_state;
    uint16_t error_handling;
    uint16_t minor_revision_level;
    uint32_t lastcheck;
    uint32_t checkinterval;
    uint32_t creator_os;
    uint32_t revision_level;
    uint16_t uid_reserved;
    uint16_t gid_reserved;
    uint32_t first_inode;
    uint16_t inode_size;
    uint16_t block_group_number;
    uint32_t feature_compatibility;
    uint32_t feature_incompat;
    uint32_t feature_ro_compat;
    uint16_t uuid[8];
    char volume_name[16];
    char last_mounted_on[64];
    uint32_t compression_info;
    uint8_t prealloc_blocks;
    uint8_t prealloc_dir_blocks;
    uint16_t reserved_gdt_blocks;
    uint8_t journal_uuid[16];
    uint32_t journal_inum;
    uint32_t journal_dev;
    uint32_t last_orphan;
    uint32_t hash_seed[4];
    uint8_t def_hash_version;
    uint8_t jnl_backup_type;
    uint16_t reserved_word_pad;
    uint32_t default_mount_opts;
    uint32_t first_meta_bg;
    uint32_t mkfs_time;
    uint32_t jnl_blocks[17];
};

static struct {
    uint32_t first_data_block;
    uint32_t log2_block_size;
    uint32_t revision_level;
    uint16_t inode_size;
    uint32_t inodes_per_group;
    uint32_t blocks_per_group;
    uint32_t total_blocks;
    uint32_t free_blocks;
    uint32_t free_inodes;
    uint32_t feature_ro_compat;
    uint32_t utime;
} sblock;

static uint32_t prev_block;

/* The ext2 inode.  */
struct ext2_inode {
    uint16_t mode;
    uint16_t uid;
    uint32_t size;
    uint32_t atime;
    uint32_t ctime;
    uint32_t mtime;
    uint32_t dtime;
    uint16_t gid;
    uint16_t nlinks;
    uint32_t blockcnt;  /* Blocks of 512 bytes!! */
    uint32_t flags;
    uint32_t osd1;
    union {
        struct datablocks {
            uint32_t dir_blocks[EXT2_INDIRECT_BLOCKS];
            uint32_t indir_block;
            uint32_t double_indir_block;
            uint32_t triple_indir_block;
        } blocks;
        char symlink[60];
    };
    uint32_t version;
    uint32_t acl;
    uint32_t dir_acl;
    uint32_t fragment_addr;
    uint32_t osd2[3];
};

/* The ext2 blockgroup.  */
struct ext2_block_group {
    uint32_t block_id;
    uint32_t inode_id;
    uint32_t inode_table_id;
    uint16_t free_blocks;
    uint16_t free_inodes;
    uint16_t used_dirs;
    uint16_t pad;
    uint32_t reserved[3];
};

struct ext2_dirent {
    uint32_t inode;
    uint16_t direntlen;
    uint8_t namelen;
    uint8_t filetype;
};

#define EXT4_EXT_MAGIC      0xf30a

static uint16_t lastUID = 0;
static uint16_t lastGID = 0;

/* Read into BLKGRP the blockgroup descriptor of blockgroup GROUP of
   the mounted filesystem DATA.  */
static int8_t ext2_read_blockgroup(uint32_t group, struct ext2_block_group *blkgrp) {
    if (mbrReadPartition
        ((sblock.first_data_block + 1) << LOG2_EXT2_BLOCK_SIZE,
         group * sizeof(struct ext2_block_group), sizeof(struct ext2_block_group), (uint8_t *)blkgrp)) {
        return -1;
    }
    return 0;
}

static int8_t ext2_write_blockgroup(uint32_t group, const struct ext2_block_group *blkgrp) {
    if (mbrWritePartition
        ((sblock.first_data_block + 1) << LOG2_EXT2_BLOCK_SIZE,
         group * sizeof(struct ext2_block_group), sizeof(struct ext2_block_group), (uint8_t *)blkgrp, 0)) {
        return -1;
    }
    return 0;
}

/* Read the inode INO for the file described by DATA into INODE.  */
static int8_t ext2_read_inode(int32_t ino, struct ext2_stripped_inode *sinode) {
    struct ext2_block_group blkgrp;
    uint16_t inodes_per_block;
    uint32_t blkno;
    uint16_t blkoff;

    /* It is easier to calculate if the first inode is 0.  */
    ino--;

    ext2_read_blockgroup(ino / sblock.inodes_per_group, &blkgrp);
    if (errno)
        return -1;

    inodes_per_block = EXT2_BLOCK_SIZE / EXT2_INODE_SIZE;
    blkno = (ino % sblock.inodes_per_group) / inodes_per_block;
    blkoff = (ino % sblock.inodes_per_group) % inodes_per_block;

    /* Read the inode.  */

#define READ_INO_DATA(n,ptr) if (mbrReadPartition(((blkgrp.inode_table_id + blkno) << LOG2_EXT2_BLOCK_SIZE), \
                EXT2_INODE_SIZE * blkoff + (uint16_t) & (((struct ext2_inode *)0)->n), \
                    sizeof(((struct ext2_inode *)0)->n), (uint8_t *)(ptr))) {return -1;}

    READ_INO_DATA(mode, &sinode->mode);
    READ_INO_DATA(size, &sinode->size);
    READ_INO_DATA(nlinks, &sinode->nlinks);
    READ_INO_DATA(blockcnt, &sinode->blockcnt);
    READ_INO_DATA(symlink, &sinode->symlink);

    READ_INO_DATA(uid, &lastUID);
    READ_INO_DATA(gid, &lastGID);

#undef READ_INO_DATA

    return 0;
}

/* Write the inode INO for the file described by DATA into INODE.  */
static int8_t ext2_write_inode(int32_t ino, struct ext2_stripped_inode *sinode) {
    struct ext2_block_group blkgrp;
    uint16_t inodes_per_block;
    uint32_t blkno;
    uint16_t blkoff;

    /* It is easier to calculate if the first inode is 0.  */
    ino--;

    ext2_read_blockgroup(ino / sblock.inodes_per_group, &blkgrp);
    if (errno)
        return -1;

    inodes_per_block = EXT2_BLOCK_SIZE / EXT2_INODE_SIZE;
    blkno = (ino % sblock.inodes_per_group) / inodes_per_block;
    blkoff = (ino % sblock.inodes_per_group) % inodes_per_block;

    /* Write the inode.  */

#define WRITE_INO_DATA(n,ptr) if (mbrWritePartition(((blkgrp.inode_table_id + blkno) << LOG2_EXT2_BLOCK_SIZE), \
                EXT2_INODE_SIZE * blkoff + (uint16_t) & (((struct ext2_inode *)0)->n), \
                    sizeof(((struct ext2_inode *)0)->n), (uint8_t *)(ptr),0)) {return -1;}

    WRITE_INO_DATA(mode, &sinode->mode);
    WRITE_INO_DATA(size, &sinode->size);
    WRITE_INO_DATA(nlinks, &sinode->nlinks);
    WRITE_INO_DATA(blockcnt, &sinode->blockcnt);
    WRITE_INO_DATA(symlink, &sinode->symlink);

#undef WRITE_INO_DATA

    return 0;
}

static int is_pow(uint32_t x, uint32_t y) { // x == y^n
    uint32_t current = y;

    while (current < x) {
        current *= y;
    }

    return (current == x);
}

static uint8_t ext2_update_sb() {
    uint32_t groups = (sblock.total_blocks + sblock.blocks_per_group - 1) / sblock.blocks_per_group;

    for (uint32_t i = 0; i < groups; ++i) {
        uint32_t sector;

        if (sblock.feature_ro_compat & EXT2_FEATURE_RO_COMPAT_SPARSE_SUPER) {
            if (i > 1 && !is_pow(i, 3) && !is_pow(i, 5)
                && !is_pow(i, 7))
                continue;
        }

        if (i == 0) {
            sector = 2;
        } else {
            sector = (sblock.first_data_block + i * sblock.blocks_per_group) << LOG2_EXT2_BLOCK_SIZE;
        }

        if (mbrWritePartition
            (sector, (uint16_t)&(((struct ext2_sblock *)0)->free_blocks), sizeof(sblock.free_blocks), (uint8_t *)&sblock.free_blocks, 0) ||
            mbrWritePartition
            (sector, (uint16_t)&(((struct ext2_sblock *)0)->free_inodes), sizeof(sblock.free_inodes), (uint8_t *)&sblock.free_inodes, 0)) {
            return -1;
        }
    }

    return 0;
}

static uint32_t ext2_get_free_group_for_block(struct ext2_block_group *bg) {
    uint32_t start = (prev_block - sblock.first_data_block) / sblock.blocks_per_group;
    uint32_t bgcount = (sblock.total_blocks - sblock.first_data_block + sblock.blocks_per_group - 1) / sblock.blocks_per_group;
    uint32_t i = start;

    do {
        if (ext2_read_blockgroup(i, bg)) {
            return 0;
        }

        if (bg->free_blocks) {
            return i;
        }

        ++i;
        if (i == bgcount)
            i = 0;
    } while (i != start);

    errno = ENOSPC;
    return (uint32_t)-1;
}

static int32_t ext2_alloc_block(uint8_t zero) {
    struct ext2_block_group bg;
    uint32_t bgnum;
    uint32_t start, i;

    bgnum = ext2_get_free_group_for_block(&bg);
    if (errno)
        return 0;

    start = i = ((prev_block - sblock.first_data_block) % sblock.blocks_per_group) / 32;

    do {
        uint32_t bitmap;
        if (mbrReadPartition((uint32_t)bg.block_id << LOG2_EXT2_BLOCK_SIZE, i * 4, 4, (uint8_t *)&bitmap)) {
            return 0;
        }

        for (uint8_t j = 0; j < 32; j++) {
            if (!(bitmap & (1UL << j))) {
                uint32_t blknr = sblock.first_data_block + sblock.blocks_per_group * bgnum + i * 32 + j;

                if (zero) {
                    if (mbrWritePartition(blknr << LOG2_EXT2_BLOCK_SIZE, 0, EXT2_BLOCK_SIZE, NULL, 0)) {
                        return 0;
                    }
                }

                bitmap |= 1UL << j;

                if (mbrWritePartition((uint32_t)bg.block_id << LOG2_EXT2_BLOCK_SIZE, i * 4, 4, (uint8_t *)&bitmap, 0)) {
                    return 0;
                }

                bg.free_blocks--;
                if (ext2_write_blockgroup(bgnum, &bg)) {
                    return 0;
                }

                sblock.free_blocks--;
                if (ext2_update_sb()) {
                    return 0;
                }

                prev_block = blknr;
                return blknr;
            }
        }

        ++i;
        if (i == sblock.blocks_per_group)
            i = 0;
    } while (i != start);

    errno = EINVAL;
    return 0;
}

static int8_t ext2_free_block(uint32_t blknr) {
    uint32_t bgnum = (blknr - sblock.first_data_block) / sblock.blocks_per_group;
    uint32_t blk = (blknr - sblock.first_data_block) % sblock.blocks_per_group;
    struct ext2_block_group bg;
    uint8_t bitmap;

    if (ext2_read_blockgroup(bgnum, &bg)) {
        return -1;
    }

    if (mbrReadPartition((uint32_t)bg.block_id << LOG2_EXT2_BLOCK_SIZE, blk / 8, 1, &bitmap)) {
        return -1;
    }

    bitmap &= ~(1UL << (blk % 8));

    if (mbrWritePartition((uint32_t)bg.block_id << LOG2_EXT2_BLOCK_SIZE, blk / 8, 1, &bitmap, 0)) {
        return -1;
    }

    bg.free_blocks++;

    if (ext2_write_blockgroup(bgnum, &bg)) {
        return -1;
    }

    sblock.free_blocks++;

    if (ext2_update_sb()) {
        return -1;
    }

    return 0;
}

static int32_t ext2_get_block(struct ext2_file *file, uint32_t fileblock, uint8_t alloc) {
    struct ext2_stripped_inode *inode = &file->inode;
    int32_t blknr = -1;
    uint16_t blksz = EXT2_BLOCK_SIZE;
    uint8_t log2_blksz = LOG2_EXT2_BLOCK_SIZE;

    if (file->cache.fblock == fileblock) {
        if (file->cache.block || !alloc)
            return file->cache.block;
    }

    /* Direct blocks.  */
    if (fileblock < EXT2_INDIRECT_BLOCKS) {
        blknr = inode->blocks.dir_blocks[fileblock];

        if (alloc & !blknr) {
            blknr = ext2_alloc_block(0);

            if (errno)
                return -1;

            inode->blocks.dir_blocks[fileblock] = blknr;
            inode->blockcnt += blksz / 512;

            ext2_write_inode(file->ino, &file->inode);
            if (errno)
                return -1;
        }
    }
    /* Indirect.  */
    else if (fileblock < EXT2_INDIRECT_BLOCKS + blksz / 4) {
        if (!inode->blocks.indir_block) {
            if (alloc) {
                inode->blocks.indir_block = ext2_alloc_block(1);

                if (errno)
                    return -1;

                inode->blockcnt += blksz / 512;

                ext2_write_inode(file->ino, &file->inode);
                if (errno)
                    return -1;
            } else {
                return 0;
            }
        }

        if (mbrReadPartition((uint32_t)inode->blocks.indir_block << log2_blksz, (fileblock - EXT2_INDIRECT_BLOCKS) * 4, 4, (uint8_t *)&blknr)) {
            return -1;
        }

        if (alloc & !blknr) {
            blknr = ext2_alloc_block(0);

            if (errno)
                return -1;

            if (mbrWritePartition
                ((uint32_t)inode->blocks.indir_block << log2_blksz, (fileblock - EXT2_INDIRECT_BLOCKS) * 4, 4, (uint8_t *)&blknr, 0)) {
                return -1;
            }

            inode->blockcnt += blksz / 512;

            ext2_write_inode(file->ino, &file->inode);
            if (errno)
                return -1;
        }
    }
    /* Double indirect.  */
    else if (fileblock < EXT2_INDIRECT_BLOCKS + ((uint32_t)blksz >> 2) * (((uint32_t)blksz >> 2) + 1)) {
        uint32_t rblock = fileblock - (EXT2_INDIRECT_BLOCKS + (blksz >> 2));
        uint32_t indir;

        if (!inode->blocks.double_indir_block) {
            if (alloc) {
                inode->blocks.double_indir_block = ext2_alloc_block(1);

                if (errno)
                    return -1;

                inode->blockcnt += blksz / 512;

                ext2_write_inode(file->ino, &file->inode);
                if (errno)
                    return -1;
            } else {
                return 0;
            }
        }
        if (mbrReadPartition((uint32_t)inode->blocks.double_indir_block << log2_blksz, (rblock / (blksz >> 2)) * 4, 4, (uint8_t *)&indir))
            return -1;

        if (!indir) {
            if (alloc) {
                indir = ext2_alloc_block(1);

                if (errno)
                    return -1;

                if (mbrWritePartition
                    ((uint32_t)inode->blocks.double_indir_block << log2_blksz, (rblock / (blksz >> 2)) * 4, 4, (uint8_t *)&indir, 0))
                    return -1;

                inode->blockcnt += blksz / 512;

                ext2_write_inode(file->ino, &file->inode);
                if (errno)
                    return -1;
            } else {
                return 0;
            }
        }

        if (mbrReadPartition(indir << log2_blksz, (rblock % (blksz >> 2)) * 4, 4, (uint8_t *)&blknr))
            return -1;

        if (alloc & !blknr) {
            blknr = ext2_alloc_block(0);

            if (errno)
                return -1;

            if (mbrWritePartition(indir << log2_blksz, (rblock % (blksz >> 2)) * 4, 4, (uint8_t *)&blknr, 0))
                return -1;

            inode->blockcnt += blksz / 512;

            ext2_write_inode(file->ino, &file->inode);
            if (errno)
                return -1;
        }
    }
    /* triple indirect.  */
    else {
        uint32_t rblock3 = fileblock - (EXT2_INDIRECT_BLOCKS + ((uint32_t)blksz >> 2) * (((uint32_t)blksz >> 2) + 1));
        uint32_t rblock2 = rblock3 / (blksz >> 2);
        uint32_t rblock1 = rblock2 / (blksz >> 2);
        uint32_t indir1, indir2;

        rblock3 %= (blksz >> 2);
        rblock2 %= (blksz >> 2);

        if (!inode->blocks.triple_indir_block) {
            if (alloc) {
                inode->blocks.triple_indir_block = ext2_alloc_block(1);

                if (errno)
                    return -1;

                inode->blockcnt += blksz / 512;

                ext2_write_inode(file->ino, &file->inode);

                if (errno)
                    return -1;
            } else {
                return 0;
            }
        }

        if (mbrReadPartition((uint32_t)inode->blocks.triple_indir_block << log2_blksz, rblock1 * 4, 4, (uint8_t *)&indir1))
            return -1;

        if (!indir1) {
            if (alloc) {
                indir1 = ext2_alloc_block(1);

                if (errno)
                    return -1;

                if (mbrWritePartition((uint32_t)inode->blocks.triple_indir_block << log2_blksz, rblock1 * 4, 4, (uint8_t *)&indir1, 0))
                    return -1;

                inode->blockcnt += blksz / 512;

                ext2_write_inode(file->ino, &file->inode);

                if (errno)
                    return -1;
            } else {
                return 0;
            }
        }

        if (mbrReadPartition(indir1 << log2_blksz, rblock2 * 4, 4, (uint8_t *)&indir2))
            return -1;

        if (!indir2) {
            if (alloc) {
                indir2 = ext2_alloc_block(1);

                if (errno)
                    return -1;

                if (mbrWritePartition(indir1 << log2_blksz, rblock2 * 4, 4, (uint8_t *)&indir2, 0))
                    return -1;

                inode->blockcnt += blksz / 512;

                ext2_write_inode(file->ino, &file->inode);

                if (errno)
                    return -1;
            } else {
                return 0;
            }
        }

        if (mbrReadPartition(indir2 << log2_blksz, rblock3 * 4, 4, (uint8_t *)&blknr))
            return -1;

        if (alloc & !blknr) {
            blknr = ext2_alloc_block(0);

            if (errno)
                return -1;

            if (mbrWritePartition(indir2 << log2_blksz, rblock3 * 4, 4, (uint8_t *)&blknr, 0))
                return -1;

            inode->blockcnt += blksz / 512;

            ext2_write_inode(file->ino, &file->inode);
            if (errno)
                return -1;
        }
    }

    file->cache.fblock = fileblock;
    file->cache.block = blknr;

    return blknr;
}

static uint8_t ext2_free_if_empty(uint32_t blknum) {
    uint8_t empty = 1;
    for (uint32_t n = 0; n < EXT2_BLOCK_SIZE; n += 4) {
        uint32_t v;

        if (mbrReadPartition(blknum << LOG2_EXT2_BLOCK_SIZE, n, 4, (uint8_t *)&v)) {
            return -1;
        }

        if (v) {
            empty = 0;
            break;
        }
    }

    if (empty) {
        if (ext2_free_block(blknum)) {
            return -1;
        }

        return 1;
    }

    return 0;
}

static uint8_t ext2_free_fileblock(struct ext2_file *file, uint32_t fileblock) {
    struct ext2_stripped_inode *inode = &file->inode;
    uint32_t blknr = 0;
    uint16_t blksz = EXT2_BLOCK_SIZE;
    uint8_t log2_blksz = LOG2_EXT2_BLOCK_SIZE;

    if (file->cache.fblock == fileblock) {
        file->cache.fblock = (uint32_t)-1;
        file->cache.block = (uint32_t)-1;
    }

    /* Direct blocks.  */
    if (fileblock < EXT2_INDIRECT_BLOCKS) {
        blknr = inode->blocks.dir_blocks[fileblock];

        inode->blocks.dir_blocks[fileblock] = 0;
        inode->blockcnt -= blksz / 512;
    }
    /* Indirect.  */
    else if (fileblock < EXT2_INDIRECT_BLOCKS + blksz / 4) {
        if (inode->blocks.indir_block) {
            if (mbrReadPartition(inode->blocks.indir_block << log2_blksz, (fileblock - EXT2_INDIRECT_BLOCKS) * 4, 4, (uint8_t *)&blknr)) {
                return -1;
            }

            if (mbrWritePartition(inode->blocks.indir_block << log2_blksz, (fileblock - EXT2_INDIRECT_BLOCKS) * 4, 4, NULL, 0)) {
                return -1;
            }

            inode->blockcnt -= blksz / 512;

            if (ext2_free_if_empty(inode->blocks.indir_block) == 1) {
                inode->blocks.indir_block = 0;
                inode->blockcnt -= blksz / 512;
            }
        }

    }
    /* Double indirect.  */
    else if (fileblock < EXT2_INDIRECT_BLOCKS + ((uint32_t)blksz >> 2) * (((uint32_t)blksz >> 2) + 1)) {
        uint32_t rblock = fileblock - (EXT2_INDIRECT_BLOCKS + (blksz >> 2));
        uint32_t indir;

        if (inode->blocks.double_indir_block) {

            if (mbrReadPartition((uint32_t)inode->blocks.double_indir_block << log2_blksz, (rblock / (blksz >> 2)) * 4, 4, (uint8_t *)&indir))
                return -1;

            if (indir) {
                if (mbrReadPartition(indir << log2_blksz, (rblock % (blksz >> 2)) * 4, 4, (uint8_t *)&blknr))
                    return -1;

                if (mbrWritePartition(indir << log2_blksz, (rblock % (blksz >> 2)) * 4, 4, NULL, 0))
                    return -1;

                inode->blockcnt -= blksz / 512;

                if (ext2_free_if_empty(indir) == 1) {
                    if (mbrWritePartition
                        ((uint32_t)inode->blocks.double_indir_block << log2_blksz, (rblock / (blksz >> 2)) * 4, 4, NULL, 0))
                        return -1;

                    inode->blockcnt -= blksz / 512;

                    if (ext2_free_if_empty(inode->blocks.double_indir_block) == 1) {
                        inode->blocks.double_indir_block = 0;
                        inode->blockcnt -= blksz / 512;
                    }
                }
            }
        }
    }
    /* triple indirect.  */
    else {
        uint32_t rblock3 = fileblock - (EXT2_INDIRECT_BLOCKS + ((uint32_t)blksz >> 2) * (((uint32_t)blksz >> 2) + 1));
        uint32_t rblock2 = rblock3 / (blksz >> 2);
        uint32_t rblock1 = rblock2 / (blksz >> 2);
        uint32_t indir1, indir2;

        rblock3 %= (blksz >> 2);
        rblock2 %= (blksz >> 2);

        if (inode->blocks.triple_indir_block) {
            if (mbrReadPartition((uint32_t)inode->blocks.triple_indir_block << log2_blksz, rblock1 * 4, 4, (uint8_t *)&indir1))
                return -1;

            if (indir1) {
                if (mbrReadPartition(indir1 << log2_blksz, rblock2 * 4, 4, (uint8_t *)&indir2))
                    return -1;

                if (indir2) {
                    if (mbrReadPartition(indir2 << log2_blksz, rblock3 * 4, 4, (uint8_t *)&blknr))
                        return -1;

                    if (mbrWritePartition(indir2 << log2_blksz, rblock3 * 4, 4, NULL, 0))
                        return -1;

                    inode->blockcnt -= blksz / 512;

                    if (ext2_free_if_empty(indir2) == 1) {
                        if (mbrWritePartition(indir1 << log2_blksz, rblock2 * 4, 4, NULL, 0))
                            return -1;

                        inode->blockcnt -= blksz / 512;

                        if (ext2_free_if_empty(indir1)
                            == 1) {
                            if (mbrWritePartition((uint32_t)
                                          inode->blocks.triple_indir_block << log2_blksz, rblock1 * 4, 4, NULL, 0))
                                return -1;

                            if (ext2_free_if_empty(inode->blocks.triple_indir_block)
                                == 1) {
                                inode->blocks.triple_indir_block = 0;
                                inode->blockcnt -= blksz / 512;
                            }
                        }
                    }
                }
            }
        }
    }

    ext2_write_inode(file->ino, &file->inode);
    if (errno)
        return -1;

    if (!blknr)
        return 0;

    return ext2_free_block(blknr);
}

int16_t ext2_rw_file(struct ext2_file *file, uint32_t pos, uint16_t len, uint8_t *buf, uint8_t write) { //write = 2 -> pgm
    uint32_t i, blockcnt;
    uint16_t size = 0;

    if (!write) {
        /* Adjust LEN so we can't read past the end of the file.  */
        if (pos >= file->inode.size) {
            len = 0;
        } else if (pos + len > file->inode.size) {
            len = file->inode.size - pos;
        }
    }

    blockcnt = ((pos + len) + EXT2_BLOCK_SIZE - 1) >> LOG2_BLOCK_SIZE;

    for (i = pos >> LOG2_BLOCK_SIZE; i < blockcnt; i++) {
        uint32_t blknr;
        uint16_t blockoff = pos & (EXT2_BLOCK_SIZE - 1);
        uint16_t blockend = EXT2_BLOCK_SIZE;

        uint16_t skipfirst = 0;

        blknr = ext2_get_block(file, i, write);

        if (errno)
            break;

        blknr = blknr << LOG2_EXT2_BLOCK_SIZE;

        /* Last block.  */
        if (i == blockcnt - 1) {
            blockend = (len + pos) & (EXT2_BLOCK_SIZE - 1);

            /* The last portion is exactly blocksize.  */
            if (!blockend)
                blockend = EXT2_BLOCK_SIZE;
        }

        /* First block.  */
        if (i == (pos >> LOG2_BLOCK_SIZE)) {
            skipfirst = blockoff;
            blockend -= skipfirst;
        }

        if (write) {
            if (mbrWritePartition(blknr, skipfirst, blockend, buf, write == 2 ? 1 : 0)) {
                break;
            }
        } else {
            /* If the block number is 0 this block is not stored on disk but
               is zero filled instead.  */
            if (blknr) {
                if (mbrReadPartition(blknr, skipfirst, blockend, buf)) {
                    break;
                }
            } else
                memset(buf, 0, blockend);
        }

        if (buf)
            buf += EXT2_BLOCK_SIZE - skipfirst;
        size += blockend;
    }

    if (write) {
        if (pos + size > file->inode.size) {
            int _err = errno;
            errno = 0;
            file->inode.size = pos + size;
            ext2_write_inode(file->ino, &file->inode);
            if (!errno)
                errno = _err;
        }
    }

    return size;
}

static uint32_t ext2_get_free_group_for_inode(struct ext2_block_group *bg) {
    uint32_t bgcount = (sblock.total_blocks - sblock.first_data_block + sblock.blocks_per_group - 1) / sblock.blocks_per_group;

    for (uint32_t i = 0; i < bgcount; ++i) {
        if (ext2_read_blockgroup(i, bg)) {
            return 0;
        }

        if (bg->free_inodes) {
            return i;
        }

    }

    errno = ENOSPC;
    return (uint32_t)-1;
}

static uint32_t ext2_alloc_inode(uint8_t dir) {
    struct ext2_block_group bg;
    uint32_t bgnum;

    bgnum = ext2_get_free_group_for_inode(&bg);
    if (errno)
        return 0;

    for (uint8_t i = 0; i < sblock.inodes_per_group; ++i) {
        uint32_t bitmap;
        if (mbrReadPartition((uint32_t)bg.inode_id << LOG2_EXT2_BLOCK_SIZE, i * 4, 4, (uint8_t *)&bitmap)) {
            return 0;
        }

        for (uint8_t j = 0; j < 32; j++) {
            if (!(bitmap & (1UL << j))) {
                uint32_t ino = sblock.inodes_per_group * bgnum + i * 32 + j + 1;

                bitmap |= 1UL << j;

                if (mbrWritePartition((uint32_t)bg.inode_id << LOG2_EXT2_BLOCK_SIZE, i * 4, 4, (uint8_t *)&bitmap, 0)) {
                    return 0;
                }

                bg.free_inodes--;
                if (dir)
                    bg.used_dirs++;
                if (ext2_write_blockgroup(bgnum, &bg)) {
                    return 0;
                }

                sblock.free_inodes--;
                if (ext2_update_sb()) {
                    return 0;
                }

                return ino;
            }
        }
    }

    errno = EINVAL;
    return 0;
}

static int8_t ext2_free_inode(uint32_t ino) {
    uint32_t bgnum = (ino - 1) / sblock.inodes_per_group;
    uint32_t nr = (ino - 1) % sblock.inodes_per_group;
    uint16_t inodes_per_block = EXT2_BLOCK_SIZE / EXT2_INODE_SIZE;
    uint32_t blkno = nr / inodes_per_block;
    uint16_t blkoff = nr % inodes_per_block;
    struct ext2_block_group bg;
    uint8_t bitmap;
    uint32_t tmp;

    if (ext2_read_blockgroup(bgnum, &bg)) {
        return -1;
    }

    if (mbrReadPartition((uint32_t)bg.inode_id << LOG2_EXT2_BLOCK_SIZE, nr / 8, 1, &bitmap)) {
        return -1;
    }

    bitmap &= ~(1UL << (nr % 8));

    if (mbrWritePartition((uint32_t)bg.inode_id << LOG2_EXT2_BLOCK_SIZE, nr / 8, 1, &bitmap, 0)) {
        return -1;
    }

    bg.free_inodes++;

    if (ext2_write_blockgroup(bgnum, &bg)) {
        return -1;
    }

    sblock.free_inodes++;

    if (ext2_update_sb()) {
        return -1;
    }
#define WRITE_INO_DATA(n,ptr) if (mbrWritePartition(((bg.inode_table_id + blkno) << LOG2_EXT2_BLOCK_SIZE), \
                EXT2_INODE_SIZE * blkoff + (uint16_t) & (((struct ext2_inode *)0)->n), \
                    sizeof(((struct ext2_inode *)0)->n), (uint8_t *)(ptr),0)) {return -1;}

    tmp = __TIMESTAMP;
    WRITE_INO_DATA(dtime, &tmp);
    WRITE_INO_DATA(nlinks, NULL);

#undef WRITE_INO_DATA

    return 0;
}

static int8_t ext2_create_inode(int32_t ino, uint16_t mode) {
    struct ext2_block_group blkgrp;
    uint16_t inodes_per_block;
    uint32_t blkno;
    uint16_t blkoff;
    uint32_t tmp;

    ino--;

    ext2_read_blockgroup(ino / sblock.inodes_per_group, &blkgrp);
    if (errno)
        return -1;

    inodes_per_block = EXT2_BLOCK_SIZE / EXT2_INODE_SIZE;
    blkno = (ino % sblock.inodes_per_group) / inodes_per_block;
    blkoff = (ino % sblock.inodes_per_group) % inodes_per_block;

#define WRITE_INO_DATA(n,ptr) if (mbrWritePartition(((blkgrp.inode_table_id + blkno) << LOG2_EXT2_BLOCK_SIZE), \
                EXT2_INODE_SIZE * blkoff + (uint16_t) & (((struct ext2_inode *)0)->n), \
                    sizeof(((struct ext2_inode *)0)->n), (uint8_t *)(ptr),0)) {return -1;}

    WRITE_INO_DATA(mode, &mode);

    WRITE_INO_DATA(uid, &lastUID);

    WRITE_INO_DATA(size, NULL);

    tmp = __TIMESTAMP;
    WRITE_INO_DATA(atime, &tmp);
    WRITE_INO_DATA(ctime, &tmp);
    WRITE_INO_DATA(mtime, &tmp);

    WRITE_INO_DATA(dtime, NULL);

    WRITE_INO_DATA(gid, &lastGID);

    tmp = 1;
    WRITE_INO_DATA(nlinks, &tmp);

    WRITE_INO_DATA(blockcnt, NULL);

    WRITE_INO_DATA(flags, NULL);

    WRITE_INO_DATA(osd1, NULL);

    WRITE_INO_DATA(symlink, NULL);

    WRITE_INO_DATA(version, NULL);
    WRITE_INO_DATA(acl, NULL);
    WRITE_INO_DATA(dir_acl, NULL);
    WRITE_INO_DATA(fragment_addr, NULL);
    WRITE_INO_DATA(osd2, NULL);

#undef WRITE_INO_DATA

    return 0;
}

static int8_t ext2_link(struct ext2_file *dir, const char *name, uint32_t ino, uint8_t type, uint8_t pgm) {
    uint32_t fpos = 0;
    uint32_t newlen = (sizeof(struct ext2_dirent) + strlen_X(name, pgm) + 4) & ~3UL;
    struct ext2_dirent newentry;

    newentry.inode = ino;
    newentry.namelen = strlen_X(name, pgm);
    newentry.filetype = type;

    while (fpos < dir->inode.size) {
        struct ext2_dirent dirent;
        uint16_t len;

        ext2_rw_file(dir, fpos, sizeof(struct ext2_dirent), (uint8_t *)&dirent, 0);
        if (errno)
            return -1;

        if (dirent.direntlen == 0) {
            errno = EINVAL;
            return -1;
        }
        len = (sizeof(struct ext2_dirent) + dirent.namelen + 4) & ~3UL;

        if (dirent.direntlen - len >= newlen) {
            newentry.direntlen = dirent.direntlen - len;
            ext2_rw_file(dir, fpos + len, sizeof(struct ext2_dirent), (uint8_t *)&newentry, 1);
            if (errno)
                return -1;

            ext2_rw_file(dir, fpos + len + sizeof(struct ext2_dirent), newentry.namelen + 1, (uint8_t *)name, pgm ? 2 : 1);
            if (errno)
                return -1;

            dirent.direntlen = len;

            ext2_rw_file(dir, fpos, sizeof(struct ext2_dirent), (uint8_t *)&dirent, 1);
            if (errno)
                return -1;

            return 0;
        }

        fpos += dirent.direntlen;
    }

    dir->inode.size += EXT2_BLOCK_SIZE;
    dir->inode.size &= ~(uint32_t)(EXT2_BLOCK_SIZE - 1);
    ext2_write_inode(dir->ino, &dir->inode);

    newentry.direntlen = dir->inode.size - fpos;

    ext2_rw_file(dir, fpos, sizeof(struct ext2_dirent), (uint8_t *)&newentry, 1);
    if (errno)
        return -1;

    ext2_rw_file(dir, fpos + sizeof(struct ext2_dirent), newentry.namelen + 1, (uint8_t *)name, pgm ? 2 : 1);
    if (errno)
        return -1;

    return 0;
}

static uint32_t ext2_unlink(struct ext2_file *dir, const char *name, uint8_t pgm) {
    uint32_t fpos = 0, oldpos = -1;
    uint16_t len = strlen_X(name, pgm);
    char buf[len + 1];

    buf[len] = 0;

    while (fpos < dir->inode.size) {
        struct ext2_dirent dirent;

        ext2_rw_file(dir, fpos, sizeof(struct ext2_dirent), (uint8_t *)&dirent, 0);
        if (errno)
            return 0;

        if (dirent.direntlen == 0) {
            errno = EINVAL;
            return 0;
        }

        if (dirent.namelen == len) {
            ext2_rw_file(dir, fpos + sizeof(struct ext2_dirent), len, buf, 0);
            if (errno)
                return 0;
            if (!(pgm ? strcmp_P(buf, name) : strcmp(buf, name))) {
                uint16_t newlen;

                if (oldpos == (uint32_t)-1) {
                    errno = EINVAL;
                    return 0;
                }

                newlen = fpos + dirent.direntlen - oldpos;

                ext2_rw_file(dir, oldpos + (uint16_t)(&(((struct ext2_dirent *)
                                     NULL)->direntlen)), sizeof(dirent.direntlen), (uint8_t *)&newlen, 1);
                if (errno)
                    return 0;

                return dirent.inode;
            }
        }

        oldpos = fpos;
        fpos += dirent.direntlen;
    }
    errno = ENOENT;
    return 0;
}

int8_t ext2Mount() {
    uint16_t magic;
    uint32_t feature_incompat;

#define READ_SB_DATA(n,ptr) if (mbrReadPartition(2, (uint16_t) & (((struct ext2_sblock *)0)->n), \
                    sizeof(((struct ext2_sblock *)0)->n), (uint8_t *)(ptr))) {return -1;}
    READ_SB_DATA(magic, &magic);
    READ_SB_DATA(feature_incompat, &feature_incompat);
    READ_SB_DATA(first_data_block, &sblock.first_data_block);
    READ_SB_DATA(log2_block_size, &sblock.log2_block_size);
    READ_SB_DATA(revision_level, &sblock.revision_level);
    READ_SB_DATA(inode_size, &sblock.inode_size);
    READ_SB_DATA(inodes_per_group, &sblock.inodes_per_group);
    READ_SB_DATA(blocks_per_group, &sblock.blocks_per_group);
    READ_SB_DATA(total_blocks, &sblock.total_blocks);
    READ_SB_DATA(free_blocks, &sblock.free_blocks);
    READ_SB_DATA(free_inodes, &sblock.free_inodes);
    READ_SB_DATA(feature_ro_compat, &sblock.feature_ro_compat);
    READ_SB_DATA(utime, &sblock.utime);
#undef READ_SB_DATA

    if (magic != EXT2_MAGIC) {
        errno = EMEDIUMTYPE;
        return -1;
    }

    if (feature_incompat & ~(EXT2_DRIVER_SUPPORTED_INCOMPAT | EXT2_DRIVER_IGNORED_INCOMPAT)) {
        errno = ENOSYS;
        return -1;
    }

    return 0;
}

uint32_t ext2FindFile(const char *name, uint8_t pgm) {
    const char *slash;
    uint8_t len;
    uint32_t ino = 2;
    uint8_t filetype = FILETYPE_DIRECTORY;
    struct ext2_file dir;

    while (name) {
        while (X_read_byte(name, pgm) == '/')
            name++;

        if (!X_read_byte(name, pgm))
            break;

        if (filetype != FILETYPE_DIRECTORY) {
            errno = ENOTDIR;
            return (uint32_t)-1;
        }

        ext2ReadInode(&dir, ino);
        if (errno)
            return (uint32_t)-1;

        if (pgm)
            slash = strchr_P(name, '/');
        else
            slash = strchr(name, '/');

        if (slash) {
            len = (uint16_t)(slash - name);
        } else {
            len = strlen_X(name, pgm);
        }

        while (!(ino = ext2ReadDir(&dir, (char *)name, len, &filetype, pgm ? 2 : 1))) ;

        if (ino == (uint32_t)-1) {
            errno = ENOENT;
            return (uint32_t)-1;
        }

        name = slash;
    }

    if (filetype != FILETYPE_DIRECTORY && filetype != FILETYPE_REG) {
        errno = ENOSYS;
        return (uint32_t)-1;
    }

    return ino;
}

int8_t ext2ReadInode(struct ext2_file *file, uint32_t ino) {
    ext2_read_inode(ino, &file->inode);
    if (errno)
        return errno;

    file->ino = ino;
    file->offset = 0;
    file->cache.fblock = (uint32_t)-1;
    file->cache.block = (uint32_t)-1;

    return 0;
}

int8_t ext2Open(struct ext2_file *file, const char *name, uint8_t pgm) {
    uint32_t ino = ext2FindFile(name, pgm);
    if (errno)
        return errno;

    return ext2ReadInode(file, ino);
}

uint32_t ext2Create(const char *dir, const char *name, uint8_t pgm) {
    uint32_t ino;
    struct ext2_file fdir;

    ext2Open(&fdir, dir, pgm);
    if (errno)
        return 0;

    if ((fdir.inode.mode & FILETYPE_INO_MASK) != FILETYPE_INO_DIRECTORY) {
        errno = ENOTDIR;
        return 0;
    }

    if (!(ino = ext2_alloc_inode(0))) {
        return 0;
    }

    if (ext2_create_inode(ino, 0644 | FILETYPE_INO_REG)) {
        return 0;
    }

    ext2_link(&fdir, name, ino, FILETYPE_REG, pgm);

    if (errno)
        return 0;

    return ino;
}

uint32_t ext2MkDir(const char *dir, const char *name, uint8_t pgm) {
    uint32_t ino, parent;
    struct ext2_file fdir;

    ext2Open(&fdir, dir, pgm);
    if (errno)
        return 0;

    if ((fdir.inode.mode & FILETYPE_INO_MASK) != FILETYPE_INO_DIRECTORY) {
        errno = ENOTDIR;
        return 0;
    }

    if (!(ino = ext2_alloc_inode(1))) {
        return 0;
    }

    if (ext2_create_inode(ino, 0755 | FILETYPE_INO_DIRECTORY)) {
        return 0;
    }

    ext2_link(&fdir, name, ino, FILETYPE_DIRECTORY, pgm);

    fdir.inode.nlinks++;
    ext2_write_inode(fdir.ino, &fdir.inode);

    parent = fdir.ino;

    ext2ReadInode(&fdir, ino);
    if (errno)
        return 0;

    ext2_link(&fdir, _PSTR("."), ino, FILETYPE_DIRECTORY, 1);
    if (errno)
        return 0;

    ext2_link(&fdir, _PSTR(".."), parent, FILETYPE_DIRECTORY, 1);
    if (errno)
        return 0;

    fdir.inode.nlinks++;
    ext2_write_inode(fdir.ino, &fdir.inode);

    return ino;
}

int8_t ext2Remove(const char *dir, const char *name, uint8_t pgm) {
    struct ext2_file file;
    uint32_t ino;

    ext2Open(&file, dir, pgm);
    if (errno)
        return -1;

    if (!(ino = ext2_unlink(&file, name, pgm))) {
        return -1;
    }

    ext2ReadInode(&file, ino);
    if (errno)
        return -1;

    ext2Truncate(&file, 0);
    if (errno)
        return -1;

    ext2_free_inode(ino);
    if (errno)
        return -1;

    return 0;
}

int16_t ext2Read(struct ext2_file *file, uint8_t *buf, uint16_t len) {
    int16_t read;
    read = ext2_rw_file(file, file->offset, len, buf, 0);
    file->offset += read;
    return read;
}

int16_t ext2Write(struct ext2_file *file, const uint8_t *buf, uint16_t len) {
    int16_t write;
    write = ext2_rw_file(file, file->offset, len, (uint8_t *)buf, 1);
    file->offset += write;
    return write;
}

uint32_t ext2ReadDir(struct ext2_file *file, char *filename, uint8_t len, uint8_t *filetype, uint8_t cmp) {
    struct ext2_dirent entry;
    uint32_t offset = file->offset;
    uint32_t ino = 0;

    if (ext2Read(file, (uint8_t *)&entry, sizeof(struct ext2_dirent)) != sizeof(struct ext2_dirent)) {
        return (uint32_t)-1;
    }

    if (cmp && len == entry.namelen) {
        char buf[len];
        if (ext2Read(file, buf, entry.namelen) != entry.namelen) {
            return (uint32_t)-1;
        }

        if (!((cmp == 2) ? strncmp_P(buf, filename, len) : strncmp(buf, filename, len))) {
            if (filetype)
                *filetype = entry.filetype;
            ino = entry.inode;
        }
    } else if (!cmp) {
        if (len > entry.namelen) {
            if (ext2Read(file, filename, entry.namelen) != entry.namelen) {
                return (uint32_t)-1;
            }

            filename[entry.namelen] = 0;
        }

        if (filetype)
            *filetype = entry.filetype;
        ino = entry.inode;
    }

    file->offset = offset + entry.direntlen;

    return ino;
}

int8_t ext2Truncate(struct ext2_file *file, uint32_t len) {
    if (len < file->inode.size) {
        for (uint32_t i = (len + EXT2_BLOCK_SIZE - 1) >> LOG2_BLOCK_SIZE; i < ((file->inode.size + EXT2_BLOCK_SIZE - 1) >> LOG2_BLOCK_SIZE); ++i) {
            if (ext2_free_fileblock(file, i)) {
                return -1;
            }
        }

        file->inode.size = len;

        ext2_write_inode(file->ino, &file->inode);
        if (errno)
            return -1;
    } else if (len > file->inode.size) {
        file->inode.size = len;
        ext2_write_inode(file->ino, &file->inode);

        if (errno)
            return -1;
    }

    return 0;
}

uint32_t ext2WriteTime() {
    return sblock.utime;
}
