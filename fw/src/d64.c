/*
 *  fw/src/d64.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <d64.h>
#include <disk.h>
#include <string.h>

struct d64DirEntry d64Directory[16];

int8_t d64Title(struct ext2_file *file, unsigned char *title) {
    file->offset = 0x16590;

    if (ext2Read(file, title, D64_TITLE_SIZE) != D64_TITLE_SIZE) {
        return -1;
    }

    title[D64_TITLE_SIZE] = 0;
}

int8_t d64ID(struct ext2_file *file, unsigned char *id) {
    file->offset = 0x165a2;

    if (ext2Read(file, id, D64_ID_SIZE) != D64_ID_SIZE) {
        return -1;
    }

    id[D64_ID_SIZE] = 0;
}

uint8_t d64ReadDir(struct ext2_file *file) {
    file->offset = 0x16600;

    if (ext2Read(file, (uint8_t *)d64Directory, 256) != 256) {
        return -1;
    }

    if (d64Directory[0].next_track) {
        file->offset = diskTrackOffset(d64Directory[0].next_track)+(((uint16_t)d64Directory[0].next_sector)<<8);
        if (ext2Read(file, ((uint8_t *)d64Directory)+256, 256) != 256) {
            return -1;
        }
    } else {
        memset(((uint8_t *)d64Directory)+256,0,256);
    }

    return 0;
}



