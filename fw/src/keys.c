/*
 *  fw/src/keys.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <keys.h>
#include <hardware.h>

uint8_t pressedKeys = KEYS_MASK;

void initKeys() {
    KEYS_DDR &= ~(KEYS_MASK << KEYS_SHIFT);
    KEYS_PORT |= KEYS_MASK << KEYS_SHIFT;
}

void getKeys(uint8_t *keys, uint8_t *rising, uint8_t *falling) {
    uint8_t k=0;
    for (uint8_t n=0; n<32; ++n) {
        uint8_t new = (KEYS_PIN >> KEYS_SHIFT) & KEYS_MASK;

        if (k != new) {
            k = new;
            n=0;
        }
    }

    *rising = (pressedKeys ^ k) & (~k);
    *falling = (pressedKeys ^ k) & k;
    *keys = k ^ KEYS_MASK;

    pressedKeys = k;
}

