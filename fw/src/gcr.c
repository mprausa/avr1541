/*
 *  fw/src/gcr.c
 *
 *  Copyright (C) 2012 Mario Prausa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gcr.h>
#include <log.h>
#include <progmem.h>

static const uint8_t PROGMEM gcrTable[16] = {
    0x0a, 0x0b, 0x12, 0x13, 0x0e, 0x0f, 0x16, 0x17,
    0x09, 0x19, 0x1a, 0x1b, 0x0d, 0x1d, 0x1e, 0x15
};

void bin2gcr(uint8_t *bin, uint8_t *gcr) {
    uint16_t g;

    g = (((uint16_t)pgm_read_byte(gcrTable+(*bin >> 4))) << 5) | pgm_read_byte(gcrTable+(*bin & 15));
    *(gcr++) = g >> 2;
    *gcr = (g << 6) & 0xc0;
    bin++;

    g = (((uint16_t)pgm_read_byte(gcrTable+(*bin >> 4))) << 5) | pgm_read_byte(gcrTable+(*bin & 15));
    *(gcr++) |= (g >> 4) & 0x3f;
    *gcr = (g << 4) & 0xf0;
    bin++;

    g = (((uint16_t)pgm_read_byte(gcrTable+(*bin >> 4))) << 5) | pgm_read_byte(gcrTable+(*bin & 15));
    *(gcr++) |= (g >> 6) & 0x0f;
    *gcr = (g << 2) & 0xfc;
    bin++;

    g = (((uint16_t)pgm_read_byte(gcrTable+(*bin >> 4))) << 5) | pgm_read_byte(gcrTable+(*bin & 15));
    *(gcr++) |= (g >> 8) & 0x03;
    *gcr = g;
}

static uint8_t gcr_lookup(uint8_t code) {
    for (uint8_t n=0; n<16; ++n) {
        if (pgm_read_byte(gcrTable+n) == code) return n;
    }

    logPrintf(_PSTR("invalid gcr code: 0x%x\n"),code);
    return 0xf;
}

void gcr2bin(uint8_t *gcr, uint8_t *bin) {
    uint8_t code;

    code = gcr[0]>>3;
    bin[0] = gcr_lookup(code)<<4;

    code = ((gcr[0]<<2)|(gcr[1]>>6))&0x1f;
    bin[0] |= gcr_lookup(code);

    code = (gcr[1]>>1)&0x1f;
    bin[1] = gcr_lookup(code)<<4;

    code = ((gcr[1]<<4)|(gcr[2]>>4))&0x1f;
    bin[1] |= gcr_lookup(code);

    code = ((gcr[2]<<1)|(gcr[3]>>7))&0x1f;
    bin[2] = gcr_lookup(code)<<4;

    code = (gcr[3]>>2)&0x1f;
    bin[2] |= gcr_lookup(code);

    code = ((gcr[3]<<3)|(gcr[4]>>5))&0x1f;
    bin[3] = gcr_lookup(code)<<4;

    code = gcr[4]&0x1f;
    bin[3] |= gcr_lookup(code);
}
