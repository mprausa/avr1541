#!/bin/bash

if [ $# -ne 2 ]; then
	echo "Usage: $0 svg-file pdf-file"
	exit 1
fi

cat $1 | `dirname $0`/hidelayer.sh parts_top \
       | `dirname $0`/hidelayer.sh parts_bottom \
       | `dirname $0`/hidelayer.sh labels_top \
       | `dirname $0`/hidelayer.sh labels_bottom > /tmp/layout.top.bottom.svg

cat /tmp/layout.top.bottom.svg | `dirname $0`/hidelayer.sh top > /tmp/layout.bottom.svg
cat /tmp/layout.top.bottom.svg | `dirname $0`/hidelayer.sh bottom > /tmp/layout.top.svg

cat $1 | `dirname $0`/hidelayer.sh parts_bottom \
       | `dirname $0`/hidelayer.sh labels_bottom \
       | `dirname $0`/hidelayer.sh top \
       | `dirname $0`/hidelayer.sh bottom \
       | `dirname $0`/hidelayer.sh vias > /tmp/layout.parts_top.svg

cat > /tmp/layout.tex <<EOF
\\documentclass[a4paper,10pt]{article}
\\usepackage{graphicx}

\\begin{document}
\\begin{figure}
  \\centering
  \\includegraphics[type=pdf,ext=.pdf,read=.pdf]{layout.all}
  \\caption{all}
\\end{figure}
\\begin{figure}
  \\centering
  \\includegraphics[type=pdf,ext=.pdf,read=.pdf]{layout.top.bottom}
  \\caption{top and bottom}
\\end{figure}
\\begin{figure}
  \\centering
  \\includegraphics[type=pdf,ext=.pdf,read=.pdf]{layout.top}
  \\caption{top}
\\end{figure}
\\begin{figure}
  \\centering
  \\includegraphics[type=pdf,ext=.pdf,read=.pdf]{layout.bottom}
  \\caption{bottom}
\\end{figure}
\\begin{figure}
  \\centering
  \\includegraphics[type=pdf,ext=.pdf,read=.pdf]{layout.parts_top}
  \\caption{parts\\_top}
\\end{figure}
EOF

if grep -q 'inkscape:label="parts_bottom"' $1; then
	cat >> /tmp/layout.tex <<EOF
	\\begin{figure}
  	  \\centering
	  \\includegraphics[type=pdf,ext=.pdf,read=.pdf]{layout.parts_bottom}
	  \\caption{parts\\_bottom}
	\\end{figure}
EOF
	cat $1 | `dirname $0`/hidelayer.sh parts_top \
	       | `dirname $0`/hidelayer.sh labels_top \
	       | `dirname $0`/hidelayer.sh top \
	       | `dirname $0`/hidelayer.sh bottom \
	       | `dirname $0`/hidelayer.sh vias > /tmp/layout.parts_bottom.svg
	inkscape -z -D -A /tmp/layout.parts_bottom.pdf /tmp/layout.parts_bottom.svg
fi	
cat >> /tmp/layout.tex << EOF
\\end{document}
EOF

inkscape -z -D -A /tmp/layout.all.pdf $1
inkscape -z -D -A /tmp/layout.top.bottom.pdf /tmp/layout.top.bottom.svg
inkscape -z -D -A /tmp/layout.top.pdf /tmp/layout.top.svg
inkscape -z -D -A /tmp/layout.bottom.pdf /tmp/layout.bottom.svg
inkscape -z -D -A /tmp/layout.parts_top.pdf /tmp/layout.parts_top.svg

cd /tmp
pdflatex layout.tex
cd "$OLDPWD"
mv /tmp/layout.pdf $2
rm -f /tmp/layout.*

exit 0
