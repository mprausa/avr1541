#!/usr/bin/perl

sub round {
	$_[0] > 0 ? int($_[0] + .5) : -int(-$_[0] + .5)
}

$layer = 0;
$gcntr = 0;

$offset = 1*3.5432;
$grid = 2.54*3.5432;

while ($line = <STDIN>) {
	if ($line =~ m/inkscape:label="top"/) {
		$layer=1;
	} elsif ($line =~ m/inkscape:label="bottom"/ ) {
		$layer=2;
	} elsif ($line =~ m/inkscape:label="vias"/ ) {
		$layer=3;
	}

	if ($layer && ($line =~ m/<\/g>/)) {
		if ($gcntr) {
			--$gcntr;
		} else {
			$layer = 0;
		}
	}

	if (($layer == 1) && ($line =~ m/^(.*style=\").*stroke.*(\".*)/ )) {
		print "$1fill:none;stroke:#008000;stroke-width:2.12598419;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:1;stroke-dasharray:none;stroke-dashoffset:0$2\n";
	} elsif (($layer == 2) && ($line =~ m/^(.*style=\").*stroke.*(\".*)/ )) {
		print "$1fill:none;stroke:#000080;stroke-width:2.12598419;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:1;stroke-dasharray:none;stroke-dashoffset:0$2\n";
	} elsif (($layer == 1 || $layer == 2) && ($line =~ m/^(.*d=\"m)\s*(.*)\"/) ) {
		$pre = $1;
		$str = $2;
		
#		print "$pre $str\"\n";
	
		@array = split(/ /,$str);
		$array[0] =~ m/(.*),(.*)/;

		$x = ($1-$offset)/$grid;
		$y = ($2-$offset)/$grid;

		$x = round($x);
		$y = round($y);

		$x = $grid*$x+$offset;
		$y = $grid*$y+$offset;

		print "$pre $x,$y";

		shift(@array);
	
		foreach (@array) {
			$_ =~ m/(.*),(.*)/;
		
			$x = $1/$grid;
			$y = $2/$grid;

			$x = round($x);
			$y = round($y);

			$x = $grid*$x;
			$y = $grid*$y;

			print " $x,$y";
		}

		print "\"\n";
	} elsif (($layer == 1 || $layer == 2) && ($line =~ m/^(.*d=\"M)\s*(.*)\"/) ) {
		$pre = $1;
		$str = $2;

#		print "$pre $str\"\n";
	
		@array = split(/ /,$str);

		print "$pre";

		foreach (@array) {
			$_ =~ m/(.*),(.*)/;
		
			$x = ($1-$offset)/$grid;
			$y = ($2-$offset)/$grid;
		
			$x = round($x);
			$y = round($y);

			$x = $grid*$x+$offset;
			$y = $grid*$y+$offset;

			print " $x,$y";
		}

		print "\"\n";
	} elsif ($layer == 3 && $line =~ m/^(.*transform=\"matrix\(.*),(.*),(.*)(\)\".*)/) {
		$pre = $1;
		$x = $2;
		$y = $3;
		$post = $4;

		$x /= $grid;
		$y /= $grid;

		$x = round($x);
		$y = round($y);

		$x = $grid*$x+1.2876218;
		$y = $grid*$y-1.1600666;

		print "$pre,$x,$y$post\n";
	} else {
		if ($layer && $line =~ m/<g/) {
			++$gcntr;
		}
		print $line
	}
}

